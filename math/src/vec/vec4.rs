use std::{fmt, ops};

use super::super::{DotProduct, Length, Vec2};

/// A three-dimensional vector using `x`-, `y`-, and `z`-components.
///
/// ```
///# use math::Vec4;
/// let v1 = Vec4::new(1., 1., 1., 1.);
/// ```
///
/// `Vec4` implements `std::ops::Add` and does componentwise addition.
///
/// ```
///# use math::Vec4;
/// let v1 = Vec4::new(1., 1., 1., 1.);
/// let v2 = Vec4::new(1., 1., 1., 1.);
/// let v3 = v1 + v2;
/// assert_eq!(v3.x, 2.);
/// assert_eq!(v3.y, 2.);
/// assert_eq!(v3.z, 2.);
/// assert_eq!(v3.w, 2.);
/// ```
///
/// `Vec4` implements `std::ops::Sub` and does componentwise subtraction.
///
/// ```
///# use math::Vec4;
/// let v1 = Vec4::new(1., 1., 1., 1.);
/// let v2 = Vec4::new(1., 1., 1., 1.);
/// let v3 = v1 - v2;
/// assert_eq!(v3.x, 0.);
/// assert_eq!(v3.y, 0.);
/// assert_eq!(v3.z, 0.);
/// assert_eq!(v3.w, 0.);
/// ```
///
/// A `Vec4` can be multiplied by a scalar value.
///
/// ```
///# use math::Vec4;
/// let v = Vec4::new(1., 2., 3., 4.) * 2.;
/// assert_eq!(v, Vec4::new(2., 4., 6., 8.));
/// ```
///
/// ```
///# use math::Vec4;
/// let v1 = Vec4::new(1., 1., 1., 1.);
/// let v2 = Vec4::new(1., 1., 2., 1.);
/// let v3 = Vec4::new(1., 1., 1., 1.);
/// assert_ne!(v1, v2);
/// assert_eq!(v1, v3);
/// ```
///
/// A `Vec4` can also be indexed to reach a particular component.
///
/// ```
///# use math::Vec4;
/// let mut v = Vec4::new(0., 1., 2., 3.);
///
/// assert_eq!(v[3], 3.);
///
/// v[3] = 9.;
/// assert_eq!(v[3], 9.);
/// ```
#[derive(Default, Clone, Copy, PartialEq)]
pub struct Vec4 {
    /// The `x`-component of the vector.
    pub x: f32,
    /// The `y`-component of the vector.
    pub y: f32,
    /// The `z`-component of the vector.
    pub z: f32,
    /// The `w`-component of the vector.
    pub w: f32,
}

impl Vec4 {
    pub const ZERO: Self = Self::new(0., 0., 0., 0.);
    pub const BASIS: Self = Self::new(1., 1., 1., 1.);
    pub const X: Self = Self::new(1., 0., 0., 0.);
    pub const Y: Self = Self::new(0., 1., 0., 0.);
    pub const Z: Self = Self::new(0., 0., 1., 0.);
    pub const W: Self = Self::new(0., 0., 0., 1.);

    /// Creates a new `Vec4`.
    ///
    /// ```
    ///# use math::Vec4;
    /// let v = Vec4::new(1., 2., 3., 4.);
    /// assert_eq!(v.x, 1.);
    /// assert_eq!(v.y, 2.);
    /// assert_eq!(v.z, 3.);
    /// assert_eq!(v.w, 4.);
    /// ```
    #[inline]
    pub const fn new(x: f32, y: f32, z: f32, w: f32) -> Self {
        Self { x, y, z, w }
    }

    /// Calculates the squared length of a `Vec4`.
    /// Equavialent to `Vec4::dot(self, self)`.
    ///
    /// ```
    ///# use math::Vec4;
    /// let v = Vec4::new(3., 4., 5., 6.);
    /// assert_eq!(v.len_sqr(), 86.);
    /// ```
    #[inline]
    pub fn len_sqr(&self) -> f32 {
        self.x * self.x + self.y * self.y + self.z * self.z + self.w * self.w
    }

    /// Normalizes the vector.
    ///
    /// ```
    ///# use math::Vec4;
    /// let v = Vec4::new(3., 0., 0., 0.);
    /// assert_eq!(v.normalize(), Vec4::X);
    /// ```
    #[inline]
    pub fn normalize(self) -> Self {
        self / self.len()
    }

    /// Drops the `w`-component and turns the `Vec4` into a `Vec3`.
    ///
    /// ```
    ///# use math::{Vec3, Vec4};
    /// let v = Vec4::new(1., 2., 3., 4.);
    /// assert_eq!(v.to_vec3(), Vec3::new(1., 2., 3.));
    /// ```
    #[inline]
    pub const fn to_vec3(self) -> Vec3 {
        Vec3::new(self.x, self.y, self.z)
    }

    #[inline]
    pub const fn xy(self) -> Vec2 {
        Vec2::new(self.x, self.y)
    }

    #[inline]
    pub const fn xyz(self) -> Vec3 {
        Vec3::new(self.x, self.y, self.z)
    }
}

impl Length for Vec4 {
    type Output = f32;

    /// Calculates the length of a `Vec4`.
    ///
    /// ```
    ///# use math::{Length, Vec4};
    /// let v = Vec4::new(3., 4., 5., 6.);
    /// assert_eq!(v.len(), 9.273619);
    /// ```
    #[inline]
    fn len(&self) -> Self::Output {
        self.len_sqr().sqrt()
    }
}

impl DotProduct for Vec4 {
    type Output = f32;

    /// Calculates the dot product between two vectors.
    ///
    /// ```
    ///# use math::{DotProduct, Vec4};
    /// let v1 = Vec4::new(3., 4., 5., 6.);
    /// let v2 = Vec4::new(3., 4., 5., 6.);
    /// assert_eq!(v1.dot(v2), 86.0f32);
    /// ```
    #[inline]
    fn dot(self, other: Self) -> Self::Output {
        self.x * other.x + self.y * other.y + self.z * other.z + self.w * other.w
    }
}

impl ops::Neg for Vec4 {
    type Output = Vec4;

    /// Negates a `Vec4` where `T: Neg`.
    ///
    /// ```
    ///# use math::Vec4;
    /// let v = Vec4::new(1., 1., 1., 1.);
    /// assert_eq!(-v, Vec4::new(-1., -1., -1., -1.));
    /// ```
    #[inline]
    fn neg(self) -> Self::Output {
        Vec4::new(-self.x, -self.y, -self.z, -self.w)
    }
}

/// Add two `Vec4`s with component-wise addition.
///
/// ```
///# use math::Vec4;
/// let v1 = Vec4::new(1., 1., 1., 1.);
/// let v2 = Vec4::new(2., 2., 2., 2.);
/// assert_eq!(v1 + v2, Vec4::new(3., 3., 3., 3.));
/// ```
impl ops::Add for Vec4 {
    type Output = Vec4;

    #[inline]
    fn add(self, other: Self) -> Self::Output {
        Vec4 {
            x: self.x + other.x,
            y: self.y + other.y,
            z: self.z + other.z,
            w: self.w + other.w,
        }
    }
}

/// Add two `Vec4`s with component-wise addition.
///
/// ```
///# use math::Vec4;
/// let mut v = Vec4::new(1., 1., 1., 1.);
/// v += Vec4::new(2., 2., 2., 2.);
/// assert_eq!(v, Vec4::new(3., 3., 3., 3.));
/// ```
impl ops::AddAssign for Vec4 {
    #[inline]
    fn add_assign(&mut self, other: Self) {
        self.x += other.x;
        self.y += other.y;
        self.z += other.z;
        self.w += other.w;
    }
}

/// Subtract two `Vec4`s with component-wise subtraction.
///
/// ```
///# use math::Vec4;
/// let v1 = Vec4::new(2., 2., 2., 2.);
/// let v2 = Vec4::new(1., 1., 1., 1.);
/// assert_eq!(v1 - v2, Vec4::new(1., 1., 1., 1.));
/// ```
impl ops::Sub for Vec4 {
    type Output = Vec4;

    #[inline]
    fn sub(self, other: Self) -> Self::Output {
        Vec4 {
            x: self.x - other.x,
            y: self.y - other.y,
            z: self.z - other.z,
            w: self.w - other.w,
        }
    }
}

/// Multiply a `Vec4` with a scalar.
///
/// ```
///# use math::Vec4;
/// let v1 = Vec4::new(2., 2., 2., 2.);
/// assert_eq!(v1 * 2., Vec4::new(4., 4., 4., 4.));
/// ```
impl ops::Mul<f32> for Vec4 {
    type Output = Vec4;

    #[inline]
    fn mul(self, scalar: f32) -> Self::Output {
        Vec4 {
            x: self.x * scalar,
            y: self.y * scalar,
            z: self.z * scalar,
            w: self.w * scalar,
        }
    }
}

/// Multiply a `Vec4` with a scalar.
///
/// ```
///# use math::Vec4;
/// let mut v1 = Vec4::new(2., 2., 2., 2.);
/// v1 *= 2.;
/// assert_eq!(v1, Vec4::new(4., 4., 4., 4.));
/// ```
impl ops::MulAssign<f32> for Vec4 {
    #[inline]
    fn mul_assign(&mut self, scalar: f32) {
        self.x *= scalar;
        self.y *= scalar;
        self.z *= scalar;
        self.w *= scalar;
    }
}

/// Divide a `Vec4` by a scalar.
///
/// ```
///# use math::Vec4;
/// let v1 = Vec4::new(2., 2., 2., 2.);
/// assert_eq!(v1 / 2., Vec4::new(1., 1., 1., 1.));
/// ```
impl ops::Div<f32> for Vec4 {
    type Output = Vec4;

    #[inline]
    fn div(self, scalar: f32) -> Self::Output {
        Vec4 {
            x: self.x / scalar,
            y: self.y / scalar,
            z: self.z / scalar,
            w: self.w / scalar,
        }
    }
}

/// Divide a `Vec4` by a scalar.
///
/// ```
///# use math::Vec4;
/// let mut v = Vec4::new(2., 2., 2., 2.);
/// v /= 2.;
/// assert_eq!(v, Vec4::new(1., 1., 1., 1.));
/// ```
impl ops::DivAssign<f32> for Vec4 {
    #[inline]
    fn div_assign(&mut self, scalar: f32) {
        self.x /= scalar;
        self.y /= scalar;
        self.z /= scalar;
        self.w /= scalar;
    }
}

impl From<[f32; 4]> for Vec4 {
    /// Creates a `Vec4` from `[f32; 4]`.
    ///
    /// ```
    ///# use math::Vec4;
    /// let v = Vec4::from([1., 1., 1., 1.]);
    /// assert_eq!(v, Vec4::new(1., 1., 1., 1.));
    /// ```
    #[inline]
    fn from([x, y, z, w]: [f32; 4]) -> Self {
        Self::new(x, y, z, w)
    }
}

use crate::Vec3;
impl From<(Vec3, f32)> for Vec4 {
    /// Creates a `Vec4` from `(Vec3, f32)`.
    ///
    /// ```
    ///# use math::{Vec3, Vec4};
    /// let v = Vec4::from((Vec3::new(1., 1., 1.), 1.));
    /// assert_eq!(v, Vec4::new(1., 1., 1., 1.));
    /// ```
    #[inline]
    fn from((Vec3 { x, y, z }, w): (Vec3, f32)) -> Self {
        Self::new(x, y, z, w)
    }
}

impl From<(f32, f32, f32, f32)> for Vec4 {
    /// Creates a `Vec4` from `(f32, f32, f32, f32)`.
    ///
    /// ```
    ///# use math::Vec4;
    /// let v = Vec4::from((1., 1., 1., 1.));
    /// assert_eq!(v, Vec4::new(1., 1., 1., 1.));
    /// ```
    #[inline]
    fn from((x, y, z, w): (f32, f32, f32, f32)) -> Self {
        Self::new(x, y, z, w)
    }
}

impl Into<[f32; 4]> for Vec4 {
    /// Creates a `[f32; 4]` from a `Vec4`.
    ///
    /// ```
    ///# use math::Vec4;
    /// let v: [f32; 4] = Vec4::new(1., 1., 1., 1.).into();
    /// assert_eq!(v, [1., 1., 1., 1.]);
    /// ```
    #[inline]
    fn into(self) -> [f32; 4] {
        let Vec4 { x, y, z, w } = self;
        [x, y, z, w]
    }
}

impl Into<(f32, f32, f32, f32)> for Vec4 {
    /// Creates a `(f32, f32, f32, f32)` from a `Vec4`.
    ///
    /// ```
    ///# use math::Vec4;
    /// let v: (f32, f32, f32, f32) = Vec4::new(1., 1., 1., 1.).into();
    /// assert_eq!(v, (1., 1., 1., 1.));
    /// ```
    #[inline]
    fn into(self) -> (f32, f32, f32, f32) {
        let Vec4 { x, y, z, w } = self;
        (x, y, z, w)
    }
}

/// Formats a `Vec4` for debug when `T: Debug`.
///
/// ```
///# use math::Vec4;
/// let v = Vec4::new(1., 1., 1., 1.);
/// let d = format!("{:?}", v);
/// assert_eq!(d, "<1.0, 1.0, 1.0, 1.0>".to_owned());
/// ```
impl fmt::Debug for Vec4 {
    #[inline]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "<{:?}, {:?}, {:?}, {:?}>",
            self.x, self.y, self.z, self.w
        )
    }
}

impl std::ops::Index<usize> for Vec4 {
    type Output = f32;

    fn index(&self, i: usize) -> &Self::Output {
        match i {
            0 => &self.x,
            1 => &self.y,
            2 => &self.z,
            3 => &self.w,
            _ => panic!("Invalid vector component position: {}", i),
        }
    }
}

impl std::ops::IndexMut<usize> for Vec4 {
    fn index_mut(&mut self, i: usize) -> &mut Self::Output {
        match i {
            0 => &mut self.x,
            1 => &mut self.y,
            2 => &mut self.z,
            3 => &mut self.w,
            _ => panic!("Invalid vector component position: {}", i),
        }
    }
}
