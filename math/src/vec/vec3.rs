use std::{fmt, ops};

use super::super::{CrossProduct, DotProduct, Length, Vec2, Vec4};

/// A three-dimensional vector using `x`-, `y`-, and `z`-components.
///
/// ```
///# use math::Vec3;
/// let v1 = Vec3 { x: 1.0, y: 1.0, z: 1.0 };
/// ```
///
/// ```
///# use math::Vec3;
/// let v1 = Vec3::new(1., 1., 1.);
/// let v2 = Vec3::new(1., 1., 1.);
/// let v3 = v1 + v2;
/// assert_eq!(v3.x, 2.);
/// assert_eq!(v3.y, 2.);
/// assert_eq!(v3.z, 2.);
/// ```
///
/// `Vec3` implements `std::ops::Sub` and does componentwise subtraction.
///
/// ```
///# use math::Vec3;
/// let v1 = Vec3::new(1., 1., 1.);
/// let v2 = Vec3::new(1., 1., 1.);
/// let v3 = v1 - v2;
/// assert_eq!(v3.x, 0.);
/// assert_eq!(v3.y, 0.);
/// assert_eq!(v3.z, 0.);
/// ```
///
/// ```
///# use math::Vec3;
/// let v = Vec3::new(1., 2., 3.) * 2.;
/// assert_eq!(v, Vec3::new(2., 4., 6.));
/// ```
///
/// ```
///# use math::Vec3;
/// let v3 = Vec3::new(1., 2., 3.);
/// let v4 = v3;
/// assert_eq!(v3, v4);
/// ```
///
/// ```
///# use math::Vec3;
/// let v1 = Vec3::new(1., 1., 1.);
/// let v2 = Vec3::new(1., 2., 1.);
/// let v3 = Vec3::new(1., 1., 1.);
/// assert_ne!(v1, v2);
/// assert_eq!(v1, v3);
/// ```
///
#[derive(Default, Clone, Copy, PartialEq, PartialOrd)]
pub struct Vec3 {
    /// The `x`-component of the vector.
    pub x: f32,
    /// The `y`-component of the vector.
    pub y: f32,
    /// The `z`-component of the vector.
    pub z: f32,
}

impl Vec3 {
    pub const ZERO: Self = Self::new(0., 0., 0.);
    pub const BASIS: Self = Self::new(1., 1., 1.);

    pub const X: Self = Self::new(1., 0., 0.);
    pub const Y: Self = Self::new(0., 1., 0.);
    pub const Z: Self = Self::new(0., 0., 1.);
}

impl Vec3 {
    /// Creates a new `Vec3`.
    ///
    /// ```
    ///# use math::Vec3;
    /// let v = Vec3::new(1., 2., 3.);
    /// assert_eq!(v.x, 1.);
    /// assert_eq!(v.y, 2.);
    /// assert_eq!(v.z, 3.);
    /// ```
    #[inline]
    pub const fn new(x: f32, y: f32, z: f32) -> Self {
        Self { x, y, z }
    }

    /// Calculates the squared length of a `Vec3`.
    ///
    /// ```
    ///# use math::Vec3;
    /// let v = Vec3::new(3., 4., 5.);
    /// assert_eq!(v.len_sqr(), 50.);
    /// ```
    #[inline]
    pub fn len_sqr(&self) -> f32 {
        self.x * self.x + self.y * self.y + self.z * self.z
    }

    /// Normalizes the vector.
    ///
    /// ```
    ///# use math::Vec3;
    /// let v = Vec3::new(3., 0., 0.);
    /// assert_eq!(v.normalize(), Vec3::X);
    /// ```
    #[inline]
    pub fn normalize(self) -> Self {
        self / self.len()
    }

    /// Converts a Vec3 to a Vec4 with the `w`-component set to 1.
    ///
    /// ```
    ///# use math::{Vec3, Vec4};
    /// let v = Vec3::new(1., 2., 3.).to_point();
    /// assert_eq!(v, Vec4::new(1., 2., 3., 1.));
    /// ```
    #[inline]
    pub fn to_point(self) -> Vec4 {
        Vec4::new(self.x, self.y, self.z, 1.)
    }

    /// Converts a `Vec` to a `Vec4` with the `w`-component set to 0.
    ///
    /// ```
    ///# use math::{Vec3, Vec4};
    /// let v = Vec3::new(1., 2., 3.).to_vec4();
    /// assert_eq!(v, Vec4::new(1., 2., 3., 0.));
    /// ```
    #[inline]
    pub fn to_vec4(self) -> Vec4 {
        Vec4::new(self.x, self.y, self.z, 0.)
    }

    /// Creates an inverse of the vector.
    ///
    /// ```
    ///# use math::Vec3;
    /// let v = Vec3::new(2., 2., 2.);
    /// assert_eq!(v.inv(), Vec3::new(0.5, 0.5, 0.5));
    /// ```
    #[inline]
    pub fn inv(self) -> Self {
        Self::new(1. / self.x, 1. / self.y, 1. / self.z)
    }

    #[inline]
    pub const fn xy(self) -> Vec2 {
        Vec2::new(self.x, self.y)
    }
}

impl Length for Vec3 {
    type Output = f32;

    /// Calculates the length of a `Vec3`.
    ///
    /// ```
    ///# use math::{Length, Vec3};
    /// let v = Vec3::new(3., 4., 5.);
    /// assert_eq!(v.len(), 7.071068);
    /// ```
    #[inline]
    fn len(&self) -> Self::Output {
        self.len_sqr().sqrt()
    }
}

impl DotProduct for Vec3 {
    type Output = f32;

    /// Calculates the squared length of a `Vec3`.
    ///
    /// ```
    ///# use math::Vec3;
    /// let v = Vec3::new(3., 4., 5.);
    /// assert_eq!(v.len_sqr(), 50.0);
    /// ```
    #[inline]
    fn dot(self, other: Self) -> Self::Output {
        self.x * other.x + self.y * other.y + self.z * other.z
    }
}

impl CrossProduct for Vec3 {
    type Output = Vec3;

    /// Cross product between two vectors.
    ///
    /// ```
    ///# use math::{CrossProduct, Vec3};
    /// let v1 = Vec3::new(1., 0., 0.);
    /// let v2 = Vec3::new(0., 1., 0.);
    /// assert_eq!(v1.cross(v2), Vec3::new(0., 0., 1.));
    /// assert_eq!(v2.cross(v1), -Vec3::new(0., 0., 1.));
    /// ```
    fn cross(self, other: Self) -> Self::Output {
        Vec3::new(
            self.y * other.z - self.z * other.y,
            self.z * other.x - self.x * other.z,
            self.x * other.y - self.y * other.x,
        )
    }
}

impl ops::Neg for Vec3 {
    type Output = Vec3;

    /// Negates a `Vec3` where `T: Neg`.
    ///
    /// ```
    ///# use math::Vec3;
    /// let v = Vec3::new(1., 1., 1.);
    /// assert_eq!(-v, Vec3::new(-1., -1., -1.));
    /// ```
    #[inline]
    fn neg(self) -> Self::Output {
        Vec3::new(-self.x, -self.y, -self.z)
    }
}

/// Add two `Vec3`s with component-wise addition.
///
/// ```
///# use math::Vec3;
/// let v1 = Vec3::new(1., 1., 1.);
/// let v2 = Vec3::new(2., 2., 2.);
/// assert_eq!(v1 + v2, Vec3::new(3., 3., 3.));
/// ```
impl ops::Add for Vec3 {
    type Output = Self;

    #[inline]
    fn add(self, other: Self) -> Self::Output {
        Vec3 {
            x: self.x + other.x,
            y: self.y + other.y,
            z: self.z + other.z,
        }
    }
}

/// Add two `Vec3`s with component-wise addition.
///
/// ```
///# use math::Vec3;
/// let mut v1 = Vec3::new(1., 1., 1.);
/// v1 += Vec3::new(2., 2., 2.);
/// assert_eq!(v1, Vec3::new(3., 3., 3.));
/// ```
impl ops::AddAssign for Vec3 {
    #[inline]
    fn add_assign(&mut self, other: Self) {
        self.x += other.x;
        self.y += other.y;
        self.z += other.z;
    }
}

/// Subtract two `Vec3`s with component-wise subtraction.
///
/// ```
///# use math::Vec3;
/// let v1 = Vec3::new(2., 2., 2.);
/// let v2 = Vec3::new(1., 1., 1.);
/// assert_eq!(v1 - v2, Vec3::new(1., 1., 1.));
/// ```
impl ops::Sub for Vec3 {
    type Output = Self;

    #[inline]
    fn sub(self, other: Self) -> Self::Output {
        Vec3 {
            x: self.x - other.x,
            y: self.y - other.y,
            z: self.z - other.z,
        }
    }
}

/// Subtract two `Vec3`s with component-wise subtraction.
///
/// ```
///# use math::Vec3;
/// let mut v1 = Vec3::new(2., 2., 2.);
/// v1 -= Vec3::new(1., 1., 1.);
/// assert_eq!(v1, Vec3::new(1., 1., 1.));
/// ```
impl ops::SubAssign for Vec3 {
    #[inline]
    fn sub_assign(&mut self, other: Self) {
        self.x -= other.x;
        self.y -= other.y;
        self.z -= other.z;
    }
}

/// Multiply a `Vec3` by a scalar value.
///
/// ```
///# use math::Vec3;
/// let v1 = Vec3::new(2., 2., 2.);
/// assert_eq!(v1 * 3., Vec3::new(6., 6., 6.));
/// ```
impl ops::Mul<f32> for Vec3 {
    type Output = Vec3;

    #[inline]
    fn mul(self, scalar: f32) -> Self::Output {
        Vec3 {
            x: self.x * scalar,
            y: self.y * scalar,
            z: self.z * scalar,
        }
    }
}

/// Multiply a `Vec3` by a scalar value.
///
/// ```
///# use math::Vec3;
/// let mut v1 = Vec3::new(2., 2., 2.);
/// v1 *= 3.;
/// assert_eq!(v1, Vec3::new(6., 6., 6.));
/// ```
impl ops::MulAssign<f32> for Vec3 {
    #[inline]
    fn mul_assign(&mut self, scalar: f32) {
        self.x *= scalar;
        self.y *= scalar;
        self.z *= scalar;
    }
}

/// Divide a `Vec3` by a scalar value.
///
/// ```
///# use math::Vec3;
/// let v1 = Vec3::new(2., 2., 2.);
/// assert_eq!(v1 / 2., Vec3::new(1., 1., 1.));
/// ```
impl ops::Div<f32> for Vec3 {
    type Output = Vec3;

    #[inline]
    fn div(self, scalar: f32) -> Self::Output {
        Vec3 {
            x: self.x / scalar,
            y: self.y / scalar,
            z: self.z / scalar,
        }
    }
}

/// Divide a `Vec3` by a scalar value.
///
/// ```
///# use math::Vec3;
/// let mut v1 = Vec3::new(2., 2., 2.);
/// v1 /= 2.;
/// assert_eq!(v1, Vec3::new(1., 1., 1.));
/// ```
impl ops::DivAssign<f32> for Vec3 {
    #[inline]
    fn div_assign(&mut self, scalar: f32) {
        self.x /= scalar;
        self.y /= scalar;
        self.z /= scalar;
    }
}

impl From<[f32; 3]> for Vec3 {
    /// Creates a `Vec3` from `[f32; 3]`.
    ///
    /// ```
    ///# use math::Vec3;
    /// let v = Vec3::from([1., 1., 1.]);
    /// assert_eq!(v, Vec3::new(1., 1., 1.));
    /// ```
    #[inline]
    fn from([x, y, z]: [f32; 3]) -> Self {
        Self::new(x, y, z)
    }
}

impl From<(f32, f32, f32)> for Vec3 {
    /// Creates a `Vec3` from `(f32, f32, f32)`.
    ///
    /// ```
    ///# use math::Vec3;
    /// let v = Vec3::from((1., 1., 1.));
    /// assert_eq!(v, Vec3::new(1., 1., 1.));
    /// ```
    #[inline]
    fn from((x, y, z): (f32, f32, f32)) -> Self {
        Self::new(x, y, z)
    }
}

impl From<Vec3> for [f32; 3] {
    /// Creates a `[f32; 3]` from a `Vec3`.
    ///
    /// ```
    ///# use math::Vec3;
    /// let v: [_; 3] = Vec3::new(1., 1., 1.).into();
    /// assert_eq!(v, [1., 1., 1.]);
    /// ```
    #[inline]
    fn from(Vec3 { x, y, z }: Vec3) -> Self {
        [x, y, z]
    }
}

impl From<Vec3> for (f32, f32, f32) {
    /// Creates a `(f32, f32, f32)` from a `Vec3`.
    ///
    /// ```
    ///# use math::Vec3;
    /// let v: (_, _, _) = Vec3::new(1., 1., 1.).into();
    /// assert_eq!(v, (1., 1., 1.));
    /// ```
    #[inline]
    fn from(Vec3 { x, y, z }: Vec3) -> Self {
        (x, y, z)
    }
}

impl From<(Vec2, f32)> for Vec3 {
    /// Creates a `Vec3` from a `(Vec2, f32)`.
    ///
    /// ```
    ///# use math::{Vec2, Vec3};
    /// let v1 = Vec2::new(1., 2.);
    /// assert_eq!(Vec3::new(1., 2., 3.), (v1, 3.).into());
    /// ```
    #[inline]
    fn from((Vec2 { x, y }, z): (Vec2, f32)) -> Self {
        Self { x, y, z }
    }
}

impl From<Vec4> for Vec3 {
    /// Creates a `Vec3` from a `Vec4`, dropping the `w`-component.
    ///
    /// ```
    ///# use math::{Vec3, Vec4};
    /// let v: Vec3 = Vec4::new(1., 2., 3., 4.).into();
    /// assert_eq!(v, Vec3::new(1., 2., 3.));
    /// ```
    fn from(Vec4 { x, y, z, .. }: Vec4) -> Self {
        Self { x, y, z }
    }
}

/// Formats a `Vec3` for debug.
///
/// ```
///# use math::Vec3;
/// let v = Vec3::new(1., 1., 1.);
/// let d = format!("{:?}", v);
/// assert_eq!(d, "<1.0, 1.0, 1.0>".to_owned());
/// ```
impl fmt::Debug for Vec3 {
    #[inline]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "<{:?}, {:?}, {:?}>", self.x, self.y, self.z)
    }
}
