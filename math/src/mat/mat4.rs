use crate::{CrossProduct, DotProduct, Vec3, Vec4};
use std::ops;

/// A 4x4 matrix structure.
///
/// ```
///# use math::Mat4;
/// let mut mat: Mat4 = [
///     0., 1., 2., 3.,
///     4., 5., 6., 7.,
///     8., 9., 0., 1.,
///     2., 3., 4., 5.,
/// ].into();
///
/// assert_eq!(mat[1][2], 6.);
///
/// mat[1][2] = 9.;
/// assert_eq!(mat[1][2], 9.);
/// ```
#[derive(Default, Clone, Copy, PartialEq)]
pub struct Mat4 {
    data: [f32; 4 * 4],
}

impl Mat4 {
    /// Creates an identity matrix.
    ///
    /// ```
    ///# use math::Mat4;
    /// let ident = Mat4::IDENT;
    /// assert_eq!(ident, [
    ///     1., 0., 0., 0.,
    ///     0., 1., 0., 0.,
    ///     0., 0., 1., 0.,
    ///     0., 0., 0., 1.,
    /// ].into());
    /// ```
    #[rustfmt::skip]
    pub const IDENT: Self = Self {
        data: [
            1., 0., 0., 0.,
            0., 1., 0., 0.,
            0., 0., 1., 0.,
            0., 0., 0., 1.,
        ],
    };

    /// Creates a matrix with the specified x-offset.
    #[inline]
    pub const fn x_translate(x: f32) -> Self {
        Self::translate(x, 0., 0.)
    }

    /// Creates a matrix with the specified y-offset.
    #[inline]
    pub const fn y_translate(y: f32) -> Self {
        Self::translate(0., y, 0.)
    }

    /// Creates a matrix with the specified z-offset.
    #[inline]
    pub const fn z_translate(z: f32) -> Self {
        Self::translate(0., 0., z)
    }

    /// Creates a matrix with the specified x-, y-, and z-offsets.
    #[inline]
    #[rustfmt::skip]
    pub const fn translate(x: f32, y: f32, z: f32) -> Self {
        Self { data: [
            1., 0., 0., x,
            0., 1., 0., y,
            0., 0., 1., z,
            0., 0., 0., 1.,
        ] }
    }

    #[inline]
    #[rustfmt::skip]
    pub fn x_rotate(radians: f32) -> Self {
        let sin = radians.sin();
        let cos = radians.cos();

        Self { data: [
            1.,   0.,   0., 0.,
            0.,  cos,  sin, 0.,
            0., -sin,  cos, 0.,
            0.,   0.,   0., 1.,
        ] }
    }

    #[inline]
    #[rustfmt::skip]
    pub fn y_rotate(radians: f32) -> Self {
        let sin = radians.sin();
        let cos = radians.cos();

        Self { data: [
            cos, 0., -sin, 0.,
             0., 1.,   0., 0.,
            sin, 0.,  cos, 0.,
             0., 0.,   0., 1.,
        ] }
    }

    #[inline]
    #[rustfmt::skip]
    pub fn z_rotate(radians: f32) -> Self {
        let sin = radians.sin();
        let cos = radians.cos();

        Self { data: [
             cos,  sin, 0., 0.,
            -sin,  cos, 0., 0.,
              0.,   0., 1., 0.,
              0.,   0., 0., 1.,
        ] }
    }

    #[inline]
    pub const fn x_scale(scalar: f32) -> Self {
        Self::scale(scalar, 0., 0.)
    }

    #[inline]
    pub const fn y_scale(scalar: f32) -> Self {
        Self::scale(0., scalar, 0.)
    }

    #[inline]
    pub const fn z_scale(scalar: f32) -> Self {
        Self::scale(0., 0., scalar)
    }

    #[inline]
    #[rustfmt::skip]
    pub const fn scale(x: f32, y: f32, z: f32) -> Self {
        Self { data: [
            x, 0., 0., 0.,
            0., y, 0., 0.,
            0., 0., z, 0.,
            0., 0., 0., 1.,
        ] }
    }

    #[inline]
    #[rustfmt::skip]
    pub const fn scale_unif(scalar: f32) -> Self {
        Self { data: [
            scalar, 0., 0., 0.,
            0., scalar, 0., 0.,
            0., 0., scalar, 0.,
            0., 0., 0., 1.,
        ] }
    }

    #[inline]
    #[rustfmt::skip]
    pub const fn trans(self) -> Self {
        let [
            m00, m01, m02, m03,
            m04, m05, m06, m07,
            m08, m09, m10, m11,
            m12, m13, m14, m15,
        ] = self.data;

        // 0  1  2  3
        // 4  5  6  7
        // 8  9  10 11
        // 12 13 14 15
        Self { data: [
            m00, m04, m08, m12,
            m01, m05, m09, m13,
            m02, m06, m10, m14,
            m03, m07, m11, m15,
        ] }
    }

    /// Calculates the determinant of the matrix.
    ///
    /// ```
    ///# use math::Mat4;
    /// let m: Mat4 = [
    ///     4.,  3.,  2., 2.,
    ///     0.,  1., -3., 3.,
    ///     0., -1.,  3., 3.,
    ///     0.,  3.,  1., 1.,
    /// ].into();
    /// assert_eq!(m.det(), -240.);
    /// ```
    #[inline]
    #[rustfmt::skip]
    pub fn det(&self) -> f32 {
        let [
            m00, m01, m02, m03,
            m04, m05, m06, m07,
            m08, m09, m10, m11,
            m12, m13, m14, m15,
        ] = self.data;

         m00 * Vec3::new(m05, m06, m07).cross(Vec3::new(m09, m10, m11)).dot(Vec3::new(m13, m14, m15))
        -m01 * Vec3::new(m04, m06, m07).cross(Vec3::new(m08, m10, m11)).dot(Vec3::new(m12, m14, m15))
        +m02 * Vec3::new(m04, m05, m07).cross(Vec3::new(m08, m09, m11)).dot(Vec3::new(m12, m13, m15))
        -m03 * Vec3::new(m04, m05, m06).cross(Vec3::new(m08, m09, m10)).dot(Vec3::new(m12, m13, m14))
    }

    #[inline]
    #[rustfmt::skip]
    pub fn adj(&self) -> Self {
        let [
            m00, m01, m02, m03,
            m04, m05, m06, m07,
            m08, m09, m10, m11,
            m12, m13, m14, m15,
        ] = self.data;

        Self { data: [
            Vec3::new(m05, m06, m07).cross(Vec3::new(m09, m10, m11)).dot(Vec3::new(m13, m14, m15)), // m00
            Vec3::new(m01, m02, m03).cross(Vec3::new(m09, m10, m11)).dot(Vec3::new(m13, m14, m15)), // m04
            Vec3::new(m01, m02, m03).cross(Vec3::new(m05, m06, m07)).dot(Vec3::new(m13, m14, m15)), // m08
            Vec3::new(m01, m02, m03).cross(Vec3::new(m05, m06, m07)).dot(Vec3::new(m09, m10, m11)), // m12

           -Vec3::new(m04, m06, m07).cross(Vec3::new(m08, m10, m11)).dot(Vec3::new(m12, m14, m15)), // m01
           -Vec3::new(m00, m02, m03).cross(Vec3::new(m08, m10, m11)).dot(Vec3::new(m12, m14, m15)), // m05
           -Vec3::new(m00, m02, m03).cross(Vec3::new(m04, m06, m07)).dot(Vec3::new(m12, m14, m15)), // m09
           -Vec3::new(m00, m02, m03).cross(Vec3::new(m04, m06, m07)).dot(Vec3::new(m08, m10, m11)), // m13

            Vec3::new(m04, m05, m07).cross(Vec3::new(m08, m09, m11)).dot(Vec3::new(m12, m13, m15)), // m02
            Vec3::new(m00, m01, m03).cross(Vec3::new(m08, m09, m11)).dot(Vec3::new(m12, m13, m15)), // m06
            Vec3::new(m00, m01, m03).cross(Vec3::new(m04, m05, m07)).dot(Vec3::new(m12, m13, m15)), // m10
            Vec3::new(m00, m01, m03).cross(Vec3::new(m04, m05, m07)).dot(Vec3::new(m08, m09, m11)), // m14

           -Vec3::new(m04, m05, m06).cross(Vec3::new(m08, m09, m10)).dot(Vec3::new(m12, m13, m14)), // m03
           -Vec3::new(m00, m01, m02).cross(Vec3::new(m08, m09, m10)).dot(Vec3::new(m12, m13, m14)), // m07
           -Vec3::new(m00, m01, m02).cross(Vec3::new(m04, m05, m06)).dot(Vec3::new(m12, m13, m14)), // m11
           -Vec3::new(m00, m01, m02).cross(Vec3::new(m04, m05, m06)).dot(Vec3::new(m08, m09, m10)), // m15
        ] } / self.det()
    }

    #[inline]
    pub fn inverse(&self) -> Self {
        let adjoint = self.adj();
        let determinant = self.det();
        adjoint / determinant
    }

    #[inline]
    #[rustfmt::skip]
    /// Creates a left-handed, column-major projection matrix.
    pub fn perspective(fov: f32, aspect: f32, near: f32, far: f32) -> Self {
        let inv = 1. / (far - near);
        let b = 1. / (fov * 0.5).tan();
        let a = b / aspect;
        let c = -(far + near) * inv;
        let d = 2. * near * far * inv;

        [ a,  0.,  0., 0.,
          0., b,   0., 0.,
          0., 0.,  c,  -d,
          0., 0., -1., 0., ].into()
    }

    #[inline]
    #[rustfmt::skip]
    /// Creates a left-handed, column-major perspective matrix.
    pub fn look_at(eye: Vec3, at: Vec3, up: Vec3) -> Self {
        let z = (at - eye).normalize();
        let x = z.cross(up).normalize();
        let y = x.cross(z);

        let z = -z;

        let (xx, xy, xz) = x.into();
        let (yx, yy, yz) = y.into();
        let (zx, zy, zz) = z.into();

        [ xx, yx, zx, -x.dot(eye),
          xy, yy, zy, -y.dot(eye),
          xz, yz, zz, -z.dot(eye),
          0., 0., 0., 1.].into()
    }

    #[inline]
    pub fn index(&self, x: usize, y: usize) -> f32 {
        let loc = y * 4 + x;
        assert!(loc < self.data.len());

        self.data[loc]
    }
}

impl ops::Mul for Mat4 {
    type Output = Mat4;

    /// Multiplies two `Mat4`.
    ///
    /// ```
    ///# use math::Mat4;
    /// let m1 = Mat4::IDENT;
    /// let m2 = Mat4::IDENT;
    /// assert_eq!(m1 * m2, Mat4::IDENT);
    /// ```
    #[inline]
    #[rustfmt::skip]
    fn mul(self, other: Self) -> Self::Output {
        let [
            s00, s04, s08, s12,
            s01, s05, s09, s13,
            s02, s06, s10, s14,
            s03, s07, s11, s15,
        ] = self.data;

        let [
            o00, o04, o08, o12,
            o01, o05, o09, o13,
            o02, o06, o10, o14,
            o03, o07, o11, o15,
        ] = other.data;

        Self { data: [
            s00 * o00 + s01 * o04 + s02 * o08 + s03 * o12,
            s00 * o01 + s01 * o05 + s02 * o09 + s03 * o13,
            s00 * o02 + s01 * o06 + s02 * o10 + s03 * o14,
            s00 * o03 + s01 * o07 + s02 * o11 + s03 * o15,

            s04 * o00 + s05 * o04 + s06 * o08 + s07 * o12,
            s04 * o01 + s05 * o05 + s06 * o09 + s07 * o13,
            s04 * o02 + s05 * o06 + s06 * o10 + s07 * o14,
            s04 * o03 + s05 * o07 + s06 * o11 + s07 * o15,

            s08 * o00 + s09 * o04 + s10 * o08 + s11 * o12,
            s08 * o01 + s09 * o05 + s10 * o09 + s11 * o13,
            s08 * o02 + s09 * o06 + s10 * o10 + s11 * o14,
            s08 * o03 + s09 * o07 + s10 * o11 + s11 * o15,

            s12 * o00 + s13 * o04 + s14 * o08 + s15 * o12,
            s12 * o01 + s13 * o05 + s14 * o09 + s15 * o13,
            s12 * o02 + s13 * o06 + s14 * o10 + s15 * o14,
            s12 * o03 + s13 * o07 + s14 * o11 + s15 * o15,
        ] }
    }
}

impl ops::Mul<f32> for Mat4 {
    type Output = Self;

    #[inline]
    fn mul(mut self, s: f32) -> Self {
        for i in self.data.iter_mut() {
            *i *= s;
        }

        self
    }
}

impl ops::MulAssign<f32> for Mat4 {
    #[inline]
    fn mul_assign(&mut self, s: f32) {
        for i in self.data.iter_mut() {
            *i *= s;
        }
    }
}

impl ops::Div<f32> for Mat4 {
    type Output = Self;

    #[inline]
    fn div(mut self, s: f32) -> Self {
        for i in self.data.iter_mut() {
            *i /= s;
        }

        self
    }
}

impl ops::DivAssign<f32> for Mat4 {
    #[inline]
    fn div_assign(&mut self, s: f32) {
        for i in self.data.iter_mut() {
            *i /= s;
        }
    }
}

impl<T> ops::Mul<T> for Mat4
where
    T: Into<Vec4>,
{
    type Output = Vec4;

    /// Multiplies `Mat4` times a `Vec4`.
    ///
    /// ```
    ///# use math::{Mat4, Vec4};
    /// let m = Mat4::IDENT;
    /// let v = Vec4::new(3., 4., 5., 6.);
    /// assert_eq!(m * v, v);
    ///
    /// let n = [3., 4., 5., 6.];
    /// assert_eq!(m * n, v);
    ///
    /// let n = (3., 4., 5., 6.);
    /// assert_eq!(m * n, v);
    /// ```
    #[inline]
    #[rustfmt::skip]
    fn mul(self, other: T) -> Self::Output {
        let Vec4 { x, y, z, w } = other.into();
        let [
            m00, m01, m02, m03,
            m04, m05, m06, m07,
            m08, m09, m10, m11,
            m12, m13, m14, m15,
        ] = self.data;

        Vec4::new(
            m00 * x + m01 * y + m02 * z + m03 * w,
            m04 * x + m05 * y + m06 * z + m07 * w,
            m08 * x + m09 * y + m10 * z + m11 * w,
            m12 * x + m13 * y + m14 * z + m15 * w,
        )
    }
}

use std::fmt;
impl fmt::Debug for Mat4 {
    #[inline]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        writeln!(
            f,
            "[{:?}, {:?}, {:?}, {:?}]\n",
            self.data[0], self.data[1], self.data[2], self.data[3]
        )?;
        writeln!(
            f,
            "[{:?}, {:?}, {:?}, {:?}]\n",
            self.data[4], self.data[5], self.data[6], self.data[7]
        )?;
        writeln!(
            f,
            "[{:?}, {:?}, {:?}, {:?}]\n",
            self.data[8], self.data[9], self.data[10], self.data[11]
        )?;
        writeln!(
            f,
            "[{:?}, {:?}, {:?}, {:?}]\n",
            self.data[12], self.data[13], self.data[14], self.data[15]
        )
    }
}

impl From<[f32; 4 * 4]> for Mat4 {
    #[inline]
    fn from(data: [f32; 4 * 4]) -> Self {
        Self { data }
    }
}

impl std::ops::Index<usize> for Mat4 {
    type Output = [f32];

    fn index(&self, row: usize) -> &Self::Output {
        assert!(row < 4);

        let start = row * 4;
        let end = start + 4;

        &self.data[start..end]
    }
}

impl std::ops::IndexMut<usize> for Mat4 {
    fn index_mut(&mut self, row: usize) -> &mut Self::Output {
        assert!(row < 4);

        let start = row * 4;
        let end = start + 4;

        &mut self.data[start..end]
    }
}
