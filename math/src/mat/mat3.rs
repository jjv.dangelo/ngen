use crate::{CrossProduct, DotProduct, Vec3};
use std::ops;

/// A 3x3 matrix structure.
///
/// ```
///# use math::Mat3;
/// let mut mat: Mat3 = [
///     0., 1., 2.,
///     3., 4., 5.,
///     6., 7., 8.,
/// ].into();
///
/// assert_eq!(mat[1][2], 5.);
///
/// mat[1][2] = 9.;
/// assert_eq!(mat[1][2], 9.);
/// ```
#[derive(Default, Clone, Copy, PartialEq)]
pub struct Mat3 {
    data: [f32; 3 * 3],
}

impl Mat3 {
    /// Creates an identity matrix.
    ///
    /// ```
    ///# use math::Mat3;
    /// let ident = Mat3::IDENT;
    /// assert_eq!(ident, [
    ///     1., 0., 0.,
    ///     0., 1., 0.,
    ///     0., 0., 1.,
    /// ].into());
    /// ```
    pub const IDENT: Self = Self {
        data: [1., 0., 0., 0., 1., 0., 0., 0., 1.],
    };

    /// Transposes a matrix, converting columns to rows.
    ///
    /// ```
    ///# use math::Mat3;
    /// let mat: Mat3 = [
    ///     1., 2., 3.,
    ///     4., 5., 6.,
    ///     7., 8., 9.,
    /// ].into();
    /// let trans = mat.trans();
    /// assert_eq!(trans, [
    ///   1., 4., 7.,
    ///   2., 5., 8.,
    ///   3., 6., 9.,
    /// ].into());
    /// ```
    #[inline]
    #[rustfmt::skip]
    pub fn trans(&self) -> Self {
        let [
            m00, m01, m02,
            m03, m04, m05,
            m06, m07, m08,
        ] = self.data;

        Self { data: [
            m00, m03, m06,
            m01, m04, m07,
            m02, m05, m08,
        ] }
    }

    /// Calculates the determinant of the matrix.
    ///
    /// ```
    ///# use math::Mat3;
    /// let mat: Mat3 = [
    ///     -4., -3.,  3.,
    ///      0.,  2., -2.,
    ///      1.,  4., -1.,
    /// ].into();
    /// assert_eq!(mat.det(), -24.);
    /// ```
    #[inline]
    #[rustfmt::skip]
    pub fn det(&self) -> f32 {
        let [
            m00, m01, m02,
            m03, m04, m05,
            m06, m07, m08,
        ] = self.data;

        let a = Vec3::new(m00, m01, m02);
        let b = Vec3::new(m03, m04, m05);
        let c = Vec3::new(m06, m07, m08);

        a.cross(b).dot(c)
    }

    /// Calculates the adjugate of the matrix.
    ///
    /// ```
    ///# use math::Mat3;
    /// let mat: Mat3 = [
    ///     2., 0., 3.,
    ///     0., 1., 0.,
    ///     0., 0., 2.,
    /// ].into();
    /// let adj = mat.adj();
    /// assert_eq!(mat * adj, Mat3::IDENT);
    /// ```
    #[inline]
    #[rustfmt::skip]
    pub fn adj(&self) -> Self {
        let [
            m00, m01, m02,
            m03, m04, m05,
            m06, m07, m08,
        ] = self.data;

        // The data are already layed out here as transposed
        // to avoid moving the data around too much.
        // We could have constructed the matrix and then
        // called `.trans()` before dividing by the determinant.
        Self { data: [
             (m04 * m08 - m05 * m07), -(m01 * m08 - m02 * m07),  (m01 * m05 - m02 * m04),
            -(m03 * m08 - m05 * m06),  (m00 * m08 - m02 * m06), -(m00 * m05 - m02 * m03),
             (m03 * m07 - m04 * m06), -(m00 * m07 - m01 * m06),  (m00 * m04 - m01 * m03),
        ] } / self.det()
    }
}

impl ops::Mul for Mat3 {
    type Output = Self;

    /// Multiplies two `Mat3`.
    ///
    /// ```
    ///# use math::Mat3;
    /// let m1 = Mat3::IDENT;
    /// let m2 = Mat3::IDENT;
    /// assert_eq!(m1 * m2, Mat3::IDENT);
    /// ```
    #[inline]
    #[rustfmt::skip]
    fn mul(self, other: Self) -> Self::Output {
        let [
            m00, m01, m02,
            m03, m04, m05,
            m06, m07, m08,
        ] = self.data;

        let [
            o00, o01, o02,
            o03, o04, o05,
            o06, o07, o08,
        ] = other.data;

        Self { data: [
            m00 * o00 + m01 * o03 + m02 * o06, // m00
            m03 * o00 + m04 * o03 + m05 * o06, // m03
            m06 * o00 + m07 * o03 + m08 * o06, // m06

            m00 * o01 + m01 * o04 + m02 * o07, // m01
            m03 * o01 + m04 * o04 + m05 * o07, // m04
            m06 * o01 + m07 * o04 + m08 * o07, // m07

            m00 * o02 + m01 * o05 + m02 * o08, // m02
            m03 * o02 + m04 * o05 + m05 * o08, // m05
            m06 * o02 + m07 * o05 + m08 * o08, // m08
        ] }
    }
}

impl ops::Div<f32> for Mat3 {
    type Output = Self;

    #[inline]
    #[rustfmt::skip]
    fn div(mut self, d: f32) -> Self::Output {
        for m in self.data.iter_mut() {
            *m /= d;
        }

        Self { data: self.data }
    }
}

impl ops::Mul<Vec3> for Mat3 {
    type Output = Vec3;

    /// Multiplies `Mat3` times a `Vec3`.
    ///
    /// ```
    ///# use math::{Mat3, Vec3};
    /// let m = Mat3::IDENT;
    /// let v = Vec3::new(3., 4., 5.);
    /// assert_eq!(m * v, v);
    /// ```
    #[inline]
    #[rustfmt::skip]
    fn mul(self, Vec3 { x, y, z }: Vec3) -> Self::Output {
        let [
            m00, m01, m02,
            m03, m04, m05,
            m06, m07, m08,
        ] = self.data;

        Vec3::new(
            m00 * x + m01 * y + m02 * z,
            m03 * x + m04 * y + m05 * z,
            m06 * x + m07 * y + m08 * z,
        )
    }
}

use std::fmt;
impl fmt::Debug for Mat3 {
    #[inline]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        writeln!(
            f,
            "[{:?}, {:?}, {:?}]\n",
            self.data[0], self.data[1], self.data[2]
        )?;
        writeln!(
            f,
            "[{:?}, {:?}, {:?}]\n",
            self.data[3], self.data[4], self.data[5]
        )?;
        writeln!(
            f,
            "[{:?}, {:?}, {:?}]\n",
            self.data[6], self.data[7], self.data[8]
        )
    }
}

impl From<[f32; 9]> for Mat3 {
    #[inline]
    fn from(data: [f32; 9]) -> Self {
        Self { data }
    }
}

impl std::ops::Index<usize> for Mat3 {
    type Output = [f32];

    fn index(&self, row: usize) -> &Self::Output {
        assert!(row < 3);

        let start = row * 3;
        let end = start + 3;

        &self.data[start..end]
    }
}

impl std::ops::IndexMut<usize> for Mat3 {
    fn index_mut(&mut self, row: usize) -> &mut Self::Output {
        assert!(row < 3);

        let start = row * 3;
        let end = start + 3;

        &mut self.data[start..end]
    }
}
