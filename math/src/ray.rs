use super::Vec3;

#[derive(Clone, Copy, Debug)]
pub struct Ray {
    pub dir: Vec3,
    pub origin: Vec3,
    pub dir_inv: Vec3,
}

impl Ray {
    #[inline]
    pub fn new(origin: Vec3, dir: Vec3) -> Self {
        Self {
            origin,
            dir,
            dir_inv: dir.inv(),
        }
    }

    #[inline]
    pub fn create_vec(&self, t: f32) -> Vec3 {
        self.origin + self.dir * t
    }
}
