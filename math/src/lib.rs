#![warn(rust_2018_idioms)]

mod vec;
pub use vec::{Vec2, Vec3, Vec4};

mod ray;
pub use ray::Ray;

mod mat;
pub use mat::{Mat3, Mat4};

mod bounding;
pub use bounding::{BoundingBox, BoundingBox2D};

pub mod bvh;

/// A trait for types that implement the dot or scalar product.
pub trait DotProduct {
    /// The output of the dot product.
    type Output;

    /// Calculates the dot product.
    fn dot(self, other: Self) -> Self::Output;
}

/// A trait for types that implement the cross product.
pub trait CrossProduct {
    /// The output of the cross product.
    type Output;

    /// Calculates the cross product.
    fn cross(self, other: Self) -> Self::Output;
}

pub trait Length {
    type Output;

    fn len(&self) -> Self::Output;
}

pub trait Lerp {
    type Output;

    fn lerp(self, other: Self, rate: f32) -> Self::Output;
}

impl<T: std::ops::Add<Output = T> + std::ops::Mul<f32, Output = T>> Lerp for T {
    type Output = Self;

    #[inline]
    fn lerp(self, other: Self, rate: f32) -> Self::Output {
        (self * (1. - rate)) + (other * rate)
    }
}
