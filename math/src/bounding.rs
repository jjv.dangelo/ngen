use crate::{Ray, Vec2, Vec3};

#[derive(Clone, Copy)]
pub struct BoundingBox2D {
    pub min: Vec2,
    pub max: Vec2,
}

impl BoundingBox2D {
    #[inline]
    const fn new(min: Vec2, max: Vec2) -> Self {
        Self { min, max }
    }

    #[inline]
    pub fn constains_point(self, point: Vec2) -> bool {
        self.min <= point && self.max >= point
    }

    #[inline]
    pub fn overlaps(self, other: Self) -> bool {
        self.min <= other.max && self.max >= other.min
    }

    #[inline]
    pub fn contains(self, other: Self) -> bool {
        other.min <= self.max
            && other.min >= self.min
            && other.max <= self.max
            && other.max >= self.min
    }

    #[inline]
    // TODO: Need a 2D ray
    pub fn intersects(self, ray: Ray) -> Option<f32> {
        let min = Vec3::new(self.min.x, self.min.y, 0.);
        let max = Vec3::new(self.min.x, self.min.y, 0.);
        let min = min - ray.origin;
        let max = max - ray.origin;

        let t1 = min.x * ray.dir_inv.x;
        let t2 = max.x * ray.dir_inv.x;

        let mut tmin = t1.min(t2);
        let mut tmax = t1.max(t2);

        let t1 = min.y * ray.dir_inv.y;
        let t2 = max.y * ray.dir_inv.y;

        tmin = tmin.max(t1.min(t2));
        tmax = tmax.min(t1.max(t2));

        if tmax > tmin.max(0.) {
            Some(tmin.max(0.))
        } else {
            None
        }
    }

    #[inline]
    pub fn width(&self) -> f32 {
        self.max.x - self.min.x
    }

    #[inline]
    pub fn height(&self) -> f32 {
        self.max.y - self.min.y
    }

    #[inline]
    pub fn perimeter(&self) -> f32 {
        2. * (self.width() + self.height())
    }

    #[inline]
    pub fn combine(self, other: &Self) -> Self {
        let min = {
            let x = self.min.x.min(other.min.x);
            let y = self.min.y.min(other.min.y);

            (x, y).into()
        };

        let max = {
            let x = self.max.x.max(other.max.x);
            let y = self.max.y.max(other.max.y);

            (x, y).into()
        };

        Self::new(min, max)
    }

    #[inline]
    pub fn translate(&mut self, offset: Vec2) -> &mut Self {
        self.min -= offset;
        self.max -= offset;
        self
    }

    #[inline]
    pub fn scale(&mut self, scalar: Vec2) -> &mut Self {
        self.min.x *= scalar.x;
        self.min.y *= scalar.y;

        self.max.x *= scalar.x;
        self.max.y *= scalar.y;

        self
    }

    #[inline]
    pub const fn min(&self) -> Vec2 {
        self.min
    }

    #[inline]
    pub const fn max(&self) -> Vec2 {
        self.max
    }

    #[inline]
    pub fn try_create<'a, I: Iterator<Item = &'a Vec2>>(mut points: I) -> Option<Self> {
        points.next().map(|first| {
            let (min, max) = points.fold((*first, *first), |(min, max), next| {
                (
                    (min.x.min(next.x), min.y.min(next.y)).into(),
                    (max.x.max(next.x), max.y.max(next.y)).into(),
                )
            });

            BoundingBox2D::new(min, max)
        })
    }
}

#[derive(Clone, Copy)]
pub struct BoundingBox {
    pub min: Vec3,
    pub max: Vec3,
}

impl BoundingBox {
    #[inline]
    const fn new(min: Vec3, max: Vec3) -> Self {
        Self { min, max }
    }

    #[inline]
    pub fn contains_point(self, point: Vec3) -> bool {
        self.min <= point && self.max >= point
    }

    #[inline]
    pub fn overlaps(self, other: Self) -> bool {
        self.min <= other.max && self.max >= other.min
    }

    #[inline]
    pub fn contains(self, other: Self) -> bool {
        other.min <= self.max
            && other.min >= self.min
            && other.max <= self.max
            && other.max >= self.min
    }

    #[inline]
    pub fn intersects(self, ray: Ray) -> Option<f32> {
        let min = self.min - ray.origin;
        let max = self.max - ray.origin;

        let t1 = min.x * ray.dir_inv.x;
        let t2 = max.x * ray.dir_inv.x;

        let mut tmin = t1.min(t2);
        let mut tmax = t1.max(t2);

        let t1 = min.y * ray.dir_inv.y;
        let t2 = max.y * ray.dir_inv.y;

        tmin = tmin.max(t1.min(t2));
        tmax = tmax.min(t1.max(t2));

        let t1 = min.z * ray.dir_inv.z;
        let t2 = max.z * ray.dir_inv.z;

        tmin = tmin.max(t1.min(t2));
        tmax = tmax.min(t1.max(t2));

        if tmax > tmin.max(0.) {
            Some(tmin.max(0.))
        } else {
            None
        }
    }

    #[inline]
    pub fn width(&self) -> f32 {
        self.max.x - self.min.x
    }

    #[inline]
    pub fn height(&self) -> f32 {
        self.max.y - self.min.y
    }

    #[inline]
    pub fn depth(&self) -> f32 {
        self.max.z - self.min.z
    }

    #[inline]
    pub fn perimeter(&self) -> f32 {
        4. * (self.width() + self.height() + self.depth())
    }

    #[inline]
    pub fn combine(self, other: Self) -> Self {
        let min = {
            let x = self.min.x.min(other.min.x);
            let y = self.min.y.min(other.min.y);
            let z = self.min.z.min(other.min.z);

            (x, y, z).into()
        };

        let max = {
            let x = self.max.x.max(other.max.x);
            let y = self.max.y.max(other.max.y);
            let z = self.max.z.max(other.max.z);

            (x, y, z).into()
        };

        Self::new(min, max)
    }

    #[inline]
    pub fn translate(&mut self, offset: Vec3) -> &mut Self {
        self.min += offset;
        self.max += offset;
        self
    }

    #[inline]
    pub fn scale(&mut self, scalar: Vec3) -> &mut Self {
        self.min.x *= scalar.x;
        self.min.y *= scalar.y;
        self.min.z *= scalar.z;

        self.max.x *= scalar.x;
        self.max.y *= scalar.y;
        self.max.z *= scalar.z;

        self
    }

    #[inline]
    pub const fn min(&self) -> Vec3 {
        self.min
    }

    #[inline]
    pub const fn max(&self) -> Vec3 {
        self.max
    }

    #[inline]
    pub fn try_create<'a, I: Iterator<Item = &'a Vec3>>(mut points: I) -> Option<Self> {
        points.next().map(|first| {
            let (min, max) = points.fold((*first, *first), |(min, max), next| {
                (
                    (min.x.min(next.x), min.y.min(next.y), min.z.min(next.z)).into(),
                    (max.x.max(next.x), max.y.max(next.y), max.z.max(next.z)).into(),
                )
            });

            BoundingBox::new(min, max)
        })
    }
}
