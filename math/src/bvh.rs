//! An auto-balancing bounding volume hierarchy (BVH) using an AVL-like tree structure, heavily inspired by
//! (box2d)[https://github.com/erincatto/Box2D/tree/master/Box2D/Collision].

use crate::{BoundingBox, Ray};

/// A height-aware node the contains a (fat, axis-aligned) bounding box, and points to its two children.
struct Node {
    /// A bounding box that contains its children.
    bb: BoundingBox,

    /// The height of the node, taken as the max height between the left and right nodes.
    height: i32,

    /// Entry in the tree to the left child.
    left: usize,

    /// Entry in the tree to the right child.
    right: usize,

    /// The Node's parent node.
    parent: Option<usize>,
}

/// A struct that defines a leaf node, its associated bounding box, and its contents.
struct Leaf<T> {
    /// The Leaf's (axis-aligned) bounding box.
    bb: BoundingBox,

    /// The Leaf's parent node.
    parent: Option<usize>,

    /// The value of the Leaf node.
    entry: T,
}

/// A struct that points to the next free node, if one exists; otherwise None.
struct Empty {
    /// The next free node, if one exists.
    next: Option<usize>,
}

enum Entry<T> {
    Node(Node),
    Empty(Empty),
    Leaf(Leaf<T>),
}

#[derive(Clone, Copy, Debug)]
/// A new-type that wraps the index into the tree of an inserted Entry::Leaf.
pub struct Index(usize);

/// A tree structure for storing a BVH.
pub struct Tree<T> {
    /// The number of internal entries in the tree.
    nodes: usize,

    /// The total count of leafs (inserted items) in the tree.
    count: usize,

    /// The entries that comprise the tree.
    tree: Vec<Entry<T>>,

    /// The index to thee root node, if it exists.
    root: Option<usize>,

    /// The index, if it exists, to the next free node.
    free: Option<usize>,
}

impl<T> Entry<T> {
    fn new_leaf(bb: BoundingBox, entry: T, parent: Option<usize>) -> Self {
        Entry::Leaf(Leaf { bb, entry, parent })
    }

    fn compute_cost(&self, bb: BoundingBox, inheritance_cost: f32) -> f32 {
        match self {
            Entry::Empty(_) => panic!("Invalid tree. Can't compute cost."),

            Entry::Leaf(ref leaf) => {
                let bb = leaf.bb.combine(bb);
                bb.perimeter() + inheritance_cost
            }

            Entry::Node(ref node) => {
                let bb = node.bb.combine(bb);
                let old_perimeter = node.bb.perimeter();
                let new_perimeter = bb.perimeter();
                new_perimeter - old_perimeter + inheritance_cost
            }
        }
    }

    fn parent(&self) -> Option<usize> {
        match self {
            // TODO: Should this just return None?
            Entry::Empty(_) => panic!("Invalid tree. Can't get parent."),

            Entry::Leaf(ref leaf) => leaf.parent,
            Entry::Node(ref node) => node.parent,
        }
    }

    fn bb(&self) -> BoundingBox {
        match self {
            Entry::Node(node) => node.bb,
            Entry::Leaf(leaf) => leaf.bb,
            Entry::Empty(_) => panic!("Invalid tree. Can't get bounding box."),
        }
    }

    fn height(&self) -> i32 {
        match self {
            Entry::Node(node) => node.height,
            Entry::Leaf(_) => 0,
            Entry::Empty(_) => -1,
        }
    }

    /// Sets the parent of a given node. Panics if the entry is Empty.
    fn set_parent(&mut self, parent: Option<usize>) {
        match self {
            Entry::Leaf(ref mut leaf) => leaf.parent = parent,
            Entry::Node(ref mut node) => node.parent = parent,
            Entry::Empty(_) => panic!("Invalid tree. Can't set parent for an empty node."),
        }
    }
}

impl<T: Copy> Tree<T> {
    /// Creates a new tree.
    pub fn new() -> Self {
        Self::with_capacity(32)
    }

    /// Creates a new tree with the given capacity.
    pub fn with_capacity(capacity: usize) -> Self {
        let capacity = capacity * 2 - 1;
        let mut tree = Vec::with_capacity(capacity);
        for i in 0..capacity - 1 {
            tree.push(Entry::Empty(Empty { next: Some(i + 1) }));
        }
        tree.push(Entry::Empty(Empty { next: None }));

        Self {
            nodes: 0,
            count: 0,
            root: None,
            free: Some(0),
            tree,
        }
    }

    /// Allocates a new node, growing the tree if needed.
    fn allocate(&mut self) -> usize {
        self.nodes += 1;

        match self.free.take() {
            Some(index) => {
                match &self.tree[index] {
                    Entry::Empty(empty) => self.free = empty.next,
                    _ => unreachable!("Invalid tree. Could not allocate. Node node isn't Empty"),
                }

                index
            }

            None => {
                let current_len = self.tree.len();
                let new_len = current_len * 2;
                self.tree.append(
                    &mut (current_len..new_len)
                        .map(|i| Entry::Empty(Empty { next: Some(i + 1) }))
                        .collect(),
                );
                self.tree[new_len - 1] = Entry::Empty(Empty { next: None });
                self.free = Some(current_len + 1);

                current_len
            }
        }
    }

    /// Inserts a new item with its bounding box into the tree.
    pub fn insert(&mut self, entry: T, bb: BoundingBox) -> Index {
        self.count += 1;
        let new_node_id = self.allocate();

        let sibling_id = match self.root {
            // This is the first tentry into the tree. Set as root and return.
            None => {
                debug_assert!(
                    self.nodes == 1,
                    "Invalid AVL tree. Inserting at root but have len {}.",
                    self.nodes
                );

                self.root = Some(new_node_id);
                self.tree[new_node_id] = Entry::new_leaf(bb, entry, None);

                return Index(new_node_id);
            }

            // Walk the tree to find the cheapest sibling next to which we should insert.
            Some(root_id) => {
                let mut index = root_id;
                loop {
                    match &self.tree[index] {
                        Entry::Node(ref node) => {
                            let current_perimeter = node.bb.perimeter();
                            let combined = node.bb.combine(bb);
                            let combined_perimeter = combined.perimeter();
                            let cost = 2. * combined_perimeter;
                            let inheritance_cost = 2. * (combined_perimeter - current_perimeter);

                            let l_child_cost =
                                self.tree[node.left].compute_cost(bb, inheritance_cost);
                            let r_child_cost =
                                self.tree[node.right].compute_cost(bb, inheritance_cost);

                            if cost < l_child_cost && cost < r_child_cost {
                                break index;
                            } else if l_child_cost < r_child_cost {
                                index = node.left;
                            } else {
                                index = node.right;
                            }
                        }

                        Entry::Leaf(_) => break index,

                        Entry::Empty(_) => {
                            panic!("Invalid tree structure. Cannot compute cost of empty node.")
                        }
                    }
                }
            }
        };

        let old_parent = self.tree[sibling_id].parent();

        let new_parent_id = self.allocate();
        let new_parent = Node {
            bb,
            height: 0,
            left: sibling_id,
            right: new_node_id,
            parent: old_parent,
        };

        self.set_parent(sibling_id, Some(new_parent_id));
        self.tree[new_node_id] = Entry::new_leaf(bb, entry, Some(new_parent_id));

        // Point the old parent to the new node, if the sibling wasn't the root node.
        match old_parent {
            // The sibling was the root node.
            None => self.root = Some(new_parent_id),

            // The sibling has a parent, so it isn't the root node.
            Some(old_parent_id) => self.update_child(old_parent_id, sibling_id, new_parent_id),
        }

        self.tree[new_parent_id] = Entry::Node(new_parent);

        // Walk back up the tree and fix the height.
        let mut next = new_parent_id;
        loop {
            let (left_id, right_id) = self.get_children(next);
            let height = self.calc_height(left_id, right_id);
            let bb = self.calc_bb(left_id, right_id);
            let mut node = self.get_node_mut(next);
            node.height = height;
            node.bb = bb;

            let index = self.balance(next);
            match self.tree[index].parent() {
                Some(parent_id) => next = parent_id,
                None => break Index(new_node_id),
            }
        }
    }

    pub fn remove(&mut self, Index(index): Index) {
        let parent = self.get_parent_id(index);
        self.empty_node(index);
        self.count -= 1;

        if let Some(parent_id) = parent {
            match &self.tree[parent_id] {
                Entry::Node(parent) => {
                    let sibling_id = if parent.left == index {
                        parent.right
                    } else {
                        debug_assert_eq!(parent.right, index);
                        parent.left
                    };

                    match parent.parent {
                        // We have a grandparent and so we need to move the
                        // sibling to the parent location
                        Some(grandparent_id) => {
                            self.replace_child(parent_id, sibling_id);
                            self.set_parent(sibling_id, Some(grandparent_id));
                            self.empty_node(parent_id);

                            let mut next = grandparent_id;
                            loop {
                                let (left_id, right_id) = self.get_children(next);
                                let height = self.calc_height(left_id, right_id);
                                let bb = self.calc_bb(left_id, right_id);
                                let mut node = self.get_node_mut(next);
                                node.height = height;
                                node.bb = bb;

                                let index = self.balance(next);
                                match self.tree[index].parent() {
                                    Some(parent_id) => next = parent_id,
                                    None => break,
                                }
                            }
                        }

                        // This was the root. Move the siblinc to its position.
                        None => {
                            assert_eq!(self.root, Some(parent_id));
                            self.root = Some(sibling_id);
                            self.set_parent(sibling_id, None);
                            self.empty_node(parent_id);
                        }
                    }
                }

                Entry::Leaf(_) | Entry::Empty(_) => unreachable!(),
            }
        } else {
            // This must be the root node.
            assert_eq!(self.root, Some(index));
            self.root = None;
        }
    }

    fn query<C, F>(&self, bb: BoundingBox, check: C, mut callback: F)
    where
        C: Fn(BoundingBox, BoundingBox) -> bool,
        F: FnMut(&T) -> bool,
    {
        for (entry, _) in self.iter().filter(|(_, leaf_bb)| check(bb, *leaf_bb)) {
            if !callback(entry) {
                break;
            }
        }
    }

    /// Returns an iterator over each of the leaf nodes in the tree.
    pub fn iter(&self) -> TreeIter<'_, T> {
        TreeIter::new(self)
    }

    /// Walks through the tree to find entries that overlap with the supplied
    /// (axis-aligned) bounding box.
    pub fn query_overlaps<F>(&self, bb: BoundingBox, callback: F)
    where
        F: FnMut(&T) -> bool,
    {
        self.query(bb, |left, right| left.overlaps(right), callback)
    }

    /// Walks through the tree to find entries that are contained by the supplied
    /// (axis-aligned) bounding box.
    pub fn query_contains<F>(&self, bb: BoundingBox, callback: F)
    where
        F: FnMut(&T) -> bool,
    {
        self.query(bb, |left, right| left.contains(right), callback)
    }

    /// Casts a ray to see if any of the bounding boxes are hit.
    pub fn cast(&self, ray: Ray) -> TreeRayIter<'_, T> {
        TreeRayIter::new(self, ray)
    }

    /// Casts a ray and picks the closest target hit, if any.
    pub fn cast_closest(&self, ray: Ray) -> Option<(&T, f32)> {
        self.cast(ray)
            .fold(None, |found, (entry, dist)| match found {
                None => Some((entry, dist)),
                Some((_, current)) if current > dist => Some((entry, dist)),
                _ => found,
            })
    }

    fn calc_height(&self, left: usize, right: usize) -> i32 {
        let left_height = self.tree[left].height();
        let right_height = self.tree[right].height();
        let result = 1 + left_height.max(right_height);
        result
    }

    fn calc_balance(&self, left: usize, right: usize) -> i32 {
        let left_height = self.tree[left].height();
        let right_height = self.tree[right].height();
        let result = left_height - right_height;
        result
    }

    fn calc_bb(&self, left: usize, right: usize) -> BoundingBox {
        let left_bb = self.tree[left].bb();
        let right_bb = self.tree[right].bb();
        let result = left_bb.combine(right_bb);
        result
    }

    fn set_parent(&mut self, node_id: usize, parent: Option<usize>) {
        let node = &mut self.tree[node_id];
        node.set_parent(parent);
    }

    fn get_parent_id(&self, node_id: usize) -> Option<usize> {
        match &self.tree[node_id] {
            Entry::Empty(_) => None,
            Entry::Node(ref node) => node.parent,
            Entry::Leaf(ref leaf) => leaf.parent,
        }
    }

    fn update_child(&mut self, parent: usize, old_child: usize, new_child: usize) {
        let parent = self.get_node_mut(parent);
        if parent.left == old_child {
            parent.left = new_child;
        } else {
            debug_assert_eq!(parent.right, old_child);
            parent.right = new_child;
        }
    }

    fn replace_child(&mut self, child_id: usize, new_child_id: usize) {
        match self.tree[child_id].parent() {
            Some(parent) => self.update_child(parent, child_id, new_child_id),
            None => match self.root {
                Some(root_id) if root_id == child_id => {
                    self.root = Some(new_child_id);
                }

                Some(_) => panic!("Node doesn't have a parent and doesn't match the root."),
                None => panic!("Invalid tree. Root node doesn't exist."),
            },
        }
    }

    fn get_node_mut(&mut self, node_id: usize) -> &mut Node {
        match &mut self.tree[node_id] {
            Entry::Node(ref mut node) => node,
            Entry::Leaf(_) => panic!("Expected Node at {}; found Leaf.", node_id),
            Entry::Empty(_) => panic!("Expected Node at {}; found Empty.", node_id),
        }
    }

    fn get_node(&self, node_id: usize) -> &Node {
        match &self.tree[node_id] {
            Entry::Node(ref node) => node,
            Entry::Leaf(_) => panic!("Expected Node at {}; found Leaf.", node_id),
            Entry::Empty(_) => panic!("Expected Node at {}; found Empty.", node_id),
        }
    }

    fn get_children(&self, node_id: usize) -> (usize, usize) {
        let node = self.get_node(node_id);
        (node.left, node.right)
    }

    fn empty_node(&mut self, node_id: usize) {
        let empty = Empty { next: self.free };
        self.tree[node_id] = Entry::Empty(empty);
        self.free = Some(node_id);
        self.nodes -= 1;
    }

    fn rotate_left(&mut self, node_id: usize) -> usize {
        let (left_id, right_id) = self.get_children(node_id);
        let (left_grandchild_id, right_grandchild_id) = self.get_children(right_id);

        let parent = self.tree[node_id].parent();
        let new_height = self.calc_height(left_id, left_grandchild_id);
        let new_bb = self.calc_bb(left_id, left_grandchild_id);

        self.set_parent(left_grandchild_id, Some(node_id));
        self.set_parent(node_id, Some(right_id));

        let node = self.get_node_mut(node_id);
        node.right = left_grandchild_id;
        node.height = new_height;
        node.bb = new_bb;

        match parent {
            Some(grandparent) => self.update_child(grandparent, node_id, right_id),

            None => match self.root {
                Some(root_id) if root_id == node_id => {
                    self.root = Some(right_id);
                }

                Some(_) => panic!("Node doesn't have a parent, and doesn't match the root."),

                None => panic!("Invalid tree. No root node."),
            },
        }

        let right_height = self.calc_height(node_id, right_grandchild_id);
        let new_bb = self.calc_bb(node_id, right_grandchild_id);
        let right = self.get_node_mut(right_id);
        right.parent = parent;
        right.height = right_height;
        right.bb = new_bb;

        right.left = node_id;

        right_id
    }

    fn rotate_right(&mut self, node_id: usize) -> usize {
        let (left_id, right_id) = self.get_children(node_id);
        let (left_grandchild_id, right_grandchild_id) = self.get_children(left_id);

        let parent = self.tree[node_id].parent();
        let new_height = self.calc_height(right_grandchild_id, right_id);
        let new_bb = self.calc_bb(right_grandchild_id, right_id);

        self.set_parent(right_grandchild_id, Some(node_id));
        self.set_parent(node_id, Some(left_id));

        let node = self.get_node_mut(node_id);
        node.left = right_grandchild_id;
        node.height = new_height;
        node.bb = new_bb;

        match parent {
            Some(grandparent) => self.update_child(grandparent, node_id, left_id),

            None => match self.root {
                Some(root_id) if root_id == node_id => {
                    self.root = Some(left_id);
                }

                Some(_) => panic!("Node doesn't have a parent, and doesn't match the root."),
                None => panic!("Invalid tree. No root node."),
            },
        }

        let left_height = self.calc_height(left_grandchild_id, node_id);
        let new_bb = self.calc_bb(left_grandchild_id, node_id);
        let left = self.get_node_mut(left_id);
        left.parent = parent;
        left.height = left_height;
        left.bb = new_bb;

        left.right = node_id;

        left_id
    }

    fn balance(&mut self, parent_id: usize) -> usize {
        let (left_id, right_id) = self.get_children(parent_id);
        let balance = self.calc_balance(left_id, right_id);

        if balance > 1 {
            debug_assert_eq!(balance, 2);

            let (left_grandchild_id, right_grandchild_id) = self.get_children(left_id);
            let left_balance = self.calc_balance(left_grandchild_id, right_grandchild_id);
            if left_balance < 0 {
                self.rotate_left(left_id);
            }

            self.rotate_right(parent_id)
        } else if balance < -1 {
            debug_assert_eq!(balance, -2);

            let (left_grandchild_id, right_grandchild_id) = self.get_children(right_id);
            let right_balance = self.calc_balance(left_grandchild_id, right_grandchild_id);
            if right_balance > 0 {
                self.rotate_right(right_id);
            }

            self.rotate_left(parent_id)
        } else {
            parent_id
        }
    }

    pub fn len(&self) -> usize {
        self.count
    }
}

pub enum Continuation {
    Continue,
    Break,
}

impl From<()> for Continuation {
    fn from(_: ()) -> Self {
        Self::Continue
    }
}

impl From<bool> for Continuation {
    fn from(val: bool) -> Self {
        if val {
            Self::Continue
        } else {
            Self::Break
        }
    }
}

pub struct TreeIter<'a, T> {
    tree: &'a Vec<Entry<T>>,
    nodes: Vec<usize>,
}

impl<'a, T> TreeIter<'a, T> {
    fn new(tree: &'a Tree<T>) -> Self {
        let nodes = tree.root.map(|id| vec![id]).unwrap_or(vec![]);
        let tree = &tree.tree;

        Self { tree, nodes }
    }
}

impl<'a, T> Iterator for TreeIter<'a, T> {
    type Item = (&'a T, BoundingBox);

    #[inline]
    fn next(&mut self) -> Option<Self::Item> {
        while let Some(node_id) = self.nodes.pop() {
            match &self.tree[node_id] {
                Entry::Leaf(ref leaf) => {
                    return Some((&leaf.entry, leaf.bb));
                }

                Entry::Node(ref node) => {
                    self.nodes.push(node.left);
                    self.nodes.push(node.right);
                }

                Entry::Empty(_) => (),
            }
        }

        return None;
    }
}

pub struct TreeRayIter<'a, T> {
    tree: &'a Vec<Entry<T>>,
    nodes: Vec<usize>,
    ray: Ray,
}

impl<'a, T> TreeRayIter<'a, T> {
    fn new(tree: &'a Tree<T>, ray: Ray) -> Self {
        let nodes = tree.root.map(|id| vec![id]).unwrap_or(vec![]);
        let tree = &tree.tree;

        Self { tree, nodes, ray }
    }
}

impl<'a, T> Iterator for TreeRayIter<'a, T> {
    type Item = (&'a T, f32);

    #[inline]
    fn next(&mut self) -> Option<Self::Item> {
        while let Some(node_id) = self.nodes.pop() {
            match &self.tree[node_id] {
                Entry::Leaf(ref leaf) => {
                    if let Some(dist) = leaf.bb.intersects(self.ray) {
                        return Some((&leaf.entry, dist));
                    }
                }

                Entry::Node(ref node) => {
                    if let Some(_) = node.bb.intersects(self.ray) {
                        self.nodes.push(node.right);
                        self.nodes.push(node.left);
                    }
                }

                _ => (),
            }
        }

        return None;
    }
}
