use std::sync::{
    atomic::{fence, AtomicUsize},
    Arc, Condvar, Mutex,
};
use std::thread;

struct Semaphore {
    lock: Mutex<usize>,
    cvar: Condvar,
}

impl Semaphore {
    pub fn new(initial: usize) -> Self {
        Self {
            lock: Mutex::new(initial),
            cvar: Condvar::new(),
        }
    }

    pub fn wait_one(&self) {
        let mut count = self.lock.lock().unwrap();
        while *count <= 0 {
            count = self.cvar.wait(count).unwrap();
        }
        *count -= 1;
    }

    pub fn release(&self) {
        *self.lock.lock().unwrap() += 1;
        self.cvar.notify_one();
    }
}

struct WorkEntry<T> {
    data: T,
    callback: Option<fn(&mut T)>,
}

impl<T> WorkEntry<T> {
    fn execute(&mut self) {
        self.callback.map(|f| f(&mut self.data));
    }
}

pub const BUFFER_SIZE: usize = 1 << 10;
pub const BUFFER_MASK: usize = BUFFER_SIZE - 1;
pub const ORDERING: std::sync::atomic::Ordering = std::sync::atomic::Ordering::SeqCst;

pub struct WorkQueue<T: Sized + Send + Sync + 'static> {
    head: Arc<AtomicUsize>,
    tail: Arc<AtomicUsize>,

    completed: Arc<AtomicUsize>,
    goal: Arc<AtomicUsize>,

    semaphore: Arc<Semaphore>,

    entries: Arc<[WorkEntry<T>; BUFFER_SIZE]>,
}

impl<T: Sized + Send + Sync + 'static> WorkQueue<T> {
    pub fn new(num_workers: usize) -> Self {
        let queue = Self {
            head: Arc::new(AtomicUsize::new(0)),
            tail: Arc::new(AtomicUsize::new(0)),

            completed: Arc::new(AtomicUsize::new(0)),
            goal: Arc::new(AtomicUsize::new(0)),

            semaphore: Arc::new(Semaphore::new(0)),

            entries: Arc::new(unsafe { std::mem::zeroed() }),
        };

        for _ in 0..num_workers {
            queue.spawn_thread();
        }

        queue
    }

    fn clone(&self) -> Self {
        Self {
            head: self.head.clone(),
            tail: self.tail.clone(),

            completed: self.completed.clone(),
            goal: self.goal.clone(),

            semaphore: self.semaphore.clone(),

            entries: self.entries.clone(),
        }
    }

    pub fn push_work(&mut self, callback: Option<fn(&mut T)>, data: T) {
        let new_head = (self.head.load(ORDERING) + 1) & BUFFER_MASK;

        assert_ne!(new_head, self.head.load(ORDERING));

        while {
            // Spin wait while we don't have room in the queue (for now, at least).
            self.head.load(ORDERING) - self.tail.load(ORDERING) > BUFFER_SIZE
        } {}

        let entry = self.get_entry(self.head.load(ORDERING));

        entry.callback = callback;
        entry.data = data;

        self.goal.fetch_add(1, ORDERING);

        fence(ORDERING);

        self.head.store(new_head, ORDERING);
        self.semaphore.release();
    }

    fn spawn_thread(&self) {
        let mut queue = self.clone();

        thread::spawn(move || loop {
            let should_sleep = queue.process_next();
            if should_sleep {
                queue.semaphore.wait_one();
            }
        });
    }

    fn get_entry(&mut self, index: usize) -> &mut WorkEntry<T> {
        let entry = unsafe {
            let entry = &self.entries[index];
            let entry_ptr = entry as *const _ as *mut WorkEntry<T>;
            &mut *entry_ptr
        };

        entry
    }

    fn process_next(&mut self) -> bool {
        let mut found_entry = false;

        let tail = self.tail.load(ORDERING);
        let new_tail = (tail + 1) & BUFFER_MASK;

        if tail != self.head.load(ORDERING) {
            let index = self.tail.compare_and_swap(tail, new_tail, ORDERING);

            if index == tail {
                let entry = self.get_entry(index);
                entry.execute();

                self.completed.fetch_add(1, ORDERING);
            }
        } else {
            found_entry = false;
        }

        found_entry
    }

    pub fn complete_work(&mut self) {
        while self.goal.load(ORDERING) != self.completed.load(ORDERING) {
            self.process_next();
        }

        self.goal.store(0, ORDERING);
        self.completed.store(0, ORDERING);
    }
}
