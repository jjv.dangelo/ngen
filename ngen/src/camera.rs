use math::{Mat4, Ray, Vec2, Vec3, Vec4};

#[derive(Clone, Copy)]
pub struct Camera {
    pub eye: Vec3,
    pub look_at: Vec3,
    pub up: Vec3,
    pub window_dim: Vec2,
    pub aspect_ratio: f32,
    pub fov: f32,
    pub near: f32,
    pub far: f32,
}

impl Camera {
    #[inline]
    pub fn new(eye: Vec3, window_dim: Vec2) -> Self {
        Self {
            eye,
            look_at: -Vec3::Z,
            up: Vec3::Y,
            window_dim,
            aspect_ratio: window_dim.x / window_dim.y,
            fov: std::f32::consts::FRAC_PI_2,
            near: 0.1,
            far: 1000.,
        }
    }

    #[inline]
    pub fn set_window_dim(&mut self, window_dim: Vec2) {
        let aspect_ratio = window_dim.x / window_dim.y;
        self.aspect_ratio = aspect_ratio;
        self.window_dim = window_dim;
    }

    #[inline]
    fn view(&self) -> Mat4 {
        Mat4::look_at(self.eye, self.look_at, self.up)
    }

    #[inline]
    fn projection(&self) -> Mat4 {
        Mat4::perspective(self.fov, self.aspect_ratio, self.near, self.far)
    }

    #[inline]
    pub fn perspective_matrix(&self) -> Mat4 {
        // self.projection() * self.view()
        self.view() * self.projection()
    }

    #[inline]
    pub fn cast_ray(&self, x: f32, y: f32) -> Ray {
        let Vec2 {
            x: width,
            y: height,
        } = self.window_dim;

        let proj = self.projection();
        let inv_view = self.view().inverse();

        let dx = (2. * x / width - 1.) / -proj.index(0, 0);
        let dy = (-2. * y / height + 1.) / proj.index(1, 1);
        let p1 = inv_view * Vec4::new(0., 0., 0., 1.);
        let p2 = inv_view * Vec4::new(1000. * dx, 1000. * dy, 1000., 1.); // TODO: Hardcoded far = 1000.

        let dir = (p2.to_vec3() - p1.to_vec3()).normalize();
        let origin = p1.to_vec3();
        Ray::new(origin, dir)
    }
}
