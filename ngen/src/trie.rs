#![macro_use]

use std::collections::HashMap;

pub struct Trie<T> {
    data: Option<T>,
    children: HashMap<char, Trie<T>>,
}

impl<T> Trie<T> {
    /// Creates a new `Trie<T>`.
    ///
    /// ```
    ///# use ngen::trie::Trie;
    /// let trie: Trie<u32> = Trie::new();
    /// ```
    pub fn new() -> Self {
        Self {
            data: None,
            children: HashMap::new(),
        }
    }

    /// Inserts an element into the `Trie<T>`.
    ///
    /// ```
    ///# use ngen::trie::Trie;
    /// let mut trie = Trie::new();
    /// trie.insert("some key", 42);
    /// ```
    pub fn insert(&mut self, string: &str, data: T) -> Option<T> {
        let mut trie = self;

        for c in string.chars() {
            trie = trie.children.entry(c).or_insert(Trie::new());
        }

        let old_data = trie.data.take();
        trie.data = Some(data);

        old_data
    }

    /// Looks for an existing elemnt in the `Trie<T>`.
    ///
    /// ```
    ///# use ngen::trie::Trie;
    /// let mut trie = Trie::new();
    /// trie.insert("some key", 42);
    /// assert_eq!(trie.find("some other key"), None);
    /// assert_eq!(trie.find("some key"), Some(&42));
    /// ```
    pub fn find(&self, string: &str) -> Option<&T> {
        let mut trie = self;

        for c in string.chars() {
            if let Some(child) = trie.children.get(&c) {
                trie = &child;
            } else {
                return None;
            }
        }

        trie.data.as_ref()
    }

    /// Returns a sub `Trie<T>` for faster sub-lookups.
    ///
    /// ```
    ///# use ngen::trie::Trie;
    /// let mut trie = Trie::new();
    /// trie.insert("some key", 42);
    /// trie.insert("some other key", 7);
    ///
    /// let sub = trie.sub("some ").unwrap();
    /// assert_eq!(sub.find("key"), Some(&42));
    /// assert_eq!(sub.find("other key"), Some(&7));
    /// assert_eq!(sub.find("non-existant key"), None);
    /// ```
    pub fn sub(&self, key: &str) -> Option<&Trie<T>> {
        let mut trie = self;

        for c in key.chars() {
            if let Some(child) = trie.children.get(&c) {
                trie = &child;
            } else {
                return None;
            }
        }

        Some(trie)
    }
}

pub trait IterVariants: Sized {
    fn iter_variants() -> Vec<Self>;
}

use std::iter;
impl<'a, T> iter::FromIterator<(&'a str, T)> for Trie<T> {
    fn from_iter<I>(iter: I) -> Self
    where
        I: iter::IntoIterator<Item = (&'a str, T)>,
    {
        let mut trie = Self::new();

        for (s, t) in iter {
            trie.insert(s, t);
        }

        trie
    }
}

impl<'a, T> iter::FromIterator<T> for Trie<T>
where
    T: AsRef<str> + Copy,
{
    fn from_iter<I>(iter: I) -> Self
    where
        I: iter::IntoIterator<Item = T>,
    {
        let mut trie = Self::new();

        for t in iter {
            trie.insert(t.as_ref(), t);
        }

        trie
    }
}

impl<'ident, R, I> From<I> for Trie<R>
where
    R: AsRef<&'ident str>,
    I: IntoIterator<Item = R>,
{
    #[inline]
    fn from(i: I) -> Self {
        let mut trie = Self::new();

        for item in i {
            trie.insert(item.as_ref(), item);
        }

        trie
    }
}

/// A helper macro to create a `Trie<T>` when `T` implements `AsRef<&str>`.
/// `AsRef<&str>` should return the string version of `T` and is the equivalent of manually doing
/// inserts.
///
/// ```nocheck
///# // We cannot import macros into doc tests, so this is marked with `nocheck`.
/// #[derive(Debug, Eq, PartialEq)]
/// enum Token { A, B, C, Longer, }
///
/// impl AsRef<str> for Token {
///     fn as_ref(&self) -> &str {
///         match self {
///             Self::A => &"A",
///             Self::B => &"B",
///             Self::C => &"C",
///             Self::Longer => &"Longer",
///         }
///     }
/// }
///
/// let trie = trie![ Token::A, Token::B, Token::C, ];
/// assert_eq!(trie.find("A"), Some(Token::A));
/// assert_eq!(trie.find("B"), Some(Token::B));
/// assert_eq!(trie.find("C"), Some(Token::C));
/// assert_eq!(trie.find("Longer"), Some(Token::Longer));
/// assert_eq!(trie.find("Z"), None);
/// assert_eq!(trie.find("42"), None);
/// ```
#[macro_export]
macro_rules! trie {
    ($($e:expr),* $(,)*) => {{
        let mut trie = Trie::new();

        $(
            trie.insert($e.as_ref(), $e);
        )*

        trie
    }}
}
