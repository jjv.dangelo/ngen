use super::{
    tokenizer::{Identifier, Token, TokenInfo, Tokenizer},
    Error,
};
use std::fmt;

#[derive(Default)]
/// An intermediary structure when parsing asset info.
pub struct ParsedAssetInfo<'source> {
    pub name: Option<&'source str>,
    pub author: Option<&'source str>,
    pub spritesheet: Option<&'source str>,
    pub description: Option<&'source str>,
    pub tile_width: Option<u32>,
    pub tile_height: Option<u32>,
}

/// An intermediary structure when parsing an asset tag.
pub struct ParsedTag<'source> {
    pub name: &'source str,
    pub value: f32,
}

/// An intermediary structure when parsing an asset entry.
pub struct ParsedAssetEntry<'source> {
    pub asset_type: Identifier<'source>,

    pub height: Option<u32>,
    pub width: Option<u32>,

    pub pos: Option<(u32, u32)>,

    pub tag_start: usize,
    pub tag_count: usize,
}

impl<'source> ParsedAssetEntry<'source> {
    fn new(asset_type: Identifier<'source>, tag_start: usize) -> Self {
        Self {
            asset_type,

            height: None,
            width: None,

            pos: None,

            tag_start,
            tag_count: 0,
        }
    }
}

/// An intermediary structure when parsing asset entries.
pub struct ParsedAssetEntries<'source> {
    pub asset_info: ParsedAssetInfo<'source>,
    pub tags: Vec<ParsedTag<'source>>,
    pub entries: Vec<ParsedAssetEntry<'source>>,
}

/// An intermediary structure when parsing an asset block.
pub struct ParsedAssetBlock<'block, 'source> {
    assets: &'block mut ParsedAssetEntries<'source>,
    current: ParsedAssetEntry<'source>,
}

impl<'block, 'source> ParsedAssetBlock<'block, 'source> {
    fn new(
        assets: &'block mut ParsedAssetEntries<'source>,
        asset_type: Identifier<'source>,
    ) -> Self {
        let current = ParsedAssetEntry::new(asset_type, assets.tags.len());
        Self { assets, current }
    }

    pub fn set_height(&mut self, height: u32) {
        self.current.height.replace(height);
    }

    pub fn set_width(&mut self, width: u32) {
        self.current.width.replace(width);
    }

    pub fn set_pos(&mut self, x: u32, y: u32) {
        self.current.pos.replace((x, y));
    }

    pub fn push_tag(&mut self, name: &'source str, value: f32) {
        self.assets.tags.push(ParsedTag { name, value });
    }

    pub fn end(mut self) {
        let assets = &mut self.assets;
        let mut entry = self.current;

        let count = assets.tags.len() - entry.tag_start;
        entry.tag_count = count;

        assets.entries.push(entry);
    }
}

impl<'source> ParsedAssetEntries<'source> {
    pub fn new() -> Self {
        Self {
            tags: vec![],
            entries: vec![],
            asset_info: ParsedAssetInfo::default(),
        }
    }

    pub fn begin_asset_block(
        &mut self,
        asset_type: Identifier<'source>,
    ) -> ParsedAssetBlock<'_, 'source> {
        ParsedAssetBlock::new(self, asset_type)
    }
}

fn get_token<'source>(
    tokenizer: &mut Tokenizer<'source>,
) -> Result<TokenInfo<'source>, ParserError<'source>> {
    if let Some(token_info) = tokenizer.next() {
        return Ok(token_info);
    } else {
        Err(ParserError::UnexpectedEOS)
    }
}

fn require_token<'source>(
    tokenizer: &mut Tokenizer<'source>,
    token: Token<'_>,
) -> Result<usize, ParserError<'source>> {
    if let Some(token_info) = tokenizer.next() {
        if token_info.token == token {
            Ok(token_info.pos())
        } else {
            Err(ParserError::UnexpectedToken(token_info))
        }
    } else {
        Err(ParserError::UnexpectedEOS)
    }
}

fn require_eq<'source>(tokenizer: &mut Tokenizer<'source>) -> Result<usize, ParserError<'source>> {
    require_token(tokenizer, Token::Equal)
}

fn require_sc<'source>(tokenizer: &mut Tokenizer<'source>) -> Result<usize, ParserError<'source>> {
    require_token(tokenizer, Token::SColon)
}

fn require_com<'source>(tokenizer: &mut Tokenizer<'source>) -> Result<usize, ParserError<'source>> {
    require_token(tokenizer, Token::Comma)
}

fn ff_to_token<'source>(
    tokenizer: &mut Tokenizer<'source>,
    token: Token<'_>,
) -> Result<usize, ParserError<'source>> {
    while let Some(token_info) = tokenizer.next() {
        if token_info.token == token {
            return Ok(token_info.pos());
        }
    }

    Err(ParserError::UnexpectedEOS)
}

fn ff_to_quote<'source>(tokenizer: &mut Tokenizer<'source>) -> Result<usize, ParserError<'source>> {
    ff_to_token(tokenizer, Token::Quote)
}

fn ff_to_sc<'source>(tokenizer: &mut Tokenizer<'source>) -> Result<usize, ParserError<'source>> {
    ff_to_token(tokenizer, Token::SColon)
}

fn parse_string<'source>(
    tokenizer: &mut Tokenizer<'source>,
) -> Result<&'source str, ParserError<'source>> {
    let token_info = get_token(tokenizer)?;
    let mut start = token_info.pos();
    let mut end = tokenizer.pos;

    if let Token::Ident(_) = &token_info.token {
        start = token_info.pos();
    } else if token_info.token == Token::Quote {
        start += 1;
        end = ff_to_quote(tokenizer)?;
    }

    Ok(&tokenizer.source[start..end])
}

fn parse_int<'source>(tokenizer: &mut Tokenizer<'source>) -> Result<u32, ParserError<'source>> {
    let token_info = get_token(tokenizer)?;

    match token_info.token {
        Token::Int(i) => Ok(i),
        _ => Err(ParserError::UnexpectedToken(token_info)),
    }
}

fn parse_float<'source>(tokenizer: &mut Tokenizer<'source>) -> Result<f32, ParserError<'source>> {
    let token_info = get_token(tokenizer)?;

    match token_info.token {
        Token::Int(i) => Ok(i as _),
        Token::Float(f) => Ok(f),
        _ => Err(ParserError::UnexpectedToken(token_info)),
    }
}

fn parse_string_assignment<'source>(
    tokenizer: &mut Tokenizer<'source>,
) -> Result<&'source str, ParserError<'source>> {
    require_eq(tokenizer)?;

    match parse_string(tokenizer) {
        Ok(value) => {
            require_sc(tokenizer)?;
            Ok(value)
        }

        Err(error) => {
            ff_to_sc(tokenizer)?;
            Err(error)
        }
    }
}

fn parse_int_assignment<'source>(
    tokenizer: &mut Tokenizer<'source>,
) -> Result<u32, ParserError<'source>> {
    require_eq(tokenizer)?;

    let value = parse_int(tokenizer)?;
    require_sc(tokenizer)?;

    Ok(value)
}

fn set_width_and_height(assets: &mut ParsedAssetEntries<'_>) -> Result<(), Error> {
    let get_and_set_dim = |mut l: &mut Option<u32>, r: Option<u32>| -> Result<f32, Error> {
        match (&mut l, &r) {
            (None, Some(ref dim)) => {
                l.replace(*dim);
                Ok(*dim as f32)
            }

            (Some(dim), _) => Ok(*dim as f32),

            (None, None) => Err(Error(format!(
                "Dimension not set in asset entry or asset info."
            ))),
        }
    };

    for entry in assets.entries.iter_mut() {
        get_and_set_dim(&mut entry.width, assets.asset_info.tile_width)?;
        get_and_set_dim(&mut entry.height, assets.asset_info.tile_height)?;

        entry
            .pos
            .map(|(x, y)| (x as f32, y as f32))
            .ok_or_else(|| Error(format!("Position not set for {:?}", entry.asset_type)))?;
    }

    Ok(())
}

struct Scanning;
struct ParsingComment;
struct ParsingAssetInfo;
struct ParsingAsset<'source> {
    ident: Identifier<'source>,
}

impl<'source> ParsingAsset<'source> {
    fn new(ident: Identifier<'source>) -> Self {
        Self { ident }
    }
}

pub enum ParserError<'source> {
    UnexpectedToken(TokenInfo<'source>),
    UnexpectedEOS,
}

pub struct ParserErrors<'source> {
    errors: Vec<ParserError<'source>>,
}

impl<'source> fmt::Display for ParserError<'source> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::UnexpectedToken(token_info) => write!(
                f,
                "Unexpected token \"{}\" at line {} col {}.",
                token_info.token,
                token_info.line(),
                token_info.col()
            ),

            Self::UnexpectedEOS => write!(f, "Unexpected end of stream."),
        }
    }
}

impl<'source> fmt::Display for ParserErrors<'source> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        for error in self.errors.iter() {
            writeln!(f, "{}", error)?;
        }

        Ok(())
    }
}

impl<'source> From<Vec<ParserError<'source>>> for ParserErrors<'source> {
    fn from(errors: Vec<ParserError<'source>>) -> Self {
        Self { errors }
    }
}

struct State<'source, S> {
    tokenizer: Tokenizer<'source>,
    asset_entries: ParsedAssetEntries<'source>,
    errors: Vec<ParserError<'source>>,

    state: S,
}

enum ParserState<'source> {
    Scanning(State<'source, Scanning>),
    ParsingAssetInfo(State<'source, ParsingAssetInfo>),
    ParsingAsset(State<'source, ParsingAsset<'source>>),
    ParsingComment(State<'source, ParsingComment>),
    Complete(ParsedAssetEntries<'source>),
    Errored(ParserErrors<'source>),
}

impl<'source> ParserState<'source> {
    fn init(source: &'source str) -> Self {
        ParserState::Scanning(State::scan(source))
    }

    fn parse(mut self) -> Result<ParsedAssetEntries<'source>, ParserErrors<'source>> {
        loop {
            match self {
                Self::Scanning(state) => {
                    self = state.transition();
                }

                Self::ParsingAssetInfo(state) => {
                    self = Self::Scanning(state.into());
                }

                Self::ParsingAsset(state) => {
                    self = Self::Scanning(state.into());
                }

                Self::ParsingComment(state) => {
                    self = Self::Scanning(state.into());
                }

                Self::Complete(assets) => return Ok(assets),

                Self::Errored(errors) => return Err(errors),
            }
        }
    }
}

impl<'source> State<'source, Scanning> {
    fn scan(source: &'source str) -> Self {
        Self {
            tokenizer: Tokenizer::new(source),
            asset_entries: ParsedAssetEntries::new(),
            errors: vec![],

            state: Scanning,
        }
    }

    fn transition(mut self) -> ParserState<'source> {
        if let Some(token_info) = self.tokenizer.next() {
            match token_info.token {
                Token::Ident(Identifier::Info) => ParserState::ParsingAssetInfo(self.into()),

                // Parse an individual asset.
                Token::Ident(ident) => ParserState::ParsingAsset((ident, self).into()),

                // Parse block-style comments.
                Token::FSlash => ParserState::ParsingComment(self.into()),

                _ => {
                    self.errors.push(ParserError::UnexpectedToken(token_info));
                    ParserState::Scanning(self)
                }
            }
        } else if self.errors.len() == 0 {
            if let Err(_error) = set_width_and_height(&mut self.asset_entries) {
                panic!("Unable to set the width and height on the asset entries.")
            } else {
                ParserState::Complete(self.asset_entries)
            }
        } else {
            ParserState::Errored(self.errors.into())
        }
    }
}

impl<'source> State<'source, ParsingAsset<'source>> {
    fn parse_asset_block(&mut self) -> Result<(), ParserError<'source>> {
        self.require_token(Token::LBrace)?;

        let assets = &mut self.asset_entries;
        let tokenizer = &mut self.tokenizer;

        let mut asset_block = assets.begin_asset_block(self.state.ident);

        loop {
            let token_info = get_token(tokenizer)?;
            if token_info.token == Token::RBrace {
                break;
            }

            if let Token::Ident(ident) = token_info.token {
                match ident {
                    // TODO: If we encounter these tags multiple times, we should at least display
                    // a warning. It is trivial to figure out if it has been set because we can
                    // have the AssetBlock return any previous values since it uses
                    // `Option::replace` to assign the new value. If the returned option is not
                    // None, we know we came across the tag more than once.
                    Identifier::Height => {
                        let value = parse_int_assignment(tokenizer)?;
                        asset_block.set_height(value);
                    }

                    Identifier::Width => {
                        let value = parse_int_assignment(tokenizer)?;
                        asset_block.set_width(value);
                    }

                    Identifier::Pos => {
                        require_eq(tokenizer)?;
                        let x = parse_int(tokenizer)?;

                        require_com(tokenizer)?;
                        let y = parse_int(tokenizer)?;

                        require_sc(tokenizer)?;

                        asset_block.set_pos(x, y);
                    }

                    Identifier::Tags => {
                        require_eq(tokenizer)?;

                        'tag: loop {
                            let tag_name = parse_string(tokenizer)?;
                            let mut value = 1.;

                            'tag_value: loop {
                                let token_info = get_token(tokenizer)?;
                                match token_info.token {
                                    Token::Comma => {
                                        asset_block.push_tag(tag_name, value);
                                        break 'tag_value;
                                    }

                                    Token::LParen => {
                                        value = parse_float(tokenizer)?;
                                        require_token(tokenizer, Token::RParen)?;
                                    }

                                    Token::SColon => {
                                        asset_block.push_tag(tag_name, value);
                                        break 'tag;
                                    }

                                    _ => {
                                        // TODO: Error reporting, invalid token.
                                        return Err(ParserError::UnexpectedToken(token_info));
                                    }
                                }
                            }
                        }
                    }

                    _ => {
                        // TODO: Error, invalid token.
                        ff_to_sc(tokenizer)?;
                    }
                }
            }
        }

        asset_block.end();

        Ok(())
    }
}

impl<'source> State<'source, ParsingComment> {
    fn parse_comment(&mut self) -> Result<(), ParserError<'source>> {
        if let Err(error) = self.require_token(Token::Star) {
            self.report_error_and_ff_to_token(error, Token::FSlash)
        }

        if let Err(error) = self.ff_to_token(Token::Star) {
            self.report_error_and_ff_to_token(error, Token::FSlash)
        }

        if let Err(error) = self.require_token(Token::FSlash) {
            self.errors.push(error);
        }

        Ok(())
    }
}

impl<'source> State<'source, ParsingAssetInfo> {
    fn parse_asset_info(&mut self) -> Result<(), ParserError<'source>> {
        self.require_token(Token::LBrace)?;

        while let Some(token_info) = self.tokenizer.next() {
            if token_info.token == Token::RBrace {
                break;
            }

            match token_info.token {
                Token::Ident(Identifier::Name) => match self.parse_string_assignment() {
                    Ok(value) => {
                        self.asset_entries.asset_info.name.replace(value);
                    }

                    Err(error) => {
                        self.report_error_and_ff_to_token(error, Token::SColon);
                    }
                },

                Token::Ident(Identifier::Author) => match self.parse_string_assignment() {
                    Ok(value) => {
                        self.asset_entries.asset_info.author.replace(value);
                    }

                    Err(error) => {
                        self.report_error_and_ff_to_token(error, Token::SColon);
                    }
                },

                Token::Ident(Identifier::Spritesheet) => match self.parse_string_assignment() {
                    Ok(value) => {
                        self.asset_entries.asset_info.spritesheet.replace(value);
                    }

                    Err(error) => {
                        self.report_error_and_ff_to_token(error, Token::SColon);
                    }
                },

                Token::Ident(Identifier::Description) => match self.parse_string_assignment() {
                    Ok(value) => {
                        self.asset_entries.asset_info.description.replace(value);
                    }

                    Err(error) => {
                        self.report_error_and_ff_to_token(error, Token::SColon);
                    }
                },

                Token::Ident(Identifier::TileWidth) => match self.parse_int_assignment() {
                    Ok(value) => {
                        self.asset_entries.asset_info.tile_width.replace(value);
                    }

                    Err(error) => {
                        self.report_error_and_ff_to_token(error, Token::SColon);
                    }
                },

                Token::Ident(Identifier::TileHeight) => match self.parse_int_assignment() {
                    Ok(value) => {
                        self.asset_entries.asset_info.tile_height.replace(value);
                    }

                    Err(error) => {
                        self.report_error_and_ff_to_token(error, Token::SColon);
                    }
                },

                _ => {
                    self.ff_to_sc()?;
                }
            }
        }

        Ok(())
    }
}

impl<'source> From<State<'source, Scanning>>
    for Result<ParsedAssetEntries<'source>, Vec<ParserError<'source>>>
{
    #[inline]
    fn from(scanning: State<'source, Scanning>) -> Self {
        if scanning.errors.len() == 0 {
            Ok(scanning.asset_entries)
        } else {
            Err(scanning.errors)
        }
    }
}

impl<'source> From<State<'source, Scanning>> for State<'source, ParsingAssetInfo> {
    #[inline]
    fn from(state: State<'source, Scanning>) -> Self {
        Self {
            tokenizer: state.tokenizer,
            asset_entries: state.asset_entries,
            errors: state.errors,
            state: ParsingAssetInfo,
        }
    }
}

impl<'source> From<(Identifier<'source>, State<'source, Scanning>)>
    for State<'source, ParsingAsset<'source>>
{
    #[inline]
    fn from((ident, state): (Identifier<'source>, State<'source, Scanning>)) -> Self {
        Self {
            tokenizer: state.tokenizer,
            asset_entries: state.asset_entries,
            errors: state.errors,
            state: ParsingAsset::new(ident),
        }
    }
}

impl<'source> From<State<'source, Scanning>> for State<'source, ParsingComment> {
    #[inline]
    fn from(state: State<'source, Scanning>) -> Self {
        Self {
            tokenizer: state.tokenizer,
            asset_entries: state.asset_entries,
            errors: state.errors,
            state: ParsingComment,
        }
    }
}

impl<'source> From<State<'source, ParsingAsset<'source>>> for State<'source, Scanning> {
    #[inline]
    fn from(mut state: State<'source, ParsingAsset<'source>>) -> Self {
        if let Err(error) = state.parse_asset_block() {
            state.report_error_and_ff_to_token(error, Token::RBrace);
        }

        Self {
            tokenizer: state.tokenizer,
            asset_entries: state.asset_entries,
            errors: state.errors,
            state: Scanning,
        }
    }
}

impl<'source> From<State<'source, ParsingAssetInfo>> for State<'source, Scanning> {
    #[inline]
    fn from(mut state: State<'source, ParsingAssetInfo>) -> State<'source, Scanning> {
        if let Err(error) = state.parse_asset_info() {
            state.report_error_and_ff_to_token(error, Token::RBrace);
        }

        Self {
            tokenizer: state.tokenizer,
            asset_entries: state.asset_entries,
            errors: state.errors,
            state: Scanning,
        }
    }
}

impl<'source> From<State<'source, ParsingComment>> for State<'source, Scanning> {
    #[inline]
    fn from(mut state: State<'source, ParsingComment>) -> Self {
        if let Err(error) = state.parse_comment() {
            state.report_error_and_ff_to_token(error, Token::FSlash);
        }

        Self {
            tokenizer: state.tokenizer,
            asset_entries: state.asset_entries,
            errors: state.errors,
            state: Scanning,
        }
    }
}

impl<'source, S> State<'source, S> {
    fn require_token(&mut self, token: Token<'_>) -> Result<usize, ParserError<'source>> {
        require_token(&mut self.tokenizer, token)
    }

    fn ff_to_token(&mut self, token: Token<'_>) -> Result<usize, ParserError<'source>> {
        ff_to_token(&mut self.tokenizer, token)
    }

    fn parse_string_assignment(&mut self) -> Result<&'source str, ParserError<'source>> {
        parse_string_assignment(&mut self.tokenizer)
    }

    fn parse_int_assignment(&mut self) -> Result<u32, ParserError<'source>> {
        parse_int_assignment(&mut self.tokenizer)
    }

    fn ff_to_sc(&mut self) -> Result<usize, ParserError<'source>> {
        ff_to_sc(&mut self.tokenizer)
    }

    fn report_error_and_ff_to_token(&mut self, error: ParserError<'source>, token: Token<'_>) {
        self.errors.push(error);
        if let Err(error) = ff_to_token(&mut self.tokenizer, token) {
            self.errors.push(error);
        }
    }
}

pub fn parse<'source>(
    source: &'source str,
) -> Result<ParsedAssetEntries<'source>, ParserErrors<'source>> {
    let parser = ParserState::init(source);
    parser.parse()
}

// Display impls

impl<'source> fmt::Display for ParsedAssetEntries<'source> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        writeln!(f, "{}", self.asset_info)?;
        writeln!(f, "Entries:")?;
        for entry in self.entries.iter() {
            writeln!(f, " {}", entry)?;

            let tags = &self.tags[entry.tag_start..entry.tag_start + entry.tag_count];
            write!(f, "  - ")?;
            for tag in tags {
                write!(f, "{} ", tag)?;
            }
            writeln!(f)?;
        }
        writeln!(f)?;

        Ok(())
    }
}

impl<'source> fmt::Display for ParsedAssetEntry<'source> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let dim = match (self.width, self.height) {
            (Some(width), Some(height)) => format!("{}x{}", width, height),
            (Some(width), None) => format!("{}xX", width),
            (None, Some(height)) => format!("Xx{}", height),
            (None, None) => format!("XxX"),
        };

        let pos = match self.pos {
            Some((x, y)) => format!("{}x{}", x, y),
            None => format!("XxX"),
        };

        write!(
            f,
            "{}: {{ pos: {}; dim: {}; tag_start: {}; tag_count: {}; }}",
            self.asset_type, pos, dim, self.tag_start, self.tag_count,
        )
    }
}

impl<'source> fmt::Display for ParsedAssetInfo<'source> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let tile_dim = match (self.tile_width, self.tile_height) {
            (Some(tile_width), Some(tile_height)) => format!("{}x{}", tile_width, tile_height),
            (Some(tile_width), None) => format!("{}xX", tile_width),
            (None, Some(tile_height)) => format!("Xx{}", tile_height),
            (None, None) => format!("XxX"),
        };

        writeln!(f, "Name:\t\t{}", self.name.unwrap_or("-- Not set --"))?;
        writeln!(f, "Author:\t\t{}", self.author.unwrap_or("-- Not set --"))?;
        writeln!(
            f,
            "Spritesheet:\t{}",
            self.spritesheet.unwrap_or("-- Not set --")
        )?;
        writeln!(
            f,
            "Description:\t{}",
            self.description.unwrap_or("-- Not set --")
        )?;
        writeln!(f, "Tile Dim:\t{}", tile_dim)
    }
}

impl<'source> fmt::Display for ParsedTag<'source> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "({}: {})", self.name, self.value)
    }
}
