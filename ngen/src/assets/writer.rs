use super::{parser::ParsedAssetEntries, Bitmap};
use crate::io::BinaryWrite;
use std::io;

const NGEN_MARKER: &[u8; 4] = b"ngen";
const VERSION: u32 = 1;

unsafe fn write_len<B: BinaryWrite>(writer: &mut B, value: usize) -> io::Result<usize> {
    let len = value as u32;
    writer.binary_write(&len)
}

unsafe fn write_slice<T, B: BinaryWrite>(writer: &mut B, value: &[T]) -> io::Result<usize> {
    let mut written = write_len(writer, value.len())?;
    written += writer.binary_write_slice(value)?;

    Ok(written)
}

unsafe fn write_opt_str<B: BinaryWrite>(writer: &mut B, value: Option<&str>) -> io::Result<usize> {
    match value {
        Some(value) => {
            let bytes = value.as_bytes();
            write_slice(writer, bytes)
        }

        None => write_slice(writer, "".as_bytes()),
    }
}

unsafe fn write_opt_u32<B: BinaryWrite>(write: &mut B, value: Option<u32>) -> io::Result<usize> {
    match value {
        Some(value) => write.binary_write(&value),
        None => write.binary_write(&0),
    }
}

struct TempBuffer {
    buffer: Vec<u8>,
}

impl TempBuffer {
    fn new() -> Self {
        Self {
            buffer: Vec::with_capacity(1024),
        }
    }

    fn begin(&mut self) -> Buffer<'_> {
        Buffer {
            data: &mut self.buffer,
        }
    }
}

struct Buffer<'a> {
    data: &'a mut Vec<u8>,
}

impl<'a> Buffer<'a> {
    unsafe fn write_to_buffer<B: BinaryWrite>(&self, b: &mut B) -> io::Result<usize> {
        let written = b.write(&self.data)?;
        Ok(written)
    }
}

impl<'a> io::Write for Buffer<'a> {
    fn write(&mut self, bytes: &[u8]) -> io::Result<usize> {
        self.data.write(bytes)
    }

    fn flush(&mut self) -> io::Result<()> {
        self.data.flush()
    }
}

impl<'a> Drop for Buffer<'a> {
    fn drop(&mut self) {
        self.data.clear();
    }
}

impl<'a> AsRef<[u8]> for Buffer<'a> {
    fn as_ref(&self) -> &[u8] {
        &self.data
    }
}

pub struct AssetWriter<'a, 'source, B: BinaryWrite> {
    last_write: u128,
    assets: &'a mut ParsedAssetEntries<'source>,
    spritesheet: &'a Bitmap,
    buffer: &'a mut B,
    temp_buffer: TempBuffer,
}

impl<'a, 'source, B: BinaryWrite> AssetWriter<'a, 'source, B> {
    pub fn init(
        last_write: u128,
        assets: &'a mut ParsedAssetEntries<'source>,
        spritesheet: &'a Bitmap,
        buffer: &'a mut B,
    ) -> Self {
        let temp_buffer = TempBuffer::new();
        Self {
            last_write,
            assets,
            spritesheet,
            buffer,
            temp_buffer,
        }
    }

    pub fn write(&mut self) -> io::Result<usize> {
        let mut total_written = unsafe { self.write_preamble()? };
        total_written += unsafe { self.write_header()? };
        total_written += unsafe { self.write_tags()? };
        total_written += unsafe { self.write_entries()? };
        total_written += unsafe { self.write_spritesheet()? };

        Ok(total_written)
    }

    unsafe fn write_preamble(&mut self) -> io::Result<usize> {
        let mut written = self.buffer.write(NGEN_MARKER)?;
        written += self.buffer.binary_write(&VERSION)?;
        written += self.buffer.binary_write(&self.last_write)?;
        written += write_len(&mut self.buffer, self.assets.entries.len())?;
        written += write_len(&mut self.buffer, self.assets.tags.len())?;

        Ok(written)
    }

    unsafe fn write_header(&mut self) -> io::Result<usize> {
        let buffer = &mut self.buffer;
        let assets = &self.assets;

        let mut written = write_opt_str(buffer, assets.asset_info.name)?;
        written += write_opt_str(buffer, assets.asset_info.author)?;
        written += write_opt_str(buffer, assets.asset_info.description)?;

        Ok(written)
    }

    unsafe fn write_tags(&mut self) -> io::Result<usize> {
        let buffer = &mut self.buffer;
        let tags = &self.assets.tags;

        let mut temp_buffer = self.temp_buffer.begin();
        for tag in tags.iter() {
            let name = tag.name.as_bytes();
            write_slice(&mut temp_buffer, name)?;
            temp_buffer.binary_write(&tag.value)?;
        }

        let written = temp_buffer.write_to_buffer(buffer)?;
        Ok(written)
    }

    unsafe fn write_entries(&mut self) -> io::Result<usize> {
        let buffer = &mut self.buffer;
        let entries = &self.assets.entries;

        let mut temp_buffer = self.temp_buffer.begin();
        for entry in entries.iter() {
            // Height x Width
            write_opt_u32(&mut temp_buffer, entry.height)?;
            write_opt_u32(&mut temp_buffer, entry.width)?;

            let (x, y) = match entry.pos {
                Some((x, y)) => (x, y),
                None => (0, 0),
            };
            temp_buffer.binary_write(&x)?;
            temp_buffer.binary_write(&y)?;

            temp_buffer.binary_write(&(entry.tag_start as u32))?;
            temp_buffer.binary_write(&(entry.tag_count as u32))?;

            let name = entry.asset_type.as_ref().as_bytes();
            write_slice(&mut temp_buffer, name)?;
        }

        let written = temp_buffer.write_to_buffer(buffer)?;
        Ok(written)
    }

    unsafe fn write_spritesheet(&mut self) -> io::Result<usize> {
        let spritesheet = &self.spritesheet;
        let buffer = &mut self.buffer;

        let mut written = buffer.binary_write(&spritesheet.height)?;
        written += buffer.binary_write(&spritesheet.width)?;
        written += buffer.binary_write_slice(&spritesheet.data)?;
        Ok(written)
    }
}
