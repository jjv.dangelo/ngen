use super::{AssetEntry, AssetPack, AssetTag, AssetType, Bitmap, BitmapId};
use crate::Trie;
use std::collections::HashMap;

// TODO: These errors aren't helpful without context in the file. We need to add positional help to
// let the consumer know where we ran into the error.
// TODO: Also, we should add warnings for things such as:
//  - Duplicate asset entries
//  - Duplicate tags in a single asset
//  - Duplicate asset header values
// Currently, we take the last value and ignore the rest.
const NGEN_NOT_SET_ERROR: &str = "The 'ngen' prefix was not set.";
const UNK_VERSION_ERROR: &str = "Unknown asset file version.";
const OUT_OF_BYTES_ERROR: &str = "The input stream is out of bytes.";
const INVALID_MEMORY_REP_ERROR: &str = "Unable to coerce the memory to a type T.";
const INVALID_UTF8_ERROR: &str = "Encountered an invalid UTF-8 string.";
const UNK_ASSET_TYPE_ERROR: &str = "Unknown asset type.";
const UNK_ASSET_TAG_ERROR: &str = "Unknown asset tag.";

const VERSION: u32 = 1;

pub struct AssetFileReader<'source, TType, TTag> {
    data: &'source [u8],
    asset_types: &'source Trie<TType>,
    asset_tags: &'source Trie<TTag>,
    pos: usize,
}

impl<'source, TType, TTag> AssetFileReader<'source, TType, TTag>
where
    TType: AssetType + Copy,
    TTag: AssetTag + Copy,
{
    pub fn new(
        data: &'source [u8],
        asset_types: &'source Trie<TType>,
        asset_tags: &'source Trie<TTag>,
    ) -> Self {
        Self {
            data,
            asset_types,
            asset_tags,
            pos: 0,
        }
    }

    pub fn load(
        mut self,
        bitmaps: &mut Vec<Bitmap>,
    ) -> Result<AssetPack<TType, TTag>, &'source str> {
        self.check_prefix()?;

        let last_updated = *unsafe { self.read_value::<u128>()? };
        let total_entries = (*unsafe { self.read_value::<u32>()? }) as usize;
        let total_tags = (*unsafe { self.read_value::<u32>()? }) as usize;
        let name = unsafe { self.read_str()? }.to_owned();
        let author = unsafe { self.read_str()? }.to_owned();
        let description = unsafe { self.read_str()? }.to_owned();
        let tags = unsafe { self.read_tags(total_tags)? };
        let entries = unsafe { self.read_entries(total_entries)? };
        let bitmap = unsafe { self.read_bitmap()? };

        let bitmap = {
            let bitmap_id = bitmaps.len();
            bitmaps.push(bitmap);
            BitmapId(bitmap_id)
        };

        Ok(AssetPack {
            last_updated,
            name,
            author,
            description,
            tags,
            entries,
            bitmap,
        })
    }

    fn check_prefix(&mut self) -> Result<(), &'source str> {
        let bytes = unsafe { self.read_slice(4)? };
        if bytes != b"ngen" {
            return Err(NGEN_NOT_SET_ERROR);
        }

        let version = unsafe { self.read_value()? };
        if VERSION != *version {
            return Err(UNK_VERSION_ERROR);
        }

        Ok(())
    }

    unsafe fn read_slice(&mut self, len: usize) -> Result<&'source [u8], &'source str> {
        let start = self.pos;
        let end = start + len;

        if end >= self.data.len() {
            return Err(OUT_OF_BYTES_ERROR);
        } else {
            self.pos = end;

            let slice = &self.data[start..end];
            let ptr = slice.as_ptr();

            Ok(std::slice::from_raw_parts(ptr, len))
        }
    }

    unsafe fn read_value<T>(&mut self) -> Result<&'source T, &'source str> {
        let len = std::mem::size_of::<T>();
        if self.pos + len >= self.data.len() {
            return Err(OUT_OF_BYTES_ERROR);
        }

        let start = self.pos;
        let end = start + len;
        self.pos = end;

        let bytes = &self.data[start..end];

        let ptr = bytes.as_ptr() as *const T;
        ptr.as_ref().ok_or(INVALID_MEMORY_REP_ERROR)
    }

    unsafe fn read_str(&mut self) -> Result<&'source str, &'source str> {
        let len = (*self.read_value::<u32>()?) as usize;
        let bytes = self.read_slice(len)?;

        std::str::from_utf8(bytes).map_err(|_| INVALID_UTF8_ERROR)
    }

    unsafe fn read_tag(&mut self) -> Result<(TTag, f32), &'source str> {
        let name = self.read_str()?;
        let value = *self.read_value::<f32>()?;
        let asset_tag = self.asset_tags.find(name).ok_or(UNK_ASSET_TAG_ERROR)?;

        Ok((*asset_tag, value))
    }

    unsafe fn read_tags(&mut self, num_tags: usize) -> Result<Vec<(TTag, f32)>, &'source str> {
        let mut tags = Vec::with_capacity(num_tags);

        for _ in 0..num_tags {
            let tag = self.read_tag()?;
            tags.push(tag);
        }

        Ok(tags)
    }

    unsafe fn read_entry(&mut self) -> Result<AssetEntry<TType>, &'source str> {
        let height = *self.read_value::<u32>()? as f32;
        let width = *self.read_value::<u32>()? as f32;

        let pos = {
            let x = *self.read_value::<u32>()? as f32;
            let y = *self.read_value::<u32>()? as f32;
            (x, y).into()
        };

        let tag_start = *self.read_value::<u32>()? as usize;
        let tag_count = *self.read_value::<u32>()? as usize;
        let tag_end = tag_start + tag_count;

        let asset_type = self.read_str()?;
        let asset_type = self
            .asset_types
            .find(asset_type)
            .ok_or(UNK_ASSET_TYPE_ERROR)?;

        Ok(AssetEntry {
            asset_type: *asset_type,
            height,
            width,
            pos,
            tag_start,
            tag_end,
        })
    }

    unsafe fn read_entries(
        &mut self,
        num_entries: usize,
    ) -> Result<HashMap<TType, Vec<AssetEntry<TType>>>, &'source str> {
        let mut entries = HashMap::new();

        for _ in 0..num_entries {
            let entry = self.read_entry()?;
            let entries = entries.entry(entry.asset_type).or_insert(Vec::new());
            entries.push(entry);
        }

        Ok(entries)
    }

    unsafe fn read_bitmap(&mut self) -> Result<Bitmap, &'source str> {
        let height = *self.read_value()?;
        let width = *self.read_value()?;

        let buffer_size = (height * width) as usize;
        assert!(buffer_size * 4 <= self.data.len() - self.pos);

        let slice = {
            let ptr = &self.data[self.pos] as *const _ as *const u32;
            std::slice::from_raw_parts(ptr, buffer_size)
        };
        let data = slice.iter().cloned().collect();
        Ok(Bitmap::new(width, height, data))
    }
}
