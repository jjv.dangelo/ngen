use crate::trie;
use crate::trie::*;
use std::fmt;

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum Identifier<'source> {
    Info,
    Name,
    Author,
    Spritesheet,
    Description,
    TileWidth,
    TileHeight,
    Width,
    Height,
    Tags,
    Pos,
    Other(&'source str),
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum Token<'source> {
    LBrace,
    RBrace,
    LParen,
    RParen,
    Quote,
    Equal,
    Pound,
    Dash,
    Colon,
    SColon,
    Comma,
    Star,
    FSlash,
    Int(u32),
    Float(f32),
    Ident(Identifier<'source>),
}

#[derive(Clone, Copy, Debug)]
pub struct TokenInfo<'token> {
    pub(crate) token: Token<'token>,
    pos: (usize, u16, u16), // (raw_pos, line, col)
}

impl<'token> TokenInfo<'token> {
    fn new(token: Token<'token>, pos: (usize, u16, u16)) -> Self {
        Self { token, pos }
    }

    pub fn pos(&self) -> usize {
        self.pos.0
    }

    pub fn line(&self) -> u16 {
        self.pos.1
    }

    pub fn col(&self) -> u16 {
        self.pos.2
    }
}

impl<'token> AsRef<Token<'token>> for TokenInfo<'token> {
    fn as_ref(&self) -> &Token<'token> {
        &self.token
    }
}

pub struct Tokenizer<'source> {
    pub source: &'source str,
    pub pos: usize,

    line: u16,
    col: u16,

    identifiers: Trie<Identifier<'source>>,
    tokens: Trie<Token<'source>>,
}

impl<'source> Tokenizer<'source> {
    pub fn new(source: &'source str) -> Self {
        let identifiers = trie![
            Identifier::Info,
            Identifier::Name,
            Identifier::Author,
            Identifier::Spritesheet,
            Identifier::Description,
            Identifier::TileWidth,
            Identifier::TileHeight,
            Identifier::Height,
            Identifier::Width,
            Identifier::Pos,
            Identifier::Tags,
        ];

        let tokens = trie![
            Token::LBrace,
            Token::RBrace,
            Token::LParen,
            Token::RParen,
            Token::Equal,
            Token::Quote,
            Token::Pound,
            Token::Dash,
            Token::Colon,
            Token::SColon,
            Token::Comma,
            Token::Star,
            Token::FSlash,
        ];

        Self {
            source,
            pos: 0,
            line: 1,
            col: 1,
            identifiers,
            tokens,
        }
    }

    fn eat_whitespace(&mut self) {
        for c in self.source[self.pos..].chars() {
            if c == '\n' {
                self.col = 0;
                self.line += 1;
                self.pos += 1;
            } else if c.is_whitespace() {
                self.pos += 1;
                self.col += 1;
            } else {
                break;
            }
        }
    }

    fn parse_token(&mut self) -> Token<'source> {
        let start = self.pos;

        for c in self.source[start..].chars() {
            if c.is_whitespace() {
                break;
            }

            if let Some(_) = self.tokens.find(&self.source[self.pos..self.pos + 1]) {
                break;
            }

            self.pos += 1;
            self.col += 1;
        }

        let chunk = &self.source[start..self.pos];
        if let Some(ident) = self.identifiers.find(chunk) {
            Token::Ident(*ident)
        } else if let Ok(num) = chunk.parse::<u32>() {
            Token::Int(num)
        } else if let Ok(num) = chunk.parse::<f32>() {
            Token::Float(num)
        } else {
            Token::Ident(Identifier::Other(chunk))
        }
    }
}

impl<'source> Iterator for Tokenizer<'source> {
    type Item = TokenInfo<'source>;

    fn next(&mut self) -> Option<Self::Item> {
        if self.pos + 1 >= self.source.len() {
            return None;
        }

        self.eat_whitespace();
        let pos = (self.pos, self.line, self.col);

        if let Some(token) = self.tokens.find(&self.source[self.pos..self.pos + 1]) {
            self.pos += 1;
            self.col += 1;
            Some(TokenInfo::new(*token, pos))
        } else {
            Some(TokenInfo::new(self.parse_token(), pos))
        }
    }
}

// fmt::Display implementations

impl<'source> fmt::Display for Identifier<'source> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Identifier::Info => write!(f, "Info"),
            Identifier::Name => write!(f, "Name"),
            Identifier::Author => write!(f, "Author"),
            Identifier::Spritesheet => write!(f, "Spritesheet"),
            Identifier::Description => write!(f, "Description"),
            Identifier::TileWidth => write!(f, "TileWidth"),
            Identifier::TileHeight => write!(f, "TileHeight"),
            Identifier::Height => write!(f, "Height"),
            Identifier::Width => write!(f, "Width"),
            Identifier::Pos => write!(f, "Pos"),
            Identifier::Tags => write!(f, "Tags"),
            Identifier::Other(ref o) => write!(f, "{}", o),
        }
    }
}

impl<'source> std::fmt::Display for Token<'source> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Token::LBrace => write!(f, "{{"),
            Token::RBrace => write!(f, "}}"),
            Token::LParen => write!(f, "("),
            Token::RParen => write!(f, ")"),
            Token::Quote => write!(f, "\""),
            Token::Equal => write!(f, "="),
            Token::Pound => write!(f, "#"),
            Token::Dash => write!(f, "-"),
            Token::Colon => write!(f, ":"),
            Token::SColon => write!(f, ";"),
            Token::Comma => write!(f, ","),
            Token::Star => write!(f, "*"),
            Token::FSlash => write!(f, "/"),
            Token::Int(ref i) => write!(f, "{}", i),
            Token::Float(ref fl) => write!(f, "{}", fl),
            Token::Ident(ref ident) => write!(f, "{}", ident),
        }
    }
}

impl<'source> AsRef<&'source str> for Identifier<'source> {
    fn as_ref(&self) -> &&'source str {
        match self {
            Identifier::Info => &"Info",
            Identifier::Name => &"Name",
            Identifier::Author => &"Author",
            Identifier::Spritesheet => &"Spritesheet",
            Identifier::Description => &"Description",
            Identifier::TileWidth => &"TileWidth",
            Identifier::TileHeight => &"TileHeight",
            Identifier::Height => &"Height",
            Identifier::Width => &"Width",
            Identifier::Pos => &"Pos",
            Identifier::Tags => &"Tags",
            Identifier::Other(ref o) => &o,
        }
    }
}

impl<'source> AsRef<&'source str> for Token<'source> {
    fn as_ref(&self) -> &&'source str {
        match self {
            Token::LBrace => &"{",
            Token::RBrace => &"}",
            Token::LParen => &"(",
            Token::RParen => &")",
            Token::Quote => &"\"",
            Token::Equal => &"=",
            Token::Pound => &"#",
            Token::Dash => &"-",
            Token::Colon => &":",
            Token::SColon => &";",
            Token::Comma => &",",
            Token::Star => &"*",
            Token::FSlash => &"/",
            Token::Ident(ident) => ident.as_ref(),
            Token::Int(_) | Token::Float(_) => unimplemented!(),
        }
    }
}
