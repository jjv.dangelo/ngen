use super::{AssetManager, BitmapId, Sprite};
use math::Vec2;
use std::collections::HashMap;

struct Header {
    num_glyphs: u32,
    bitmap_width: u32,
    bitmap_height: u32,
}

struct Glyph {
    character: char,
    pos: Vec2,
    dim: Vec2,
}

const MARKER: &'static [u8; 4] = b"ngen";

pub struct Font {
    texture_id: BitmapId,
    glyphs: HashMap<char, Sprite>,
}

impl Font {
    pub fn new<A: AssetManager>(assets: &mut A) -> Option<Self> {
        let texture_id = assets.load_bitmap("assets/platformer/font.png")?;

        let mut glyphs = HashMap::new();
        glyphs.insert(
            '0',
            assets.create_sprite(texture_id, (3., 5.).into(), 31., 37.)?,
        );
        glyphs.insert(
            '1',
            assets.create_sprite(texture_id, (37., 5.).into(), 19., 37.)?,
        );
        glyphs.insert(
            '2',
            assets.create_sprite(texture_id, (61., 5.).into(), 26., 37.)?,
        );
        glyphs.insert(
            '3',
            assets.create_sprite(texture_id, (91., 5.).into(), 26., 37.)?,
        );
        glyphs.insert(
            '4',
            assets.create_sprite(texture_id, (122., 5.).into(), 32., 37.)?,
        );
        glyphs.insert(
            '5',
            assets.create_sprite(texture_id, (159., 5.).into(), 26., 37.)?,
        );
        glyphs.insert(
            '6',
            assets.create_sprite(texture_id, (188., 5.).into(), 33., 37.)?,
        );
        glyphs.insert(
            '7',
            assets.create_sprite(texture_id, (226., 5.).into(), 29., 37.)?,
        );
        glyphs.insert(
            '8',
            assets.create_sprite(texture_id, (259., 5.).into(), 31., 37.)?,
        );
        glyphs.insert(
            '9',
            assets.create_sprite(texture_id, (295., 5.).into(), 32., 37.)?,
        );

        glyphs.insert(
            'a',
            assets.create_sprite(texture_id, (331., 5.).into(), 29., 37.)?,
        );
        glyphs.insert(
            'b',
            assets.create_sprite(texture_id, (364., 5.).into(), 30., 37.)?,
        );
        glyphs.insert(
            'c',
            assets.create_sprite(texture_id, (3., 56.).into(), 20., 37.)?,
        );
        glyphs.insert(
            'd',
            assets.create_sprite(texture_id, (28., 56.).into(), 29., 37.)?,
        );
        glyphs.insert(
            'e',
            assets.create_sprite(texture_id, (62., 56.).into(), 27., 37.)?,
        );
        glyphs.insert(
            'f',
            assets.create_sprite(texture_id, (93., 56.).into(), 21., 37.)?,
        );
        glyphs.insert(
            'g',
            assets.create_sprite(texture_id, (119., 56.).into(), 30., 45.)?,
        );
        glyphs.insert(
            'h',
            assets.create_sprite(texture_id, (152., 56.).into(), 30., 37.)?,
        );
        glyphs.insert(
            'i',
            assets.create_sprite(texture_id, (186., 56.).into(), 16., 37.)?,
        );
        glyphs.insert(
            'j',
            assets.create_sprite(texture_id, (203., 56.).into(), 21., 45.)?,
        );
        glyphs.insert(
            'k',
            assets.create_sprite(texture_id, (228., 56.).into(), 35., 37.)?,
        );
        glyphs.insert(
            'l',
            assets.create_sprite(texture_id, (268., 56.).into(), 12., 37.)?,
        );
        glyphs.insert(
            'm',
            assets.create_sprite(texture_id, (285., 56.).into(), 47., 37.)?,
        );
        glyphs.insert(
            'n',
            assets.create_sprite(texture_id, (337., 56.).into(), 31., 37.)?,
        );
        glyphs.insert(
            'o',
            assets.create_sprite(texture_id, (370., 56.).into(), 29., 37.)?,
        );
        glyphs.insert(
            'p',
            assets.create_sprite(texture_id, (3., 107.).into(), 29., 47.)?,
        );
        glyphs.insert(
            'q',
            assets.create_sprite(texture_id, (37., 107.).into(), 30., 47.)?,
        );
        glyphs.insert(
            'r',
            assets.create_sprite(texture_id, (70., 107.).into(), 21., 37.)?,
        );
        glyphs.insert(
            's',
            assets.create_sprite(texture_id, (97., 107.).into(), 20., 37.)?,
        );
        glyphs.insert(
            't',
            assets.create_sprite(texture_id, (122., 107.).into(), 23., 37.)?,
        );
        glyphs.insert(
            'u',
            assets.create_sprite(texture_id, (149., 107.).into(), 30., 37.)?,
        );
        glyphs.insert(
            'v',
            assets.create_sprite(texture_id, (185., 107.).into(), 30., 37.)?,
        );
        glyphs.insert(
            'w',
            assets.create_sprite(texture_id, (222., 107.).into(), 42., 37.)?,
        );
        glyphs.insert(
            'x',
            assets.create_sprite(texture_id, (271., 107.).into(), 33., 37.)?,
        );
        glyphs.insert(
            'y',
            assets.create_sprite(texture_id, (312., 107.).into(), 33., 47.)?,
        );
        glyphs.insert(
            'z',
            assets.create_sprite(texture_id, (348., 107.).into(), 27., 37.)?,
        );

        glyphs.insert(
            'A',
            assets.create_sprite(texture_id, (3., 159.).into(), 36., 37.)?,
        );
        glyphs.insert(
            'B',
            assets.create_sprite(texture_id, (47., 159.).into(), 30., 37.)?,
        );
        glyphs.insert(
            'C',
            assets.create_sprite(texture_id, (81., 159.).into(), 29., 37.)?,
        );
        glyphs.insert(
            'D',
            assets.create_sprite(texture_id, (115., 159.).into(), 32., 37.)?,
        );
        glyphs.insert(
            'E',
            assets.create_sprite(texture_id, (150., 159.).into(), 24., 37.)?,
        );
        glyphs.insert(
            'F',
            assets.create_sprite(texture_id, (179., 159.).into(), 24., 37.)?,
        );
        glyphs.insert(
            'G',
            assets.create_sprite(texture_id, (207., 159.).into(), 35., 37.)?,
        );
        glyphs.insert(
            'H',
            assets.create_sprite(texture_id, (247., 159.).into(), 34., 37.)?,
        );
        glyphs.insert(
            'I',
            assets.create_sprite(texture_id, (281., 159.).into(), 14., 37.)?,
        );
        glyphs.insert(
            'J',
            assets.create_sprite(texture_id, (300., 159.).into(), 21., 37.)?,
        );
        glyphs.insert(
            'K',
            assets.create_sprite(texture_id, (326., 159.).into(), 35., 37.)?,
        );
        glyphs.insert(
            'L',
            assets.create_sprite(texture_id, (364., 159.).into(), 25., 37.)?,
        );
        glyphs.insert(
            'M',
            assets.create_sprite(texture_id, (3., 213.).into(), 48., 37.)?,
        );
        glyphs.insert(
            'N',
            assets.create_sprite(texture_id, (56., 213.).into(), 34., 37.)?,
        );
        glyphs.insert(
            'O',
            assets.create_sprite(texture_id, (94., 213.).into(), 40., 37.)?,
        );
        glyphs.insert(
            'P',
            assets.create_sprite(texture_id, (136., 213.).into(), 29., 37.)?,
        );
        glyphs.insert(
            'Q',
            assets.create_sprite(texture_id, (169., 213.).into(), 39., 42.)?,
        );
        glyphs.insert(
            'R',
            assets.create_sprite(texture_id, (212., 213.).into(), 35., 37.)?,
        );
        glyphs.insert(
            'S',
            assets.create_sprite(texture_id, (252., 213.).into(), 27., 37.)?,
        );
        glyphs.insert(
            'T',
            assets.create_sprite(texture_id, (283., 213.).into(), 26., 37.)?,
        );
        glyphs.insert(
            'U',
            assets.create_sprite(texture_id, (314., 213.).into(), 31., 37.)?,
        );
        glyphs.insert(
            'V',
            assets.create_sprite(texture_id, (350., 213.).into(), 39., 37.)?,
        );
        glyphs.insert(
            'W',
            assets.create_sprite(texture_id, (3., 263.).into(), 43., 37.)?,
        );
        glyphs.insert(
            'X',
            assets.create_sprite(texture_id, (52., 263.).into(), 39., 37.)?,
        );
        glyphs.insert(
            'Y',
            assets.create_sprite(texture_id, (97., 263.).into(), 35., 37.)?,
        );
        glyphs.insert(
            'Z',
            assets.create_sprite(texture_id, (136., 263.).into(), 34., 37.)?,
        );

        glyphs.insert(
            ' ',
            assets.create_sprite(texture_id, (397., 5.).into(), 14., 37.)?,
        );
        glyphs.insert(
            '`',
            assets.create_sprite(texture_id, (174., 263.).into(), 14., 37.)?,
        );
        glyphs.insert(
            '&',
            assets.create_sprite(texture_id, (192., 263.).into(), 38., 37.)?,
        );
        glyphs.insert(
            '\'',
            assets.create_sprite(texture_id, (234., 263.).into(), 14., 37.)?,
        );
        glyphs.insert(
            '*',
            assets.create_sprite(texture_id, (252., 263.).into(), 25., 37.)?,
        );
        glyphs.insert(
            '*',
            assets.create_sprite(texture_id, (252., 263.).into(), 25., 37.)?,
        );
        glyphs.insert(
            '\\',
            assets.create_sprite(texture_id, (317., 263.).into(), 21., 37.)?,
        );
        glyphs.insert(
            ':',
            assets.create_sprite(texture_id, (340., 263.).into(), 14., 37.)?,
        );
        glyphs.insert(
            ',',
            assets.create_sprite(texture_id, (357., 263.).into(), 14., 42.)?,
        );
        glyphs.insert(
            '$',
            assets.create_sprite(texture_id, (375., 263.).into(), 29., 42.)?,
        );
        glyphs.insert(
            '=',
            assets.create_sprite(texture_id, (3., 317.).into(), 19., 37.)?,
        );
        glyphs.insert(
            '!',
            assets.create_sprite(texture_id, (26., 317.).into(), 12., 37.)?,
        );
        glyphs.insert(
            '#',
            assets.create_sprite(texture_id, (42., 317.).into(), 29., 37.)?,
        );
        glyphs.insert(
            '-',
            assets.create_sprite(texture_id, (75., 317.).into(), 15., 37.)?,
        );
        glyphs.insert(
            '•',
            assets.create_sprite(texture_id, (94., 317.).into(), 9., 37.)?,
        );
        glyphs.insert(
            '{',
            assets.create_sprite(texture_id, (107., 317.).into(), 17., 42.)?,
        );
        glyphs.insert(
            '[',
            assets.create_sprite(texture_id, (128., 317.).into(), 15., 42.)?,
        );
        glyphs.insert(
            '‹',
            assets.create_sprite(texture_id, (146., 317.).into(), 14., 37.)?,
        );
        glyphs.insert(
            '(',
            assets.create_sprite(texture_id, (164., 317.).into(), 14., 37.)?,
        );
        glyphs.insert(
            '%',
            assets.create_sprite(texture_id, (180., 317.).into(), 39., 37.)?,
        );
        glyphs.insert(
            '.',
            assets.create_sprite(texture_id, (222., 317.).into(), 14., 37.)?,
        );
        glyphs.insert(
            '|',
            assets.create_sprite(texture_id, (239., 317.).into(), 10., 37.)?,
        );
        glyphs.insert(
            '+',
            assets.create_sprite(texture_id, (252., 317.).into(), 21., 37.)?,
        );
        glyphs.insert(
            '?',
            assets.create_sprite(texture_id, (277., 317.).into(), 25., 37.)?,
        );
        glyphs.insert(
            '?',
            assets.create_sprite(texture_id, (277., 317.).into(), 25., 37.)?,
        );
        glyphs.insert(
            '"',
            assets.create_sprite(texture_id, (306., 317.).into(), 22., 37.)?,
        );
        glyphs.insert(
            '}',
            assets.create_sprite(texture_id, (332., 317.).into(), 17., 42.)?,
        );
        glyphs.insert(
            ']',
            assets.create_sprite(texture_id, (355., 317.).into(), 15., 42.)?,
        );
        glyphs.insert(
            '›',
            assets.create_sprite(texture_id, (374., 317.).into(), 14., 42.)?,
        );
        glyphs.insert(
            ')',
            assets.create_sprite(texture_id, (391., 317.).into(), 14., 42.)?,
        );

        Some(Self { glyphs, texture_id })
    }

    pub fn get_sprites(&self, string: &str) -> Vec<Sprite> {
        let mut sprites = vec![];

        for c in string.chars() {
            if let Some(sprite) = self.glyphs.get(&c) {
                sprites.push(*sprite);
            }
        }

        sprites
    }

    pub fn from_disk<A: AssetManager>(assets: &mut A, location: &str) -> std::io::Result<Self> {
        use std::fs;

        let bytes = fs::read(location)?;
        assert_eq!(&bytes[..4], MARKER);

        // TODO: Unwrap
        let header = unsafe { bytes.as_ptr().offset(4).cast::<Header>().as_ref() }.unwrap();

        let glyph_data = unsafe {
            let ptr = bytes
                .as_ptr()
                .offset(4)
                .offset(std::mem::size_of::<Header>() as _)
                .cast::<Glyph>();
            std::slice::from_raw_parts(ptr, header.num_glyphs as _)
        };

        let image_data = unsafe {
            let ptr = bytes
                .as_ptr()
                .offset(4)
                .offset(std::mem::size_of::<Header>() as _)
                .offset((std::mem::size_of::<Glyph>() * header.num_glyphs as usize) as _)
                .cast::<u32>();

            std::slice::from_raw_parts(ptr, (header.bitmap_width * header.bitmap_height) as _)
        };

        let texture_id = assets.store_bitmap(
            header.bitmap_width as _,
            header.bitmap_height as _,
            image_data.iter().cloned().collect(),
        );

        let mut glyphs = HashMap::new();
        for glyph in glyph_data.iter() {
            // TODO: Unwrap
            let sprite = assets
                .create_sprite(texture_id, glyph.pos, glyph.dim.x, glyph.dim.y)
                .unwrap();
            glyphs.insert(glyph.character, sprite);
        }

        Ok(Self { texture_id, glyphs })
    }

    pub fn write_to_disk<A: AssetManager>(
        &self,
        assets: &A,
        location: &str,
    ) -> std::io::Result<()> {
        use std::fs::File;
        use std::io::prelude::*;

        if let Some(bitmap) = assets.get_bitmap(&self.texture_id) {
            let mut file = File::create(location)?;
            file.write_all(MARKER)?;

            let header = Header {
                num_glyphs: self.glyphs.len() as _,
                bitmap_width: bitmap.width,
                bitmap_height: bitmap.height,
            };

            let header_bytes = unsafe {
                let ptr = &header as *const _ as *const u8;
                let size = std::mem::size_of::<Header>();
                std::slice::from_raw_parts(ptr, size)
            };
            file.write_all(header_bytes)?;

            for (character, sprite) in self.glyphs.iter() {
                let glyph = Glyph {
                    character: *character,
                    pos: sprite.pos,
                    dim: sprite.dim,
                };

                let glyph_bytes = unsafe {
                    let ptr = &glyph as *const _ as *const u8;
                    let size = std::mem::size_of::<Glyph>();
                    std::slice::from_raw_parts(ptr, size)
                };
                file.write_all(glyph_bytes)?;
            }

            let bitmap_data = unsafe {
                let ptr = bitmap.data.as_ptr() as *const _ as *const u8;
                let size = (bitmap.width * bitmap.height) as usize * std::mem::size_of::<u32>();
                std::slice::from_raw_parts(ptr, size)
            };
            file.write_all(bitmap_data)?;

            file.write_all(MARKER)?;
        }

        Ok(())
    }
}
