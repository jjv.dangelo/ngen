pub mod opengl;

mod gui;
pub use gui::Gui;

use crate::assets::BitmapId;
use math::{Mat4, Vec2, Vec3, Vec4};

pub enum RenderCommand {
    ClearDepthBuffer,
    ClearColor(Vec4),
    BeginLayer(Vec4),
    EndLayer(Mat4, usize, usize),
}

#[derive(Clone, Copy, Default)]
struct TexturedVertex {
    pub pos: Vec3,
    pub col: Vec4,
    pub uv: Vec2,
    pub texture_id: f32,
}

impl TexturedVertex {
    fn new(pos: Vec3, col: Vec4) -> Self {
        Self {
            pos,
            col,
            uv: Vec2::ZERO,
            texture_id: 0.,
        }
    }
}

pub struct RenderCommands {
    buffer: Vec<RenderCommand>,
    screen_dim: Vec2,

    max_textures: usize,
    num_textures: usize,
    textures: Vec<BitmapId>,

    max_quads: usize,
    num_quads: usize,
    verts: Vec<TexturedVertex>,
}

impl RenderCommands {
    fn new(screen_dim: Vec2, max_quads: usize, max_textures: usize) -> Self {
        Self {
            buffer: Vec::new(),
            screen_dim,

            max_quads,
            num_quads: 0,
            verts: vec![TexturedVertex::default(); max_quads],

            max_textures,
            num_textures: 0,
            textures: vec![BitmapId::default(); max_textures],
        }
    }

    pub const fn get_window_dim(&self) -> Vec2 {
        self.screen_dim
    }

    pub fn begin_render_group(&mut self, view_proj: Mat4, color: Vec4) -> RenderGroup<'_> {
        RenderGroup::begin(self, view_proj, color)
    }
}

pub struct QuadBuilder<'rg, 'rc> {
    render_group: &'rg mut RenderGroup<'rc>,
    pos: Vec2,
    dim: Vec2,
    color: Vec4,
    texture: Option<(BitmapId, [Vec2; 4])>,
}

impl<'rg, 'rc> QuadBuilder<'rg, 'rc> {
    fn new(render_group: &'rg mut RenderGroup<'rc>) -> Self {
        Self {
            render_group,
            pos: Vec2::ZERO,
            dim: Vec2::ZERO,
            color: (1., 1., 1., 1.).into(),
            texture: None,
        }
    }

    #[inline]
    pub fn set_pos(mut self, pos: Vec2) -> Self {
        self.pos = pos;
        self
    }

    #[inline]
    pub fn set_dim(mut self, dim: Vec2) -> Self {
        self.dim = dim;
        self
    }

    #[inline]
    pub fn set_width(mut self, width: f32) -> Self {
        self.dim.x = width;
        self
    }

    #[inline]
    pub fn set_height(mut self, height: f32) -> Self {
        self.dim.y = height;
        self
    }

    #[inline]
    pub fn set_color(mut self, color: Vec4) -> Self {
        self.color = color;
        self
    }

    #[inline]
    pub fn set_texture(mut self, bitmap: BitmapId, uv: [Vec2; 4]) -> Self {
        self.texture = Some((bitmap, uv));
        self
    }

    #[inline]
    pub fn push(self) -> &'rg mut RenderGroup<'rc> {
        let Self {
            render_group,
            pos,
            dim,
            color,
            texture,
        } = self;

        if let Some((bitmap, uv)) = texture {
            render_group.push_textured_quad_raw(pos, dim.x, dim.y, uv, bitmap, color);
        } else {
            render_group.push_quad(pos, dim.x, dim.y, color);
        }

        render_group
    }
}

pub struct RenderGroup<'rc> {
    commands: &'rc mut RenderCommands,
    starting_quad_index: usize,
    view_proj: Mat4,
}

impl<'rc> RenderGroup<'rc> {
    // TODO: Change the begin method so that it doesn't require the flags.
    fn begin(commands: &'rc mut RenderCommands, view_proj: Mat4, color: Vec4) -> Self {
        if commands.num_quads == 0 {
            commands.buffer.push(RenderCommand::BeginLayer(color));
        } else {
            commands.buffer.push(RenderCommand::ClearDepthBuffer);
        }

        let starting_quad_index = commands.num_quads;
        Self {
            commands,
            starting_quad_index,
            view_proj,
        }
    }

    pub fn end(self) -> &'rc mut RenderCommands {
        self.commands.buffer.push(RenderCommand::EndLayer(
            self.view_proj,
            self.starting_quad_index,
            self.commands.num_quads,
        ));

        self.commands
    }

    pub fn begin_quad(&mut self) -> QuadBuilder<'_, 'rc> {
        QuadBuilder::new(self)
    }

    pub const fn get_window_width(&self) -> f32 {
        self.commands.screen_dim.x
    }

    pub const fn get_window_height(&self) -> f32 {
        self.commands.screen_dim.y
    }

    pub const fn get_window_dim(&self) -> Vec2 {
        self.commands.screen_dim
    }

    pub fn push_quad(&mut self, pos: Vec2, width: f32, height: f32, color: Vec4) {
        let Vec2 { x, y } = pos;
        let offset_x = x + width;
        let offset_y = y + height;

        let current_quad = self.commands.num_quads;
        assert!(current_quad < self.commands.max_quads);

        let vert_0 = &mut self.commands.verts[current_quad * 4 + 0];
        *vert_0 = TexturedVertex::new((x, y, 0.).into(), color);
        vert_0.uv = Vec2::new(1., 0.);
        vert_0.texture_id = 0.;

        let vert_1 = &mut self.commands.verts[current_quad * 4 + 1];
        *vert_1 = TexturedVertex::new((x, offset_y, 0.).into(), color);
        vert_1.uv = Vec2::new(0., 0.);
        vert_1.texture_id = 0.;

        let vert_2 = &mut self.commands.verts[current_quad * 4 + 2];
        *vert_2 = TexturedVertex::new((offset_x, offset_y, 0.).into(), color);
        vert_2.uv = Vec2::new(0., 1.);
        vert_2.texture_id = 0.;

        let vert_3 = &mut self.commands.verts[current_quad * 4 + 3];
        *vert_3 = TexturedVertex::new((offset_x, y, 0.).into(), color);
        vert_3.uv = Vec2::new(1., 1.);
        vert_3.texture_id = 0.;

        self.commands.num_quads += 1;
    }

    pub fn push_textured_quad(
        &mut self,
        pos: Vec2,
        width: f32,
        height: f32,
        bitmap: BitmapId,
        color: Vec4,
    ) {
        let uv = [
            (0., 1.).into(),
            (0., 0.).into(),
            (1., 0.).into(),
            (1., 1.).into(),
        ];
        self.push_textured_quad_raw(pos, width, height, uv, bitmap, color);
    }

    pub fn push_textured_quad_raw(
        &mut self,
        pos: Vec2,
        width: f32,
        height: f32,
        uv: [Vec2; 4],
        bitmap: BitmapId,
        color: Vec4,
    ) {
        let Vec2 { x, y } = pos;
        let offset_x = x + width;
        let offset_y = y + height;

        let current_quad = self.commands.num_quads;
        assert!(current_quad < self.commands.max_quads);

        let mut texture_slot = -1;
        for (id, bit) in self
            .commands
            .textures
            .iter()
            .enumerate()
            .take(self.commands.num_textures)
        {
            if *bit == bitmap {
                texture_slot = id as i32;
                break;
            }
        }

        if texture_slot == -1 {
            assert!(self.commands.num_textures < self.commands.max_textures);

            texture_slot = self.commands.num_textures as i32;
            self.commands.textures[self.commands.num_textures] = bitmap;
            self.commands.num_textures += 1;
        }

        self.commands.verts[current_quad * 4 + 0] = TexturedVertex::new((x, y, 0.).into(), color);
        self.commands.verts[current_quad * 4 + 0].uv = uv[0];
        self.commands.verts[current_quad * 4 + 0].texture_id = (texture_slot + 1) as _;

        self.commands.verts[current_quad * 4 + 1] =
            TexturedVertex::new((x, offset_y, 0.).into(), color);
        self.commands.verts[current_quad * 4 + 1].uv = uv[1];
        self.commands.verts[current_quad * 4 + 1].texture_id = (texture_slot + 1) as _;

        self.commands.verts[current_quad * 4 + 2] =
            TexturedVertex::new((offset_x, offset_y, 0.).into(), color);
        self.commands.verts[current_quad * 4 + 2].uv = uv[2];
        self.commands.verts[current_quad * 4 + 2].texture_id = (texture_slot + 1) as _;

        self.commands.verts[current_quad * 4 + 3] =
            TexturedVertex::new((offset_x, y, 0.).into(), color);
        self.commands.verts[current_quad * 4 + 3].uv = uv[3];
        self.commands.verts[current_quad * 4 + 3].texture_id = (texture_slot + 1) as _;

        self.commands.num_quads += 1;
    }
}
