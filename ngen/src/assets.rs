mod font;
mod parser;
mod reader;
mod tokenizer;
mod writer;

use math::Vec2;
use sdl2::{image::LoadSurface, surface::Surface};
use std::{collections::HashMap, fmt, fs, hash::Hash, io, path::PathBuf, time};

use crate::trie::*;
pub use font::Font;
use parser::ParsedAssetEntries;

#[derive(Clone, Copy, Debug, Default, Eq, Hash, PartialEq)]
pub struct BitmapId(pub usize);

#[derive(Clone, Copy)]
pub struct Sprite {
    pub bitmap_id: BitmapId,
    pub uv: [Vec2; 4],
    pub pos: Vec2,
    pub dim: Vec2,
}

pub struct Bitmap {
    pub width: u32,
    pub height: u32,
    pub data: Vec<u32>,
}

impl Bitmap {
    pub fn new(width: u32, height: u32, data: Vec<u32>) -> Self {
        assert!(data.len() >= (width * height) as usize);

        Self {
            width,
            height,
            data,
        }
    }

    #[inline]
    pub fn get_uv_coords(&self, pos: Vec2, width: f32, height: f32) -> [Vec2; 4] {
        let Vec2 { mut x, mut y } = pos;
        let (mut x_offset, mut y_offset) = (x + width, y + height);

        x /= self.width as f32;
        x_offset /= self.width as f32;
        y /= self.height as f32;
        y_offset /= self.height as f32;

        [
            (x, y_offset).into(),
            (x, y).into(),
            (x_offset, y).into(),
            (x_offset, y_offset).into(),
        ]
    }
}

pub trait AssetType: AsRef<str> + Copy + Eq + Hash {}
impl<T> AssetType for T where T: AsRef<str> + Copy + Eq + Hash {}

pub trait AssetTag: AsRef<str> + Copy + Eq + Hash {}
impl<T> AssetTag for T where T: AsRef<str> + Copy + Eq + Hash {}

pub trait Assets {
    fn get_bitmap(&self, bitmap_id: BitmapId) -> Option<Bitmap>;
}

pub struct Error(String);

impl From<Error> for String {
    fn from(Error(s): Error) -> Self {
        s
    }
}

fn file_is(file: &PathBuf, expected_ext: &str) -> bool {
    if let Some(ext) = file.extension() {
        if let Some(ext) = ext.to_str() {
            return ext == expected_ext;
        }
    }

    return false;
}

fn file_is_ngt(file: &PathBuf) -> bool {
    file_is(file, "ngt")
}

fn file_is_nga(file: &PathBuf) -> bool {
    file_is(file, "nga")
}

fn find_asset_files(path: PathBuf) -> io::Result<Vec<PathBuf>> {
    let mut dirs = vec![path];
    let mut asset_files = vec![];

    while let Some(dir) = dirs.pop() {
        for entry in fs::read_dir(dir)? {
            if let Ok(entry) = entry {
                let path = entry.path();
                if path.is_dir() {
                    dirs.push(path);
                } else if file_is_ngt(&path) {
                    asset_files.push(path);
                }
            }
        }
    }

    Ok(asset_files)
}

fn get_timestamp(file: &PathBuf) -> Result<u128, Error> {
    let last_write_time = file.metadata()?.modified()?;
    let ms_since_epoch = last_write_time
        .duration_since(time::SystemTime::UNIX_EPOCH)?
        .as_millis();
    Ok(ms_since_epoch)
}

fn load_spritesheet(assets: &ParsedAssetEntries<'_>, base_path: &PathBuf) -> Result<Bitmap, Error> {
    if let Some(spritesheet) = assets.asset_info.spritesheet {
        let spritesheet_path = base_path.join(spritesheet).canonicalize()?;
        let image = Surface::from_file(spritesheet_path)?;
        let image_width = image.width();
        let image_height = image.height();

        let data = image.with_lock(|bytes| {
            let mut data = Vec::with_capacity((image_width * image_height) as usize);

            for pixel in bytes.chunks(4) {
                match pixel {
                    [b, g, r, a] => {
                        let af = *a as f32 / 255.;
                        let r = ((*r as f32) * af) as u32;
                        let g = ((*g as f32) * af) as u32;
                        let b = ((*b as f32) * af) as u32;

                        let color = (*a as u32) << 24 | r << 16 | g << 8 | b;
                        data.push(color);
                    }

                    _ => {
                        return Err(Error(format!(
                        "Image is malformed. Expected four bytes per pixel. Found {}-byte pixel.",
                        pixel.len(),
                    )))
                    }
                }
            }

            Ok(data)
        })?;

        let bitmap = Bitmap::new(image_width, image_height, data);
        Ok(bitmap)
    } else {
        Err(Error(
            "Asset pack does not have a spritesheet set, which is currently required.".to_owned(),
        ))
    }
}

pub struct AssetEntry<TType> {
    pub asset_type: TType,
    pub width: f32,
    pub height: f32,
    pub pos: Vec2,
    pub tag_start: usize,
    pub tag_end: usize,
}

pub struct AssetPack<TType, TTag> {
    pub last_updated: u128,
    pub name: String,
    pub author: String,
    pub description: String,
    pub tags: Vec<(TTag, f32)>,
    pub entries: HashMap<TType, Vec<AssetEntry<TType>>>,
    pub bitmap: BitmapId,
}

impl<TType, TTag> AssetPack<TType, TTag>
where
    TType: AssetType,
    TTag: AssetTag,
{
    fn get_tags(&self, entry: &AssetEntry<TType>) -> &[(TTag, f32)] {
        &self.tags[entry.tag_start..entry.tag_end]
    }

    /// Looks through the asset entries to find an asset that matches the input vector the closest.
    pub fn find(
        &self,
        asset_type: TType,
        mut weight: f32,
        asset_vector: &HashMap<TTag, f32>,
    ) -> Option<(f32, BitmapId, &AssetEntry<TType>)> {
        if let Some(assets) = self.entries.get(&asset_type) {
            let mut matched = None;

            for asset_entry in assets {
                let mut asset_weight = 0.;

                let tags = self.get_tags(&asset_entry);
                for (asset_tag, v) in tags {
                    if let Some(a) = asset_vector.get(asset_tag) {
                        let distance = (a - v).abs();
                        let difference = 1. - distance;
                        asset_weight += difference;
                    }
                }

                if asset_weight > weight {
                    weight = asset_weight;
                    matched = Some(asset_entry);
                }
            }

            return matched.map(|matched| (weight, self.bitmap, matched));
        }

        None
    }
}

pub struct AssetPacks<TType, TTag> {
    asset_packs: Vec<AssetPack<TType, TTag>>,
    bitmaps: Vec<Bitmap>,
    asset_type_map: Trie<TType>, // TODO: We will use these during runtime when we implement the ability to check for asset file updates.
    asset_tag_map: Trie<TTag>,
}

impl<TType, TTag> AssetPacks<TType, TTag>
where
    TType: IterVariants + AssetType,
    TTag: IterVariants + AssetTag,
{
    pub fn load_with_maps(
        path: &str,
        asset_type_map: Trie<TType>,
        asset_tag_map: Trie<TTag>,
    ) -> Result<Self, Error> {
        use io::{Read, Write};

        let asset_files = find_asset_files(path.into())?;
        let mut asset_packs = Vec::with_capacity(asset_files.len());
        let mut bitmaps = Vec::with_capacity(asset_files.len());

        for mut asset_file in asset_files {
            let mut base_directory = asset_file.clone();
            base_directory.pop();

            let mut file = fs::File::open(asset_file.clone())?;
            let mut contents = String::new();
            file.read_to_string(&mut contents)?;

            let mut assets = parser::parse(&contents)?;
            let spritesheet = load_spritesheet(&assets, &base_directory)?;

            let ms_since_epoch = get_timestamp(&asset_file)?;

            let mut binary_data = Vec::with_capacity(1024 * 1024);
            let mut writer = writer::AssetWriter::init(
                ms_since_epoch,
                &mut assets,
                &spritesheet,
                &mut binary_data,
            );

            let written = writer.write()?;

            asset_file.set_extension("nga");
            let mut output_file = fs::File::create(asset_file)?;
            output_file.set_len(written as _)?;
            output_file.write_all(&binary_data)?;

            let asset_pack =
                reader::AssetFileReader::new(&binary_data, &asset_type_map, &asset_tag_map)
                    .load(&mut bitmaps)?;
            asset_packs.push(asset_pack);
        }

        Ok(Self {
            asset_packs,
            bitmaps,
            asset_type_map,
            asset_tag_map,
        })
    }

    pub fn load(path: &str) -> Result<Self, Error>
    where
        TType: IterVariants,
        TTag: IterVariants,
    {
        let asset_type_map = TType::iter_variants().into_iter().collect();
        let asset_tag_map = TTag::iter_variants().into_iter().collect();
        Self::load_with_maps(path, asset_type_map, asset_tag_map)
    }

    pub fn find_sprite(
        &self,
        asset_type: TType,
        asset_vector: &HashMap<TTag, f32>,
    ) -> Option<[Vec2; 4]> {
        let mut matched = None;
        let mut weight = 0.;

        for pack in &self.asset_packs {
            if let Some((new_weight, bitmap, entry)) = pack.find(asset_type, weight, asset_vector) {
                weight = new_weight;
                matched = Some((bitmap, entry));
            }
        }

        matched.map(|(BitmapId(bitmap), entry)| {
            let bitmap = &self.bitmaps[bitmap];
            bitmap.get_uv_coords(entry.pos, entry.width, entry.height)
        })
    }
}

pub trait AssetManager {
    fn load_bitmap<P: Into<PathBuf>>(&mut self, path: P) -> Option<BitmapId>;
    fn get_bitmap(&self, bitmap_id: &BitmapId) -> Option<&Bitmap>;
    fn push_bitmap(&mut self, bitmap: Bitmap) -> BitmapId;

    fn store_bitmap(&mut self, width: u32, height: u32, bitmap_data: Vec<u32>) -> BitmapId;

    fn create_sprite(
        &self,
        bitmap_id: BitmapId,
        pos: Vec2,
        width: f32,
        height: f32,
    ) -> Option<Sprite>;
}

impl<TType, TTag> AssetManager for AssetPacks<TType, TTag> {
    fn load_bitmap<P: Into<PathBuf>>(&mut self, path: P) -> Option<BitmapId> {
        let image = Surface::from_file(path.into()).ok()?;
        image.with_lock(|bytes| {
            let width = image.width();
            let height = image.height();
            let mut data = Vec::with_capacity((width * height) as usize);

            for pixel in bytes.chunks(4) {
                match pixel {
                    [b, g, r, a] => {
                        let af = *a as f32 / 255.;
                        let r = ((*r as f32) * af) as u32;
                        let g = ((*g as f32) * af) as u32;
                        let b = ((*b as f32) * af) as u32;

                        let color = (*a as u32) << 24 | r << 16 | g << 8 | b << 0;

                        data.push(color);
                    }

                    _ => unreachable!(),
                }
            }

            let bitmap_id = self.store_bitmap(width, height, data);
            Some(bitmap_id)
        })
    }

    fn push_bitmap(&mut self, bitmap: Bitmap) -> BitmapId {
        let bitmap_id = self.bitmaps.len();
        self.bitmaps.push(bitmap);
        BitmapId(bitmap_id)
    }

    fn get_bitmap(&self, BitmapId(id): &BitmapId) -> Option<&Bitmap> {
        self.bitmaps.get(*id)
    }

    fn store_bitmap(&mut self, width: u32, height: u32, bitmap_data: Vec<u32>) -> BitmapId {
        let bitmap_id = self.bitmaps.len();
        let bitmap = Bitmap::new(width, height, bitmap_data);
        self.bitmaps.push(bitmap);

        BitmapId(bitmap_id)
    }

    fn create_sprite(
        &self,
        bitmap_id: BitmapId,
        pos: Vec2,
        width: f32,
        height: f32,
    ) -> Option<Sprite> {
        let bitmap = self.get_bitmap(&bitmap_id)?;
        let uv = bitmap.get_uv_coords(pos, width, height);
        let dim = (width, height).into();

        Some(Sprite {
            bitmap_id,
            uv,
            pos,
            dim,
        })
    }
}

// Display/Debug impls

impl<D: fmt::Display> From<D> for Error {
    fn from(error: D) -> Self {
        Self(error.to_string())
    }
}

impl fmt::Debug for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}
