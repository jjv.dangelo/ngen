#![warn(rust_2018_idioms)]

pub mod array;
pub mod assets;
pub mod camera;
pub mod controls;
pub mod io;
pub mod queue;
pub mod renderer;
mod time;
pub mod trie;

use assets::AssetPacks;
use math::Vec2;
use renderer::{opengl::OpenGLRenderer, RenderCommands};
pub use time::Tick;
pub use trie::Trie;

/// The `Simulation` communicates to the `Designer` on each timestep if the
/// simulation should `Continuation::Continue` or if it should
/// `Continuation::Exit`.
pub enum Continuation {
    /// The simulation uses `Continue` to express to the platform that it should
    /// continue the simulation after the given timestep.
    Continue,

    /// The simulation uses `Exit` to express to the platform that it should
    /// end the simulation after the given timestep.
    Break,
}

impl From<()> for Continuation {
    fn from(_: ()) -> Self {
        Self::Continue
    }
}

impl From<bool> for Continuation {
    fn from(cont: bool) -> Self {
        if cont {
            Self::Continue
        } else {
            Self::Break
        }
    }
}

/// A simluation for the platform to execute.
pub trait Simulation<AType, ATag> {
    /// The platform will call `update` on each tick of the timestep.
    fn update(
        &mut self,
        input: &mut Input,
        assets: &mut AssetPacks<AType, ATag>,
        screen_dim: Vec2,
        tick: Tick,
    ) -> Continuation;

    /// The platform will call `render` after the available timesteps.
    fn render(
        &mut self,
        input: &mut Input,
        assets: &mut AssetPacks<AType, ATag>,
        render_commands: &mut RenderCommands,
    );
}

pub struct Input {
    /// The current keyboard state for the timestep.
    pub keyboard: controls::Keyboard,

    /// The current mouse state for the timestep.
    pub mouse: controls::Mouse,
}

impl Input {
    fn new() -> Self {
        let keyboard = Default::default();
        let mouse = Default::default();

        Self { keyboard, mouse }
    }
}

pub struct Designer {
    asset_folders: Vec<String>,
    window_dim: (u32, u32),
    window_title: &'static str,
    max_quads: usize,
}

impl Designer {
    pub const fn init() -> Self {
        Self {
            asset_folders: Vec::new(),
            window_dim: (1280, 720),
            window_title: "ngen",
            max_quads: 10_000,
        }
    }

    pub fn inject_asset_folder<S: Into<String>>(mut self, path: S) -> Self {
        self.asset_folders.push(path.into());
        self
    }

    pub fn initial_window_size(mut self, width: u32, height: u32) -> Self {
        self.window_dim = (width, height);
        self
    }

    pub fn window_title(mut self, window_title: &'static str) -> Self {
        self.window_title = window_title;
        self
    }

    pub fn max_quads(mut self, max_quads: usize) -> Self {
        self.max_quads = max_quads;
        self
    }

    pub fn mount<T, A, S>(self, mut simulation: S) -> Result<(), String>
    where
        T: crate::trie::IterVariants + assets::AssetType,
        A: crate::trie::IterVariants + assets::AssetTag,
        S: Simulation<T, A>,
    {
        let Self {
            mut asset_folders,
            window_dim: (width, height),
            window_title,
            max_quads,
        } = self;

        let mut context = sdl2::init()?;
        sdl2::image::init(sdl2::image::InitFlag::all())?;

        let mut renderer = Box::new(OpenGLRenderer::new(
            &mut context,
            width,
            height,
            window_title,
            max_quads,
        )?);
        let mut assets = Box::new({
            AssetPacks::load(
                &asset_folders
                    .pop()
                    .expect(
                        "We currently need at least one asset folder, and we're lazy right now so this panics."
                    )
            )?
        });
        let mut timer = time::Timer::new(&mut context, 0.01, 0.25)?;
        let mut input = Input::new();
        let mut event_pump = context.event_pump()?;

        'running: loop {
            for tick in timer.begin_frame() {
                input.mouse.new_frame();
                input.keyboard.new_frame();

                for event in event_pump.poll_iter() {
                    use sdl2::event::{Event, WindowEvent};
                    use sdl2::keyboard::Keycode;
                    use sdl2::mouse::MouseButton;

                    match event {
                        Event::Quit { .. } => break 'running,

                        Event::MouseMotion { x, y, .. } => {
                            input.mouse.pos = Vec2::new(x as _, y as _);
                        }

                        Event::MouseButtonDown {
                            mouse_btn: MouseButton::Left,
                            x,
                            y,
                            ..
                        } => {
                            input.mouse.pos = Vec2::new(x as _, y as _);
                            input.mouse.left_button.set_is_down(true);
                        }

                        Event::MouseButtonUp {
                            mouse_btn: MouseButton::Left,
                            x,
                            y,
                            ..
                        } => {
                            input.mouse.pos = Vec2::new(x as _, y as _);
                            input.mouse.left_button.set_is_down(false);
                        }

                        Event::KeyDown {
                            keycode: Some(key), ..
                        } => match key {
                            Keycode::W => input.keyboard.w.set_is_down(true),
                            Keycode::A => input.keyboard.a.set_is_down(true),
                            Keycode::S => input.keyboard.s.set_is_down(true),
                            Keycode::D => input.keyboard.d.set_is_down(true),
                            Keycode::Space => input.keyboard.space.set_is_down(true),
                            Keycode::Return => input.keyboard.enter.set_is_down(true),
                            Keycode::Escape => input.keyboard.esc.set_is_down(true),
                            Keycode::F1 => input.keyboard.f[01].set_is_down(true),
                            Keycode::F2 => input.keyboard.f[02].set_is_down(true),
                            Keycode::F3 => input.keyboard.f[03].set_is_down(true),
                            Keycode::F4 => input.keyboard.f[04].set_is_down(true),
                            Keycode::F5 => input.keyboard.f[05].set_is_down(true),
                            Keycode::F6 => input.keyboard.f[06].set_is_down(true),
                            Keycode::F7 => input.keyboard.f[07].set_is_down(true),
                            Keycode::F8 => input.keyboard.f[08].set_is_down(true),
                            Keycode::F9 => input.keyboard.f[09].set_is_down(true),
                            Keycode::F10 => input.keyboard.f[10].set_is_down(true),
                            Keycode::F11 => input.keyboard.f[11].set_is_down(true),
                            Keycode::F12 => input.keyboard.f[12].set_is_down(true),
                            _ => (),
                        },

                        Event::KeyUp {
                            keycode: Some(key), ..
                        } => match key {
                            Keycode::W => input.keyboard.w.set_is_down(false),
                            Keycode::A => input.keyboard.a.set_is_down(false),
                            Keycode::S => input.keyboard.s.set_is_down(false),
                            Keycode::D => input.keyboard.d.set_is_down(false),
                            Keycode::Space => input.keyboard.space.set_is_down(false),
                            Keycode::Return => input.keyboard.enter.set_is_down(false),
                            Keycode::Escape => input.keyboard.esc.set_is_down(false),
                            Keycode::F1 => input.keyboard.f[01].set_is_down(false),
                            Keycode::F2 => input.keyboard.f[02].set_is_down(false),
                            Keycode::F3 => input.keyboard.f[03].set_is_down(false),
                            Keycode::F4 => input.keyboard.f[04].set_is_down(false),
                            Keycode::F5 => input.keyboard.f[05].set_is_down(false),
                            Keycode::F6 => input.keyboard.f[06].set_is_down(false),
                            Keycode::F7 => input.keyboard.f[07].set_is_down(false),
                            Keycode::F8 => input.keyboard.f[08].set_is_down(false),
                            Keycode::F9 => input.keyboard.f[09].set_is_down(false),
                            Keycode::F10 => input.keyboard.f[10].set_is_down(false),
                            Keycode::F11 => input.keyboard.f[11].set_is_down(false),
                            Keycode::F12 => input.keyboard.f[12].set_is_down(false),
                            _ => (),
                        },

                        Event::Window {
                            win_event: WindowEvent::Resized(width, height),
                            ..
                        } => renderer.set_aspect_ratio(width, height),

                        _ => (),
                    }
                }

                match simulation.update(&mut input, &mut assets, renderer.get_screen_dim(), tick) {
                    Continuation::Continue => (),
                    Continuation::Break => break 'running,
                }
            } // End of clock frame

            let render_commands = renderer.begin_frame();
            simulation.render(&mut input, &mut assets, render_commands);

            renderer.render(&*assets);
        }

        Ok(())
    }
}
