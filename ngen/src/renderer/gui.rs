use crate::{
    assets::{Font, Sprite},
    controls::Mouse,
    renderer::RenderCommands,
};
use math::{Mat4, Vec2, Vec4};

enum Command {
    Quad {
        pos: Vec2,
        dim: Vec2,
        color: Vec4,
    },
    Sprite {
        pos: Vec2,
        dim: Vec2,
        color: Vec4,
        sprite: Sprite,
    },
}

pub struct Gui {
    font: Font,
    commands: Vec<Command>,
    mouse: Mouse,
    layout: Layout,
    layouts: Vec<Layout>,
}

#[derive(Clone, Copy)]
enum Layout {
    Fixed,
    Stack { pos: Vec2, padding: Vec2 },
    Vert { pos: Vec2, padding: Vec2 },
}

impl Layout {
    fn get_pos(&mut self, in_pos: Vec2, in_dim: Vec2) -> Vec2 {
        match self {
            Self::Fixed => in_pos,

            Self::Stack { pos, padding } => {
                let result = *pos + in_pos;
                pos.y = result.y + in_dim.y + padding.y;
                result
            }

            Self::Vert { pos, padding } => {
                let result = *pos + in_pos;
                pos.x = result.x + in_dim.x + padding.x;
                result
            }
        }
    }
}

impl Gui {
    pub fn new(font: Font) -> Self {
        let mouse = Mouse::default();
        let layout = Layout::Fixed;
        let layouts = vec![];
        Self {
            font,
            commands: Vec::new(),
            mouse,
            layout,
            layouts,
        }
    }

    pub fn begin_frame<'a>(&'a mut self, mouse: Mouse) {
        self.mouse = mouse;
        self.commands.clear();
        self.layouts.clear();
        self.layout = Layout::Fixed;
    }

    fn mouse_in(&self, pos: Vec2, dim: Vec2) -> bool {
        let Vec2 { x, y } = self.mouse.pos;
        let x_bounded = x >= pos.x && x <= (pos + dim).x;
        let y_bounded = y >= pos.y && y <= (pos + dim).y;
        let bounded = x_bounded && y_bounded;

        bounded
    }

    #[allow(dead_code)]
    pub fn push_fixed_layout(&mut self) {
        self.layouts.push(self.layout);
        self.layout = Layout::Fixed;
    }

    pub fn push_stack_layout(&mut self, pos: Vec2, height: f32, padding: Vec2) {
        let pos = self.layout.get_pos(pos, (height, 0.).into());
        self.layouts.push(self.layout);
        self.layout = Layout::Stack { pos, padding };
    }

    pub fn push_vert_layout(&mut self, pos: Vec2, width: f32, padding: Vec2) {
        let pos = self.layout.get_pos(pos, (0., width).into());
        self.layouts.push(self.layout);
        self.layout = Layout::Vert { pos, padding };
    }

    pub fn pop_layout(&mut self) {
        self.layout = self.layouts.pop().expect("More 'pops' than 'pushes'.");
    }

    pub fn panel(&mut self, pos: Vec2, dim: Vec2, color: Vec4) -> bool {
        let pos = self.layout.get_pos(pos, dim);
        self.commands.push(Command::Quad { pos, dim, color });

        self.mouse_in(pos, dim)
    }

    pub fn sprite(&mut self, pos: Vec2, dim: Vec2, color: Vec4, sprite: Sprite) -> bool {
        let pos = self.layout.get_pos(pos, dim);
        self.commands.push(Command::Sprite {
            pos,
            dim,
            color,
            sprite,
        });

        self.mouse_in(pos, dim)
    }

    pub fn write_text(&mut self, pos: Vec2, color: Vec4, text: &str) {
        let padding = 1.;
        let sprites = self.font.get_sprites(text);
        let width = sprites.iter().map(|s| s.dim.y + padding).sum();
        self.push_vert_layout(pos, width, Vec2::new(padding, padding));
        for sprite in sprites {
            self.sprite(Vec2::ZERO, sprite.dim, color, sprite);
        }
        self.pop_layout();
    }

    pub fn render(&self, render_commands: &mut RenderCommands) {
        let mut render_group =
            render_commands.begin_render_group(Mat4::IDENT, (1.0, 1.0, 1.0, 0.).into());
        let Vec2 {
            x: width,
            y: height,
        } = render_group.get_window_dim();

        for command in self.commands.iter().rev() {
            match command {
                Command::Quad {
                    mut pos,
                    mut dim,
                    color,
                } => {
                    dim.x /= width * 0.5;
                    dim.y /= height * 0.5;

                    pos.x = (pos.x / width) * 2. - 1.;
                    pos.y = -(pos.y / height) * 2. + 1. - dim.y;

                    render_group.push_quad(pos, dim.x, dim.y, *color);
                }

                Command::Sprite {
                    mut pos,
                    mut dim,
                    color,
                    sprite,
                } => {
                    dim.x /= width * 0.5;
                    dim.y /= height * 0.5;

                    pos.x = (pos.x / width) * 2. - 1.;
                    pos.y = -(pos.y / height) * 2. + 1. - dim.y;

                    render_group.push_textured_quad_raw(
                        pos,
                        dim.x,
                        dim.y,
                        sprite.uv,
                        sprite.bitmap_id,
                        *color,
                    );
                }
            }
        }

        render_group.end();
    }
}
