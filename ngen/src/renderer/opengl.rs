use super::{RenderCommand, RenderCommands, TexturedVertex};
use crate::assets::{AssetManager, BitmapId};
use math::{Vec2, Vec3, Vec4};
use sdl2::Sdl;
use std::collections::HashMap;

type Index = u32;

pub struct OpenGLRenderer {
    canvas: sdl2::render::WindowCanvas,
    screen_dim: Vec2,

    program_id: u32,
    vert_buffer_id: u32,
    index_buffer_id: u32,
    view_proj_id: i32,

    w_texture_id: u32, // 1x1 White texture
    samplers: Vec<i32>,

    commands: RenderCommands,

    textures: HashMap<BitmapId, u32>,
}

impl OpenGLRenderer {
    pub fn new(
        context: &mut Sdl,
        window_width: u32,
        window_height: u32,
        window_title: &str,
        max_quads: usize,
    ) -> Result<Self, String> {
        let video = context.video()?;
        let canvas = video
            .window(window_title, window_width, window_height)
            .opengl()
            .position_centered()
            .resizable()
            .build()
            .map_err(|e| e.to_string())?
            .into_canvas()
            .build()
            .map_err(|e| e.to_string())?;

        gl::load_with(|s| video.gl_get_proc_address(s) as *const _);
        canvas
            .window()
            .gl_set_context_to_current()
            .map_err(|e| e.to_string())?;

        unsafe {
            gl::Viewport(0, 0, window_width as _, window_height as _);

            gl::Enable(gl::CULL_FACE);
            gl::Enable(gl::TEXTURE_2D);
            gl::Enable(gl::DEPTH_TEST); // TODO: Test this.
            gl::Enable(gl::BLEND);
            gl::BlendFunc(gl::SRC_ALPHA, gl::ONE_MINUS_SRC_ALPHA);
            gl::Disable(gl::SRGB);

            gl::FrontFace(gl::CW);
            gl::CullFace(gl::BACK);
        }

        let program_id = load_shader(VERTEX_SHADER_SRC, FRAGMENT_SHADER_SRC)?;

        let get_texture_slot_name = |id: i32| -> [u8; 15] {
            let tens = id / 10;
            assert!(tens < 10);

            let mut result = *b"u_textures[\0\0\0\0";
            let mut next = 11;
            if tens > 0 {
                result[next] = ('0' as u8) + (tens as u8);
                next += 1;
            }

            result[next] = ('0' as u8) + ((id % 10) as u8);
            result[next + 1] = ']' as u8;

            result
        };

        let mut max_textures = 0;
        unsafe { gl::GetIntegerv(gl::MAX_TEXTURE_IMAGE_UNITS, &mut max_textures) };

        let mut samplers = Vec::with_capacity(max_textures as _);
        for i in 0..max_textures {
            let texture_slot_name = get_texture_slot_name(i);
            let uniform_location =
                unsafe { gl::GetUniformLocation(program_id, texture_slot_name.as_ptr() as _) };
            samplers.push(uniform_location);
        }

        let screen_dim = Vec2::new(window_width as _, window_height as _);
        let textures = HashMap::new();
        Ok(Self {
            canvas,
            screen_dim,
            program_id,
            vert_buffer_id: create_vertex_buffer(max_quads * 4),
            index_buffer_id: create_index_buffer(max_quads * 6),
            view_proj_id: unsafe {
                gl::GetUniformLocation(program_id, b"u_view_proj\0".as_ptr() as _)
            },
            w_texture_id: load_texture(&[0xFFFFFFFF], 1, 1),
            samplers,
            commands: RenderCommands::new(screen_dim, max_quads, (max_textures.min(96)) as _),
            textures,
        })
    }

    pub fn set_aspect_ratio(&mut self, width: i32, height: i32) {
        self.screen_dim = Vec2::new(width as _, height as _);
        unsafe { gl::Viewport(0, 0, width as _, height as _) };
    }

    pub const fn get_screen_dim(&self) -> Vec2 {
        self.screen_dim
    }

    pub fn begin_frame(&mut self) -> &mut RenderCommands {
        self.commands.screen_dim = self.screen_dim;
        self.commands.num_textures = 0;
        self.commands.num_quads = 0;
        self.commands.buffer.clear();

        &mut self.commands
    }

    pub fn render<A: AssetManager>(&mut self, assets: &A) {
        for command in self.commands.buffer.iter() {
            use std::{mem, ptr};
            use RenderCommand::*;

            match command {
                BeginLayer(Vec4 { x, y, z, w }) => unsafe {
                    gl::ClearColor(*x, *y, *z, *w);
                    gl::Clear(gl::COLOR_BUFFER_BIT | gl::DEPTH_BUFFER_BIT);
                },

                EndLayer(view_proj, start, end) => unsafe {
                    gl::UseProgram(self.program_id);

                    gl::UniformMatrix4fv(
                        self.view_proj_id,
                        1,
                        gl::FALSE,
                        view_proj as *const _ as *const _,
                    );

                    gl::BindTextureUnit(0, self.w_texture_id);
                    gl::ActiveTexture(gl::TEXTURE0);
                    gl::BindTexture(gl::TEXTURE_2D, self.w_texture_id);
                    gl::Uniform1i(self.samplers[0], 0);

                    for (id, bitmap_id) in self
                        .commands
                        .textures
                        .iter()
                        .enumerate()
                        .take(self.commands.num_textures)
                    {
                        // TODO: Put this into a texture manager so we can also evict
                        // unused textures in the future.
                        if let Some(texture_id) = self.textures.get(bitmap_id) {
                            gl::BindTextureUnit((id + 1) as _, *texture_id);
                            gl::ActiveTexture(gl::TEXTURE0 + (id as u32) + 1);
                            gl::BindTexture(gl::TEXTURE_2D, *texture_id);
                            gl::Uniform1i(self.samplers[id + 1], (id as i32) + 1);
                        } else {
                            // TODO: This should move out of the renderer and get streamed in.
                            if let Some(bitmap) = assets.get_bitmap(bitmap_id) {
                                let texture_id =
                                    load_texture(&bitmap.data[..], bitmap.width, bitmap.height);
                                self.textures.insert(*bitmap_id, texture_id);
                            }
                        }
                    }

                    let num_quads = *end - *start;

                    let total_size = num_quads * mem::size_of::<TexturedVertex>() * 4;
                    let vert_offset = self.commands.verts.as_ptr().offset((*start * 4) as _);
                    gl::BindBuffer(gl::ARRAY_BUFFER, self.vert_buffer_id);
                    gl::BufferSubData(gl::ARRAY_BUFFER, 0, total_size as _, vert_offset as _);

                    let num_indices = num_quads * 6;
                    gl::BindBuffer(gl::ELEMENT_ARRAY_BUFFER, self.index_buffer_id);
                    gl::BindVertexArray(self.vert_buffer_id);
                    gl::DrawElements(
                        gl::TRIANGLES,
                        num_indices as _,
                        gl::UNSIGNED_INT,
                        ptr::null(),
                    );

                    gl::BindBuffer(gl::ELEMENT_ARRAY_BUFFER, 0);
                    gl::BindBuffer(gl::ARRAY_BUFFER, 0);
                    gl::UseProgram(0);
                },

                ClearDepthBuffer => unsafe { gl::Clear(gl::DEPTH_BUFFER_BIT) },

                ClearColor(Vec4 { x, y, z, w }) => unsafe { gl::ClearColor(*x, *y, *z, *w) },
            }
        }

        self.canvas.present();
    }
}

fn create_vertex_buffer(max_vert_count: usize) -> u32 {
    use std::{mem, ptr};

    const VERT_SIZE: usize = mem::size_of::<TexturedVertex>();
    let buffer_size = (VERT_SIZE * max_vert_count) as isize;

    unsafe {
        let mut buffer_id = 0;
        gl::GenBuffers(1, &mut buffer_id);
        gl::BindBuffer(gl::ARRAY_BUFFER, buffer_id);
        gl::BufferData(gl::ARRAY_BUFFER, buffer_size, ptr::null(), gl::DYNAMIC_DRAW);

        let mut attrib = 0;
        let mut total_offset = 0;
        gl::EnableVertexAttribArray(attrib); // Position
        gl::VertexAttribPointer(
            attrib,
            3,
            gl::FLOAT,
            gl::FALSE,
            VERT_SIZE as _,
            total_offset as *const _,
        );

        attrib += 1;
        total_offset += std::mem::size_of::<Vec3>();
        gl::EnableVertexAttribArray(attrib); // Color
        gl::VertexAttribPointer(
            attrib,
            4,
            gl::FLOAT,
            gl::FALSE,
            VERT_SIZE as _,
            total_offset as *const _,
        );

        attrib += 1;
        total_offset += std::mem::size_of::<Vec4>();
        gl::EnableVertexAttribArray(attrib); // TexCoord
        gl::VertexAttribPointer(
            attrib,
            2,
            gl::FLOAT,
            gl::FALSE,
            VERT_SIZE as _,
            total_offset as *const _,
        );

        attrib += 1;
        total_offset += std::mem::size_of::<Vec2>();
        gl::EnableVertexAttribArray(attrib); // TexId
        gl::VertexAttribPointer(
            attrib,
            1,
            gl::FLOAT,
            gl::FALSE,
            VERT_SIZE as _,
            total_offset as *const _,
        );

        buffer_id
    }
}

fn create_index_buffer(max_index_count: usize) -> u32 {
    const INDEX_SIZE: usize = std::mem::size_of::<Index>();
    let buffer_size = (max_index_count * INDEX_SIZE) as isize;

    let index_buffer = {
        let max_quads = max_index_count / 6;
        let mut buffer = vec![0 as Index; max_index_count];
        for quad in 0..max_quads {
            buffer[quad * 6 + 0] = (quad * 4 + 0) as Index;
            buffer[quad * 6 + 1] = (quad * 4 + 1) as Index;
            buffer[quad * 6 + 2] = (quad * 4 + 2) as Index;
            buffer[quad * 6 + 3] = (quad * 4 + 2) as Index;
            buffer[quad * 6 + 4] = (quad * 4 + 3) as Index;
            buffer[quad * 6 + 5] = (quad * 4 + 0) as Index;
        }

        buffer
    };

    unsafe {
        let mut buffer_id = 0;
        gl::CreateBuffers(1, &mut buffer_id);
        gl::BindBuffer(gl::ELEMENT_ARRAY_BUFFER, buffer_id);
        gl::BufferData(
            gl::ELEMENT_ARRAY_BUFFER,
            buffer_size,
            index_buffer.as_ptr() as _,
            gl::STATIC_DRAW,
        );

        gl::BindBuffer(gl::ELEMENT_ARRAY_BUFFER, 0); // Unbind the buffer

        buffer_id
    }
}

fn load_texture(image: &[u32], width: u32, height: u32) -> u32 {
    unsafe {
        let mut id = 0;

        gl::GenTextures(1, &mut id);
        gl::BindTexture(gl::TEXTURE_2D, id);
        gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_WRAP_S, gl::REPEAT as _);
        gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_WRAP_T, gl::REPEAT as _);
        gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MIN_FILTER, gl::LINEAR as _);
        gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MAG_FILTER, gl::LINEAR as _);
        gl::TexImage2D(
            gl::TEXTURE_2D,
            0,
            gl::RGBA as _,
            width as i32,
            height as i32,
            0,
            gl::RGBA,
            gl::UNSIGNED_BYTE,
            image.as_ptr() as _,
        );
        gl::GenerateMipmap(gl::TEXTURE_2D);

        gl::BindTexture(gl::TEXTURE_2D, 0);

        id
    }
}

fn load_shader(vertex_source: &str, fragment_source: &str) -> Result<u32, String> {
    use std::{ffi, ptr};

    unsafe {
        let vertex_shader_id = gl::CreateShader(gl::VERTEX_SHADER);
        let fragment_shader_id = gl::CreateShader(gl::FRAGMENT_SHADER);

        let vertex_shader_src = ffi::CString::new(vertex_source)
            .map_err(|_| "Invalid vertex shader source".to_owned())?;
        let fragment_shader_src = ffi::CString::new(fragment_source)
            .map_err(|_| "Invalid fragment shader source".to_owned())?;

        gl::ShaderSource(
            vertex_shader_id,
            1,
            &vertex_shader_src.as_ptr(),
            ptr::null(),
        );
        gl::CompileShader(vertex_shader_id);
        check_error_for_id(vertex_shader_id)?;

        gl::ShaderSource(
            fragment_shader_id,
            1,
            &fragment_shader_src.as_ptr(),
            ptr::null(),
        );
        gl::CompileShader(fragment_shader_id);
        check_error_for_id(fragment_shader_id)?;

        let program_id = gl::CreateProgram();
        gl::AttachShader(program_id, vertex_shader_id);
        gl::AttachShader(program_id, fragment_shader_id);

        gl::LinkProgram(program_id);
        check_error_for_id(program_id)?;

        gl::DetachShader(program_id, vertex_shader_id);
        gl::DetachShader(program_id, fragment_shader_id);

        gl::DeleteShader(vertex_shader_id);
        gl::DeleteShader(fragment_shader_id);

        Ok(program_id)
    }
}

fn check_error_for_id(id: u32) -> Result<(), String> {
    use std::{ffi, ptr};

    let mut log_length = 0;
    unsafe {
        gl::GetShaderiv(id, gl::INFO_LOG_LENGTH, &mut log_length);
        if log_length != 0 {
            let mut log = vec![0u8; log_length as usize];
            gl::GetShaderInfoLog(id, log_length, ptr::null_mut(), log.as_mut_ptr() as _);
            log.pop();

            let log = ffi::CString::new(log)
                .map_err(|_| "Couldn't generate shader error log".to_owned())?;
            return Err(log
                .into_string()
                .map_err(|_| "Couldn't generate shader error log".to_owned())?);
        }
    }

    Ok(())
}

const VERTEX_SHADER_SRC: &str = r#"
    #version 130

    in vec3 a_position;
    in vec4 a_color;
    in vec2 a_tex_coord;
    in float a_texture_id;

    uniform mat4 u_view_proj;

    out vec4 v_color;
    out vec2 v_tex_coord;
    out float v_texture_id;

    void main() {
        gl_Position = u_view_proj * vec4(a_position, 1);
        v_color = a_color;
        v_tex_coord = a_tex_coord;
        v_texture_id = a_texture_id;
    }
"#;

const FRAGMENT_SHADER_SRC: &str = r#"
    #version 130

    in vec4 v_color;
    in vec2 v_tex_coord;
    in float v_texture_id;

    uniform sampler2D u_textures[96];

    void main() {
        // Unfortunately, we can't dynamically index into u_textures[].
        // TODO: Turn this into a binary search instead of a large case statement.
        vec4 sampled = vec4(1, 1, 1, 1);
        int texture_id = int(v_texture_id);
        switch (texture_id) {
            case 0:  { sampled = texture(u_textures[0],  v_tex_coord); } break;
            case 1:  { sampled = texture(u_textures[1],  v_tex_coord); } break;
            case 2:  { sampled = texture(u_textures[2],  v_tex_coord); } break;
            case 3:  { sampled = texture(u_textures[3],  v_tex_coord); } break;
            case 4:  { sampled = texture(u_textures[4],  v_tex_coord); } break;
            case 5:  { sampled = texture(u_textures[5],  v_tex_coord); } break;
            case 6:  { sampled = texture(u_textures[6],  v_tex_coord); } break;
            case 7:  { sampled = texture(u_textures[7],  v_tex_coord); } break;
            case 8:  { sampled = texture(u_textures[8],  v_tex_coord); } break;
            case 9:  { sampled = texture(u_textures[9],  v_tex_coord); } break;
            case 10: { sampled = texture(u_textures[10], v_tex_coord); } break;
            case 11: { sampled = texture(u_textures[11], v_tex_coord); } break;
            case 12: { sampled = texture(u_textures[12], v_tex_coord); } break;
            case 13: { sampled = texture(u_textures[13], v_tex_coord); } break;
            case 14: { sampled = texture(u_textures[14], v_tex_coord); } break;
            case 15: { sampled = texture(u_textures[15], v_tex_coord); } break;
            case 16: { sampled = texture(u_textures[16], v_tex_coord); } break;
            case 17: { sampled = texture(u_textures[17], v_tex_coord); } break;
            case 18: { sampled = texture(u_textures[18], v_tex_coord); } break;
            case 19: { sampled = texture(u_textures[19], v_tex_coord); } break;
            case 20: { sampled = texture(u_textures[20], v_tex_coord); } break;
            case 21: { sampled = texture(u_textures[21], v_tex_coord); } break;
            case 22: { sampled = texture(u_textures[22], v_tex_coord); } break;
            case 23: { sampled = texture(u_textures[23], v_tex_coord); } break;
            case 24: { sampled = texture(u_textures[24], v_tex_coord); } break;
            case 25: { sampled = texture(u_textures[25], v_tex_coord); } break;
            case 26: { sampled = texture(u_textures[26], v_tex_coord); } break;
            case 27: { sampled = texture(u_textures[27], v_tex_coord); } break;
            case 28: { sampled = texture(u_textures[28], v_tex_coord); } break;
            case 29: { sampled = texture(u_textures[29], v_tex_coord); } break;
            case 30: { sampled = texture(u_textures[30], v_tex_coord); } break;
            case 31: { sampled = texture(u_textures[31], v_tex_coord); } break;
        };

        gl_FragColor = sampled * v_color;
    }
"#;
