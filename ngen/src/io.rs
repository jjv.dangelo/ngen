use std::{convert, io};

pub unsafe fn to_bytes<T: Sized>(t: &T) -> &[u8] {
    std::slice::from_raw_parts((t as *const T) as *const u8, std::mem::size_of::<T>())
}

pub unsafe fn slice_to_bytes<T: Sized>(t: &[T]) -> &[u8] {
    std::slice::from_raw_parts(t.as_ptr() as *const u8, std::mem::size_of::<T>() * t.len())
}

pub unsafe fn try_from_bytes<T: Sized>(bytes: &[u8]) -> Option<&T> {
    if bytes.len() != std::mem::size_of::<T>() {
        return None;
    }

    let ptr = bytes.as_ptr() as *const T;
    ptr.as_ref()
}

pub trait BinaryWrite: io::Write {
    unsafe fn binary_write<T: Sized>(&mut self, t: &T) -> io::Result<usize> {
        let bytes = to_bytes(t);
        self.write(bytes)
    }

    unsafe fn binary_write_slice<T: Sized>(&mut self, t: &[T]) -> io::Result<usize> {
        let bytes = slice_to_bytes(t);
        self.write(bytes)
    }

    unsafe fn binary_write_str(&mut self, s: &str) -> io::Result<usize> {
        let bytes = s.as_bytes();
        self.write(bytes)
    }
}

impl<T: io::Write> BinaryWrite for T {}

pub trait BinaryRead: convert::AsRef<[u8]> {
    unsafe fn binary_read<'a, T: Sized>(&'a self) -> Option<&'a T> {
        try_from_bytes(self.as_ref())
    }
}

impl<T: AsRef<[u8]>> BinaryRead for T {}
