use math::Vec2;

#[derive(Clone, Copy, Default)]
pub struct Button {
    ended_down: bool,
    half_transition_count: u32,
}

impl Button {
    #[inline]
    pub fn was_down(&self) -> bool {
        self.half_transition_count > 1 || self.half_transition_count == 1 && self.ended_down
    }

    #[inline]
    pub const fn is_down(&self) -> bool {
        self.ended_down
    }

    #[inline]
    pub fn set_is_down(&mut self, is_down: bool) {
        if self.ended_down != is_down {
            self.ended_down = is_down;
            self.half_transition_count += 1;
        }
    }
}

#[derive(Clone, Copy, Default)]
pub struct Mouse {
    pub pos: Vec2,
    pub scroll_delta: i32,

    pub left_button: Button,
    pub right_button: Button,
    pub middle_button: Button,
}

impl Mouse {
    pub(crate) fn new_frame(&mut self) {
        self.left_button.half_transition_count = 0;
        self.right_button.half_transition_count = 0;
        self.middle_button.half_transition_count = 0;
        self.scroll_delta = 0;
    }
}

#[derive(Clone, Copy, Default)]
pub struct Keyboard {
    pub w: Button,
    pub a: Button,
    pub s: Button,
    pub d: Button,

    pub space: Button,
    pub enter: Button,
    pub esc: Button,

    /// F1-F12, `f[0]` is not used.
    pub f: [Button; 13],
}

impl Keyboard {
    pub(crate) fn new_frame(&mut self) {
        self.w.half_transition_count = 0;
        self.a.half_transition_count = 0;
        self.s.half_transition_count = 0;
        self.d.half_transition_count = 0;
        self.space.half_transition_count = 0;
        self.enter.half_transition_count = 0;
        self.esc.half_transition_count = 0;

        for fkey in self.f.iter_mut() {
            fkey.half_transition_count = 0;
        }
    }
}
