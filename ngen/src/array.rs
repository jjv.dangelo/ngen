#[derive(Clone, Copy, Eq, Hash, PartialEq)]
pub struct Index {
    value: usize,
    generation: u64,
}

#[derive(Clone, Default)]
struct AllocatorEntry {
    is_live: bool,
    generation: u64,
}

#[derive(Clone)]
pub struct GenerationalIndexAllocator {
    entries: Vec<AllocatorEntry>,
    free: Vec<usize>,
}

impl GenerationalIndexAllocator {
    pub fn new() -> Self {
        Self::with_capacity(32)
    }

    pub fn with_capacity(capacity: usize) -> Self {
        let entries = std::iter::repeat_with(Default::default)
            .take(capacity)
            .collect();
        let free = (0..capacity).collect();

        Self { entries, free }
    }

    fn grow_if_needed(&mut self) {
        if self.free.len() == 0 {
            let next = self.entries.len();
            self.free = (next..next * 2).collect();
            self.entries
                .append(&mut (next..next * 2).map(|_| Default::default()).collect());
        }
    }

    pub fn allocate(&mut self) -> Index {
        self.grow_if_needed();

        let next = self.free.pop().unwrap();
        let entry = &mut self.entries[next];

        assert!(!entry.is_live);
        entry.is_live = true;
        entry.generation += 1;

        Index {
            value: next,
            generation: entry.generation,
        }
    }

    pub fn is_live(&self, index: Index) -> bool {
        let entry = &self.entries[index.value];
        entry.generation == index.generation && entry.is_live
    }

    pub fn iter(&self) -> GenerationalIndexAllocatorIter<'_> {
        GenerationalIndexAllocatorIter::new(self)
    }
}

pub struct GenerationalIndexAllocatorIter<'a> {
    allocator: &'a GenerationalIndexAllocator,
    index: usize,
}

impl<'a> GenerationalIndexAllocatorIter<'a> {
    fn new(allocator: &'a GenerationalIndexAllocator) -> Self {
        Self {
            allocator,
            index: 0,
        }
    }

    fn next_index(&mut self) -> usize {
        let next = self.index;
        self.index += 1;
        next
    }
}

impl<'a> Iterator for GenerationalIndexAllocatorIter<'a> {
    type Item = Index;

    #[inline]
    fn next(&mut self) -> Option<Self::Item> {
        let entries = &self.allocator.entries;

        while self.index < entries.len() {
            let index = self.next_index();
            let entry = &entries[index];

            if entry.is_live {
                return Some(Index {
                    generation: entry.generation,
                    value: index,
                });
            }
        }

        return None;
    }
}

pub struct ArrayEntry<T> {
    value: T,
    generation: u64,
}

impl<T: Clone> Clone for ArrayEntry<T> {
    fn clone(&self) -> Self {
        Self {
            value: self.value.clone(),
            generation: self.generation,
        }
    }
}

pub struct Array<T> {
    entries: Vec<Option<ArrayEntry<T>>>,
}

impl<T: Clone> Clone for Array<T> {
    fn clone(&self) -> Self {
        let entries = self.entries.clone();
        Self { entries }
    }
}

impl<T> Array<T> {
    pub fn new() -> Self {
        Self::with_capacity(32)
    }

    pub fn with_capacity(len: usize) -> Self {
        let entries = std::iter::repeat_with(|| None).take(len).collect();
        Array { entries }
    }

    pub fn set(&mut self, index: Index, value: T) -> bool {
        if index.value >= self.entries.len() {
            self.entries.append(
                &mut std::iter::repeat_with(|| None)
                    .take(index.value - self.entries.len() + 1)
                    .collect(),
            );
        }

        let entry = &mut self.entries[index.value];

        if let Some(mut entry) = entry.as_mut() {
            let should_update = entry.generation <= index.generation;
            if should_update {
                entry.generation = index.generation;
                entry.value = value;
            }

            should_update
        } else {
            let generation = index.generation;
            *entry = Some(ArrayEntry { value, generation });

            true
        }
    }

    pub fn get(&self, index: Index) -> Option<&T> {
        if index.value >= self.entries.len() {
            return None;
        }

        let entry = &self.entries[index.value];

        if let Some(entry) = entry.as_ref() {
            if entry.generation == index.generation {
                return Some(&entry.value);
            }
        }

        return None;
    }

    pub fn get_mut(&mut self, index: Index) -> Option<&mut T> {
        if index.value >= self.entries.len() {
            return None;
        }

        let entry = &mut self.entries[index.value];

        if let Some(entry) = entry.as_mut() {
            if entry.generation == index.generation {
                return Some(&mut entry.value);
            }
        }

        return None;
    }

    pub fn clear_value(&mut self, index: Index) -> Option<T> {
        if index.value >= self.entries.len() {
            return None;
        }

        let entry = &mut self.entries[index.value];
        let can_take = if let Some(entry) = entry {
            index.generation >= entry.generation
        } else {
            false
        };

        if can_take {
            entry.take().map(|entry| entry.value)
        } else {
            None
        }
    }

    #[inline]
    pub fn iter<'a>(&'a self, allocator: &'a GenerationalIndexAllocator) -> ArrayIter<'a, T> {
        ArrayIter::new(self, allocator)
    }
}

pub struct ArrayIter<'a, T> {
    array: &'a Array<T>,
    allocator: &'a GenerationalIndexAllocator,
    index: usize,
}

impl<'a, T> ArrayIter<'a, T> {
    #[inline]
    fn new(array: &'a Array<T>, allocator: &'a GenerationalIndexAllocator) -> Self {
        Self {
            array,
            allocator,
            index: 0,
        }
    }
}

impl<'a, T> Iterator for ArrayIter<'a, T> {
    type Item = &'a T;

    #[inline]
    fn next(&mut self) -> Option<Self::Item> {
        let entries = &self.array.entries;
        let allocator = &self.allocator;

        while self.index < entries.len() {
            let index = self.index;
            self.index += 1;

            if let Some(ref entry) = &entries[index] {
                if allocator.is_live(Index {
                    value: index,
                    generation: entry.generation,
                }) {
                    return Some(&entry.value);
                }
            }
        }

        return None;
    }
}
