#![warn(rust_2018_idioms)]

#[cfg(feature = "platformer")]
mod platformer;
#[cfg(feature = "platformer")]
use platformer::AppState;

fn main() -> Result<(), String> {
    const MAX_QUADS: usize = 10_000;
    const WINDOW_WIDTH: u32 = 1600;
    const WINDOW_HEIGHT: u32 = 1200;
    const WINDOW_TITLE: &str = "ngen Sandbox";

    ngen::Designer::init()
        .inject_asset_folder("assets")
        .initial_window_size(WINDOW_WIDTH, WINDOW_HEIGHT)
        .window_title(WINDOW_TITLE)
        .max_quads(MAX_QUADS)
        .mount(AppState::Uninitialized)
}
