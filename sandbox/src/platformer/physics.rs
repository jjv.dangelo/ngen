use super::entity::EntityId;
use math::{bvh, Ray, Vec2, Vec3};
use ngen::array::{Array, GenerationalIndexAllocator};
use std::collections::HashMap;

#[derive(Clone, Copy)]
pub enum PhysicsState {
    Fixed,
    Fluid { vel: Vec2, ground: Option<EntityId> },
}

#[derive(Clone, Copy)]
pub enum PhysicsComponent {
    Permeable { pos: Vec2, state: PhysicsState },
    Impermeable { pos: Vec2, state: PhysicsState },
}

impl PhysicsComponent {
    pub const fn permeable(pos: Vec2, state: PhysicsState) -> Self {
        PhysicsComponent::Permeable { pos, state }
    }

    pub const fn impermeable(pos: Vec2, state: PhysicsState) -> Self {
        PhysicsComponent::Impermeable { pos, state }
    }

    pub fn pos(&self) -> &Vec2 {
        match self {
            PhysicsComponent::Permeable { pos, .. } | PhysicsComponent::Impermeable { pos, .. } => {
                pos
            }
        }
    }

    pub fn pos_mut(&mut self) -> &mut Vec2 {
        match self {
            PhysicsComponent::Permeable { pos, .. } | PhysicsComponent::Impermeable { pos, .. } => {
                pos
            }
        }
    }

    pub fn vel(&self) -> Option<&Vec2> {
        match self {
            PhysicsComponent::Permeable {
                state: PhysicsState::Fluid { vel, .. },
                ..
            }
            | PhysicsComponent::Impermeable {
                state: PhysicsState::Fluid { vel, .. },
                ..
            } => Some(vel),
            _ => None,
        }
    }

    pub fn vel_mut(&mut self) -> Option<&mut Vec2> {
        match self {
            PhysicsComponent::Permeable {
                state: PhysicsState::Fluid { vel, .. },
                ..
            }
            | PhysicsComponent::Impermeable {
                state: PhysicsState::Fluid { vel, .. },
                ..
            } => Some(vel),
            _ => None,
        }
    }

    pub fn state(&self) -> &PhysicsState {
        match self {
            PhysicsComponent::Permeable { state, .. }
            | PhysicsComponent::Impermeable { state, .. } => state,
        }
    }

    pub fn state_mut(&mut self) -> &mut PhysicsState {
        match self {
            PhysicsComponent::Permeable { state, .. }
            | PhysicsComponent::Impermeable { state, .. } => state,
        }
    }
}

pub enum Plane {
    X { dist: f32 },
    Y { dist: f32 },
}

impl Plane {
    fn dist_mut(&mut self) -> &mut f32 {
        match self {
            Self::X { dist } | Self::Y { dist } => dist,
        }
    }
}

pub struct Collision {
    pub entity: EntityId,
    pub plane: Plane,
}

impl Collision {
    const fn new(entity: EntityId, plane: Plane) -> Self {
        Self { entity, plane }
    }

    const fn new_x(entity: EntityId, dist: f32) -> Self {
        Self::new(entity, Plane::X { dist })
    }

    const fn new_y(entity: EntityId, dist: f32) -> Self {
        Self::new(entity, Plane::Y { dist })
    }

    fn dist_mut(&mut self) -> &mut f32 {
        self.plane.dist_mut()
    }
}

pub struct Collisions {
    pub pos: Vec2,
    pub vel: Vec2,
    pub target_pos: Vec2,
    pub collisions: Vec<Collision>,
}

impl Collisions {
    const fn new(pos: Vec2, vel: Vec2, target_pos: Vec2, collisions: Vec<Collision>) -> Self {
        Self {
            pos,
            vel,
            target_pos,
            collisions,
        }
    }
}

pub fn update(
    allocator: &GenerationalIndexAllocator,
    aabb: &bvh::Tree<EntityId>,
    level: &super::Level,
    components: &mut Array<PhysicsComponent>,
    dt: f32,
) -> HashMap<EntityId, Collisions> {
    let gravity = level.gravity;
    let quarter_tile = level.tile_size * 0.25;
    let mut entity_collisions = HashMap::new();

    for id in allocator.iter() {
        if let Some(component) = components.get(id) {
            let mut x_collisions = HashMap::new();
            let mut y_collisions = HashMap::new();

            let result = match component {
                PhysicsComponent::Impermeable {
                    state: PhysicsState::Fluid { vel, .. },
                    pos,
                } => {
                    let vel = *vel + gravity;
                    let max_vel_x = vel.x * dt;
                    let max_vel_y = vel.y * dt;
                    let target_pos = *pos + vel * dt;

                    let x_offset = Vec2::new(quarter_tile, 0.);
                    let y_offset = Vec2::new(0., quarter_tile);

                    let ray_positions: [Vec3; 4] = [
                        (*pos - x_offset + y_offset, 0.).into(),
                        (*pos + x_offset + y_offset, 0.).into(),
                        (*pos - x_offset, 0.).into(),
                        (*pos + x_offset, 0.).into(),
                    ];

                    let x_sign = vel.x / vel.x.abs();
                    let x_ray_dir = Vec3::new(x_sign, 0., 0.);
                    for ray in ray_positions.iter().map(|p| Ray::new(*p, x_ray_dir)) {
                        for (x_hit, dist) in aabb.cast(ray) {
                            if allocator.is_live(*x_hit) && dist.abs() < max_vel_x.abs() {
                                let collision = x_collisions
                                    .entry(*x_hit)
                                    .or_insert(Collision::new_x(*x_hit, dist));
                                *collision.dist_mut() = (*collision.dist_mut()).min(dist);
                            }
                        }
                    }

                    let y_sign = vel.y / vel.y.abs();
                    let y_ray_dir = Vec3::new(0., y_sign, 0.);
                    for ray in ray_positions.iter().map(|p| Ray::new(*p, y_ray_dir)) {
                        for (y_hit, dist) in aabb.cast(ray) {
                            if allocator.is_live(*y_hit) && dist.abs() < max_vel_y.abs() {
                                let collision = y_collisions
                                    .entry(*y_hit)
                                    .or_insert(Collision::new_y(*y_hit, dist));
                                *collision.dist_mut() = (*collision.dist_mut()).min(dist);
                            }
                        }
                    }

                    Some((*pos, target_pos, vel))
                }

                PhysicsComponent::Permeable {
                    state: PhysicsState::Fluid { vel, .. },
                    pos,
                } => {
                    let vel = *vel + gravity * dt;
                    Some((*pos, *pos + vel, vel))
                }

                _ => None,
            };

            if let Some((pos, target_pos, vel)) = result {
                let y_collisions = y_collisions.drain().map(|(_, collision)| collision);
                let x_collisions = x_collisions.drain().map(|(_, collision)| collision);
                let collisions = y_collisions.chain(x_collisions).collect();
                entity_collisions.insert(id, Collisions::new(pos, vel, target_pos, collisions));
            }
        }
    }

    entity_collisions
}

pub fn commit(
    components: &mut Array<PhysicsComponent>,
    entity: EntityId,
    pos: Vec2,
    vel: Vec2,
    new_ground: Option<EntityId>,
) {
    if let Some(component) = components.get_mut(entity) {
        *component.pos_mut() = pos;
        component.vel_mut().map(|current_vel| *current_vel = vel);

        if let PhysicsState::Fluid { ground, .. } = component.state_mut() {
            *ground = new_ground;
        }
    }
}
