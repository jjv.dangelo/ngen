use super::{
    components::*,
    entity::EntityId,
    physics::{PhysicsComponent, PhysicsState},
};
use math::{BoundingBox, Vec2};
use ngen::{
    array::{Array, GenerationalIndexAllocator},
    camera::Camera,
};

pub struct Level {
    pub environment: Environment,

    width: f32,
    pub tile_size: f32, // Assumes square tiles for now
    pub tiles_per_x: f32,

    pub gravity: Vec2,
}

impl Level {
    pub fn new(width: f32, tile_size: f32, tiles_per_x: f32) -> Self {
        let gravity = (0., -tile_size * 0.05).into();
        let environment = Environment::Grass;
        Self {
            environment,
            width,
            tile_size,
            tiles_per_x,
            gravity,
        }
    }

    pub fn constrain_camera(&self, camera: &mut Camera) {
        let Vec2 { x, y } = camera.window_dim;
        let n = self.tiles_per_x * 0.5 * self.tile_size;
        let d = n * y / x / (camera.fov * 0.5).tan();
        let h = n * y / x;

        camera.eye.x = camera.eye.x.max(n).min(self.width * self.tile_size - n);
        camera.look_at.x = camera.eye.x;
        camera.eye.y = h;
        camera.look_at.y = camera.eye.y;
        camera.eye.z = d;
    }

    // TODO: temporary
    pub fn generate(
        &self,
        allocator: &mut GenerationalIndexAllocator,
        aabb: &mut math::bvh::Tree<EntityId>,
        bounding: &mut Array<math::bvh::Index>,
        physics: &mut Array<PhysicsComponent>,
        tiles: &mut Array<TileComponent>,
    ) {
        let mut add_tile =
            |x: f32, y: f32, tile_component: TileComponent, permeable: bool| -> Option<()> {
                let id = allocator.allocate();

                let x = x * self.tile_size;
                let x1 = x + self.tile_size;
                let y1 = y + self.tile_size;

                let bb = BoundingBox::try_create([(x, y, 0.).into(), (x1, y1, 0.).into()].iter())?;
                let bvh_id = aabb.insert(id, bb);
                bounding.set(id, bvh_id);
                tiles.set(id, tile_component);

                let physics_component = if permeable {
                    PhysicsComponent::permeable((x, y).into(), PhysicsState::Fixed)
                } else {
                    PhysicsComponent::impermeable((x, y).into(), PhysicsState::Fixed)
                };

                physics.set(id, physics_component);

                Some(())
            };

        let total_tiles = (self.tiles_per_x * self.tile_size) as usize;
        for i in 0..total_tiles {
            let x = i as f32;

            if i == 0 {
                add_tile(x, 0., TileComponent::Ground(GroundType::Left), false);
            } else if i == total_tiles - 1 {
                add_tile(x, 0., TileComponent::Ground(GroundType::Right), false);
            } else {
                add_tile(x, 0., TileComponent::Ground(GroundType::Mid), false);
            }

            if i == 0 || i == 2 {
                add_tile(
                    x,
                    self.tile_size,
                    TileComponent::Ground(GroundType::Mid),
                    false,
                );
                add_tile(
                    x,
                    self.tile_size * 3.,
                    TileComponent::Ground(GroundType::Mid),
                    false,
                );
            }

            if i == 5 {
                for n in 1..10 {
                    add_tile(
                        x,
                        n as f32 * self.tile_size,
                        TileComponent::Ladder(LadderPart::Mid),
                        true,
                    );
                }

                add_tile(
                    x,
                    10. * self.tile_size,
                    TileComponent::Ladder(LadderPart::Top),
                    true,
                );
            }

            if i == 10 {
                for n in 1..=3 {
                    add_tile(
                        x,
                        n as f32 * self.tile_size,
                        TileComponent::Ground(GroundType::Mid),
                        false,
                    );
                }
            }
        }
    }
}
