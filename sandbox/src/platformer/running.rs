use super::{
    components::{AlienColor, ControlComponent, LadderPart, TileComponent},
    physics,
    player::PlayerState,
    Memory,
};
use math::Vec2;
use ngen::{Input, Tick};

#[inline]
pub(crate) fn frame(memory: &mut Memory, input: &mut Input, window_dim: Vec2, tick: Tick) {
    // Decompose the memory so we can share parts of it more easily.
    let keyboard = &input.keyboard;
    let aabb = &memory.aabb;
    let level = &mut memory.level;
    let tiles = &mut memory.tiles;
    let allocator = &memory.allocator;
    let physics = &mut memory.physics;
    let camera = &mut memory.camera;
    let controls = &memory.controls;
    let player = &mut memory.player;
    let player_id = player.id;
    let player_state = &mut player.state;
    let gui = &mut memory.gui;

    gui.begin_frame(input.mouse);

    let tile_size = level.tile_size as f32;
    let dt = tick.dt as f32;

    camera.set_window_dim(window_dim);

    if keyboard.f[12].was_down() {
        level.environment.next();
    }

    if keyboard.f[11].was_down() {
        memory.show_bvh = !memory.show_bvh;
    }

    memory.running += tick.dt;
    if memory.running >= 0.35 {
        memory.running = 0.;
        memory.current ^= 1; // Terrible animation system for now
    }

    player_state.inc_elapsed(dt);

    for id in allocator.iter() {
        if let Some(ControlComponent::Keyboard) = controls.get(id) {
            if let Some(physics) = physics.get_mut(id) {
                physics.vel_mut().map(|vel| {
                    vel.x = 0.;

                    if let PlayerState::Climbing(ref mut c) = player_state {
                        vel.y = -level.gravity.y;
                        // Reset at the begining of the frame and update during collision checking
                        // so that the player does not get into a non-stop climb state.
                        c.on_ladder = false;

                        if keyboard.w.is_down() {
                            vel.y += tile_size * 2.;
                        }

                        if keyboard.s.is_down() {
                            vel.y -= tile_size * 2.;
                        }

                        if keyboard.a.is_down() {
                            vel.x = tile_size * -1.5;
                        }

                        if keyboard.d.is_down() {
                            vel.x = tile_size * 1.5;
                        }
                    }

                    if let PlayerState::Standing(ref mut s) = player_state {
                        if keyboard.s.is_down() {
                            *player_state = PlayerState::Ducking(s.duck());
                        } else {
                            if keyboard.d.is_down() {
                                vel.x = tile_size * 3.;
                            }

                            if keyboard.a.is_down() {
                                vel.x = -tile_size * 3.;
                            }

                            if s.can_jump {
                                if keyboard.space.is_down() {
                                    *player_state = PlayerState::Jumping(s.jump());
                                    vel.y += tile_size * 4.;
                                }
                            } else {
                                s.can_jump = keyboard.space.was_down();
                            }
                        }
                    }

                    if let PlayerState::Jumping(_) = player_state {
                        if keyboard.d.is_down() {
                            vel.x = tile_size * 1.5;
                        }

                        if keyboard.a.is_down() {
                            vel.x = -tile_size * 1.5;
                        }
                    }

                    if let PlayerState::Ducking(ref mut d) = player_state {
                        if d.can_jump {
                            if keyboard.space.is_down() {
                                *player_state = PlayerState::Jumping(d.jump());
                                vel.y += tile_size * 6.;
                                return;
                            }
                        } else {
                            d.can_jump = keyboard.space.was_down();
                        }

                        if !keyboard.s.is_down() {
                            *player_state = PlayerState::Standing(d.stand());
                        }
                    }
                });
            }
        }
    }

    let collisions = physics::update(allocator, aabb, level, physics, dt);

    for (entity, unresolved) in collisions {
        let mut vel = unresolved.vel;
        let mut pos = unresolved.target_pos;
        let mut ground = None;

        for collision in unresolved.collisions.iter() {
            match physics.get(collision.entity) {
                Some(physics::PhysicsComponent::Impermeable { .. }) => match collision.plane {
                    physics::Plane::X { dist } => {
                        let sign = vel.x / vel.x.abs();
                        pos.x = unresolved.pos.x + dist * sign;
                    }

                    physics::Plane::Y { dist } => {
                        let sign = unresolved.vel.y / unresolved.vel.y.abs();
                        vel.y = 0.;
                        pos.y = unresolved.pos.y + dist * sign;
                        ground = Some(collision.entity);
                    }
                },

                Some(physics::PhysicsComponent::Permeable { .. }) => {
                    if let Some(TileComponent::Ladder(LadderPart::Mid)) =
                        tiles.get(collision.entity)
                    {
                        // TODO: Shortcut for now. When we have other entities that move we'll
                        // update the logic here.
                        if entity != player_id {
                            continue;
                        }

                        if let Some(ControlComponent::Keyboard) = controls.get(entity) {
                            let climb = keyboard.s.is_down() || keyboard.w.is_down();

                            match player_state {
                                // TODO: If we are not moving the character, we do not cast a ray
                                // that will hit the ladder. It can be a good effect for now, but
                                // in reality, we should also do a bounding box check. Maybe we
                                // should move that into the physics check?
                                PlayerState::Climbing(ref mut c) => {
                                    c.on_ladder = true;
                                    *player_state = PlayerState::Climbing(*c);
                                }

                                PlayerState::Standing(ref mut s) if climb => {
                                    vel.y = 1.;
                                    let mut climb = s.climb();
                                    climb.on_ladder = true;
                                    *player_state = PlayerState::Climbing(climb);
                                }

                                PlayerState::Jumping(ref mut j) if climb => {
                                    vel.y = 0.;
                                    let mut climb = j.climb();
                                    climb.on_ladder = true;
                                    *player_state = PlayerState::Climbing(climb);
                                }

                                _ => (),
                            }
                        }
                    }
                }

                _ => (),
            }
        }

        physics::commit(physics, entity, pos, vel, ground);
    }

    if let Some(player_physics) = physics.get_mut(player_id) {
        use super::{components::*, physics::*, player::*};

        let pos = player_physics.pos();
        memory.camera.eye.x = pos.x;
        memory.camera.look_at.x = pos.x;

        if let PhysicsState::Fluid {
            ground: Some(_),
            vel,
        } = player_physics.state()
        {
            if let PlayerState::Jumping(j) = player_state {
                *player_state = PlayerState::Standing(j.land());
            } else if let PlayerState::Climbing(c) = player_state {
                if vel.y == 0. {
                    *player_state = PlayerState::Standing(c.stand());
                }
            }
        } else {
            match player_state {
                PlayerState::Standing(ref s) => {
                    *player_state = PlayerState::Jumping(s.jump());
                }

                PlayerState::Ducking(ref d) => {
                    *player_state = PlayerState::Jumping(d.jump());
                }

                PlayerState::Climbing(ref c) if !c.on_ladder => {
                    *player_state = PlayerState::Jumping(c.fall());
                }

                _ => (),
            }
        }

        if let Some(TileComponent::Alien(color, action)) = tiles.get_mut(player_id) {
            let n = memory.current & 1 == 0;
            let vel = player_physics.vel().cloned().unwrap_or(Vec2::ZERO);
            let s = vel.x == 0.; // Stopped horizontally
            let facing = if s { action.facing() } else { vel.into() };

            *action = match player_state {
                PlayerState::Jumping(_) => AlienAction::Jumping(facing),
                PlayerState::Standing(_) if s => AlienAction::Standing(facing),
                PlayerState::Standing(_) if n => AlienAction::RunningA(facing),
                PlayerState::Standing(_) => AlienAction::RunningB(facing),
                PlayerState::Ducking(_) => AlienAction::Ducking(facing),
                PlayerState::Climbing(_) if n => AlienAction::ClimbingA(facing),
                PlayerState::Climbing(_) => AlienAction::ClimbingB(facing),
                PlayerState::Hit(_) => AlienAction::Hit(facing),
            };

            if keyboard.f[01].was_down() {
                color.next();
            }
        }
    }

    let padding = 5.;
    if gui.panel(
        memory.block,
        (
            (64. + padding) * 5. + padding,
            (64. + padding) * 5. + padding,
        )
            .into(),
        (0.2, 0.2, 0.2, 0.2).into(),
    ) {
        if input.mouse.left_button.is_down() {
            memory.block += input.mouse.pos - memory.last_mouse_pos;
        }
    }

    let colors = [
        AlienColor::Beige,
        AlienColor::Blue,
        AlienColor::Green,
        AlienColor::Pink,
        AlienColor::Yellow,
    ];
    gui.push_vert_layout(
        memory.block + Vec2::new(padding, padding),
        64.,
        (padding, padding).into(),
    );
    for _ in 0..5 {
        gui.push_stack_layout((0., 0.).into(), 64., (padding, padding).into());
        for (i, sprite) in memory.block_sprites.iter().enumerate() {
            if gui.sprite(
                (0., 0.).into(),
                (64., 64.).into(),
                (1., 1., 1., 1.).into(),
                *sprite,
            ) {
                if input.mouse.left_button.was_down() {
                    if let Some(TileComponent::Alien(color, _)) = tiles.get_mut(player_id) {
                        *color = colors[i];
                    }
                }
            }
        }
        gui.pop_layout();
    }
    gui.pop_layout();

    gui.write_text((5., 5.).into(), (1., 1., 1., 1.).into(), "Hi, ‹Love›!");

    level.constrain_camera(&mut memory.camera);
    memory.last_mouse_pos = input.mouse.pos;
}
