mod components;
mod entity;
mod initialize;
mod level;
mod physics;
mod player;
mod render;
mod running;
mod sprites;

use components::*;
use entity::EntityId;
use level::Level;
use math::{bvh, Vec2};
use ngen::{
    array::{Array, GenerationalIndexAllocator},
    assets::{BitmapId, Sprite},
    camera::Camera,
    renderer::{Gui, RenderCommands},
    Continuation, Input, Tick,
};
use physics::PhysicsComponent;
use player::Player;
use std::rc::Rc;

pub(crate) type PhysicsComponents = Array<PhysicsComponent>;
pub(crate) type ControlComponents = Array<ControlComponent>;
pub(crate) type TileComponents = Array<TileComponent>;
pub(crate) type BoundingComponents = Array<bvh::Index>;

type AssetPacks = ngen::assets::AssetPacks<GameAssetType, GameAssetTag>;

pub(crate) enum AppState {
    Uninitialized,
    Paused(Rc<Memory>),
    // Editing(Rc<State>),
    Running(Rc<Memory>),
}

pub(crate) struct Memory {
    camera: Camera,

    running: f64,
    current: usize,

    player: Player,

    allocator: GenerationalIndexAllocator,
    physics: PhysicsComponents,
    controls: ControlComponents,
    tiles: TileComponents,
    bounding: BoundingComponents,
    aabb: bvh::Tree<EntityId>,

    level: Level,
    gui: Gui,

    texture_id: BitmapId,

    show_bvh: bool,

    last_mouse_pos: Vec2,
    block: Vec2,
    block_sprites: [Sprite; 5],
}

impl ngen::Simulation<GameAssetType, GameAssetTag> for AppState {
    #[inline]
    fn update(
        &mut self,
        input: &mut Input,
        assets: &mut AssetPacks,
        screen_dim: Vec2,
        tick: Tick,
    ) -> Continuation {
        if input.keyboard.esc.was_down() {
            // For now, if the escape key is down, let's end the game.
            // In the future, this should likely open up a menu screen or something.
            return Continuation::Break;
        }

        match self {
            AppState::Running(memory) => {
                // Assume enter = "start" button
                if input.keyboard.enter.was_down() {
                    *self = AppState::Paused(memory.clone());
                } else if let Some(memory) = Rc::get_mut(memory) {
                    running::frame(memory, input, screen_dim, tick);
                }
            }

            AppState::Paused(memory) => {
                // Assume enter = "start" button
                if input.keyboard.enter.was_down() {
                    *self = AppState::Running(memory.clone());
                }
            }

            AppState::Uninitialized => {
                if let Some(mut memory) = initialize::init(assets, screen_dim) {
                    {
                        let allocator = &mut memory.allocator;
                        let aabb = &mut memory.aabb;
                        let tiles = &mut memory.tiles;
                        let bounding = &mut memory.bounding;
                        let physics = &mut memory.physics;
                        memory
                            .level
                            .generate(allocator, aabb, bounding, physics, tiles);
                    }
                    *self = AppState::Running(Rc::new(memory));
                }
            }
        }

        Continuation::Continue
    }

    #[inline]
    fn render(
        &mut self,
        _input: &mut Input,
        assets: &mut AssetPacks,
        render_commands: &mut RenderCommands,
    ) {
        match self {
            AppState::Running(ref state) | AppState::Paused(ref state) => {
                render::process(state, assets, render_commands);
            }

            AppState::Uninitialized => {}
        }
    }
}

#[derive(Clone, Copy, Eq, Hash, PartialEq)]
pub enum GameAssetType {
    Character,
    Item,
    Box,
    Environment,
    Tile,
}

impl AsRef<str> for GameAssetType {
    fn as_ref(&self) -> &str {
        match self {
            Self::Character => "character",
            Self::Item => "item",
            Self::Box => "box",
            Self::Environment => "environment",
            Self::Tile => "tile",
        }
    }
}

#[derive(Clone, Copy, Eq, Hash, PartialEq)]
pub enum GameAssetTag {
    Alien,
    Barnacle,
    Bee,

    Color,

    Front,
    Climb,
    Duck,
    Hit,
    Jump,
    Stand,
    Swim,
    Walk,
    Cimb,
    Attack,
    Dead,
    Move,

    Gui,
    Empty,
    Bomb,
    Coin,

    Item,
    Used,
    Disabled,

    Boxed,
    Crate,
    Crossed,
    Explosive,
    Brick,
    Bridge,
    Flat,

    Bush,
    Cactus,
    Chain,
    Desert,
    Dirt,
    Log,

    Right,
    Left,
    Center,
    Half,
    Corner,
    Rounded,
    Cliff,
    Hill,

    Door,
    Top,
    Mid,
    Open,
    Closed,
}

impl AsRef<str> for GameAssetTag {
    fn as_ref(&self) -> &str {
        match self {
            Self::Alien => "alien",
            Self::Barnacle => "barnacle",
            Self::Bee => "bee",

            Self::Color => "color",

            Self::Front => "front",
            Self::Climb => "climb",
            Self::Duck => "duck",
            Self::Hit => "hit",
            Self::Jump => "jump",
            Self::Stand => "stand",
            Self::Swim => "swim",
            Self::Walk => "walk",
            Self::Cimb => "climb",
            Self::Attack => "attack",
            Self::Dead => "dead",
            Self::Move => "move",

            Self::Gui => "gui",
            Self::Empty => "empty",
            Self::Bomb => "bomb",
            Self::Coin => "coin",

            Self::Item => "item",
            Self::Used => "used",
            Self::Disabled => "disabled",

            Self::Boxed => "boxed",
            Self::Crate => "crate",
            Self::Crossed => "crossed",
            Self::Explosive => "explosive",
            Self::Brick => "brick",
            Self::Bridge => "bridge",
            Self::Flat => "flat",

            Self::Bush => "bush",
            Self::Cactus => "cactus",
            Self::Chain => "chain",
            Self::Desert => "desert",
            Self::Dirt => "dirt",
            Self::Log => "log",

            Self::Right => "right",
            Self::Left => "left",
            Self::Center => "center",
            Self::Half => "half",
            Self::Corner => "corner",
            Self::Rounded => "rounded",
            Self::Cliff => "cliff",
            Self::Hill => "hill",

            Self::Door => "door",
            Self::Top => "top",
            Self::Mid => "mid",
            Self::Open => "open",
            Self::Closed => "closed",
        }
    }
}

impl ngen::trie::IterVariants for GameAssetTag {
    fn iter_variants() -> Vec<Self> {
        vec![
            GameAssetTag::Alien,
            GameAssetTag::Barnacle,
            GameAssetTag::Bee,
            GameAssetTag::Color,
            GameAssetTag::Front,
            GameAssetTag::Climb,
            GameAssetTag::Duck,
            GameAssetTag::Hit,
            GameAssetTag::Jump,
            GameAssetTag::Stand,
            GameAssetTag::Swim,
            GameAssetTag::Walk,
            GameAssetTag::Cimb,
            GameAssetTag::Attack,
            GameAssetTag::Dead,
            GameAssetTag::Move,
            GameAssetTag::Gui,
            GameAssetTag::Empty,
            GameAssetTag::Bomb,
            GameAssetTag::Coin,
            GameAssetTag::Item,
            GameAssetTag::Used,
            GameAssetTag::Disabled,
            GameAssetTag::Boxed,
            GameAssetTag::Crate,
            GameAssetTag::Crossed,
            GameAssetTag::Explosive,
            GameAssetTag::Brick,
            GameAssetTag::Bridge,
            GameAssetTag::Flat,
            GameAssetTag::Bush,
            GameAssetTag::Cactus,
            GameAssetTag::Chain,
            GameAssetTag::Desert,
            GameAssetTag::Dirt,
            GameAssetTag::Log,
            GameAssetTag::Right,
            GameAssetTag::Left,
            GameAssetTag::Center,
            GameAssetTag::Half,
            GameAssetTag::Corner,
            GameAssetTag::Rounded,
            GameAssetTag::Cliff,
            GameAssetTag::Hill,
            GameAssetTag::Door,
            GameAssetTag::Top,
            GameAssetTag::Mid,
            GameAssetTag::Open,
            GameAssetTag::Closed,
        ]
    }
}

impl ngen::trie::IterVariants for GameAssetType {
    fn iter_variants() -> Vec<Self> {
        vec![
            GameAssetType::Character,
            GameAssetType::Item,
            GameAssetType::Box,
            GameAssetType::Environment,
            GameAssetType::Tile,
        ]
    }
}
