use math::{DotProduct, Vec2};
use ngen::assets::Bitmap;

#[derive(Clone, Copy)]
pub enum AlienColor {
    Beige,
    Blue,
    Green,
    Pink,
    Yellow,
}

#[derive(Clone, Copy)]
pub enum Facing {
    Left,
    Right,
}

impl From<Vec2> for Facing {
    fn from(v: Vec2) -> Self {
        if v.dot(Vec2::X) >= 0. {
            Self::Right
        } else {
            Self::Left
        }
    }
}

pub enum AlienAction {
    Standing(Facing),
    RunningA(Facing),
    RunningB(Facing),
    Jumping(Facing),
    Ducking(Facing),
    ClimbingA(Facing),
    ClimbingB(Facing),
    Hit(Facing),
}

impl AlienAction {
    pub fn facing(&self) -> Facing {
        match self {
            Self::Standing(f)
            | Self::RunningA(f)
            | Self::RunningB(f)
            | Self::Jumping(f)
            | Self::Ducking(f)
            | Self::ClimbingA(f)
            | Self::ClimbingB(f)
            | Self::Hit(f) => *f,
        }
    }
}

impl AlienColor {
    pub fn next(&mut self) {
        *self = match self {
            AlienColor::Pink => AlienColor::Blue,
            AlienColor::Blue => AlienColor::Beige,
            AlienColor::Beige => AlienColor::Green,
            AlienColor::Green => AlienColor::Yellow,
            AlienColor::Yellow => AlienColor::Pink,
        };
    }

    pub fn get_uv_coords(&self, action: &AlienAction, bitmap: &Bitmap) -> [Vec2; 4] {
        use AlienAction::*;
        use AlienColor::*;

        let (pos, facing) = match (self, action) {
            (Beige, Standing(facing)) => ((780., 1548.), *facing),
            (Beige, RunningA(facing)) => ((780., 774.), *facing),
            (Beige, RunningB(facing)) => ((780., 516.), *facing),
            (Beige, Jumping(facing)) => ((910., 0.), *facing),
            (Beige, Ducking(facing)) => ((910., 774.), *facing),
            (Beige, ClimbingA(facing)) => ((910., 1290.), *facing),
            (Beige, ClimbingB(facing)) => ((910., 1032.), *facing),
            (Beige, Hit(facing)) => ((910., 258.), *facing),

            (Blue, Standing(facing)) => ((650., 516.), *facing),
            (Blue, RunningA(facing)) => ((520., 1548.), *facing),
            (Blue, RunningB(facing)) => ((520., 1290.), *facing),
            (Blue, Jumping(facing)) => ((650., 774.), *facing),
            (Blue, Ducking(facing)) => ((650., 1548.), *facing),
            (Blue, ClimbingA(facing)) => ((780., 258.), *facing),
            (Blue, ClimbingB(facing)) => ((780., 0.), *facing),
            (Blue, Hit(facing)) => ((650., 1032.), *facing),

            (Green, Standing(facing)) => ((390., 1290.), *facing),
            (Green, RunningA(facing)) => ((390., 516.), *facing),
            (Green, RunningB(facing)) => ((390., 258.), *facing),
            (Green, Jumping(facing)) => ((390., 1548.), *facing),
            (Green, Ducking(facing)) => ((520., 516.), *facing),
            (Green, ClimbingA(facing)) => ((520., 1032.), *facing),
            (Green, ClimbingB(facing)) => ((520., 774.), *facing),
            (Green, Hit(facing)) => ((650., 1032.), *facing),

            (Pink, Standing(facing)) => ((260., 258.), *facing),
            (Pink, RunningA(facing)) => ((130., 1290.), *facing),
            (Pink, RunningB(facing)) => ((130., 1032.), *facing),
            (Pink, Jumping(facing)) => ((260., 516.), *facing),
            (Pink, Ducking(facing)) => ((260., 1290.), *facing),
            (Pink, ClimbingA(facing)) => ((390., 0.), *facing),
            (Pink, ClimbingB(facing)) => ((260., 1548.), *facing),
            (Pink, Hit(facing)) => ((260., 774.), *facing),

            (Yellow, Standing(facing)) => ((0., 1032.), *facing),
            (Yellow, RunningA(facing)) => ((0., 258.), *facing),
            (Yellow, RunningB(facing)) => ((0., 0.), *facing),
            (Yellow, Jumping(facing)) => ((0., 1290.), *facing),
            (Yellow, Ducking(facing)) => ((130., 258.), *facing),
            (Yellow, ClimbingA(facing)) => ((130., 774.), *facing),
            (Yellow, ClimbingB(facing)) => ((130., 516.), *facing),
            (Yellow, Hit(facing)) => ((0., 1548.), *facing),
        };

        let [a, b, c, d] = bitmap.get_uv_coords(pos.into(), 128., 256.);
        let result = match facing {
            Facing::Right => [a, b, c, d],
            Facing::Left => [d, c, b, a],
        };
        result
    }
}

#[derive(Clone, Copy)]
pub enum Environment {
    Dirt,
    Grass,
    Planet,
    Sand,
    Snow,
    Stone,
}

impl Environment {
    pub fn next(&mut self) {
        *self = match self {
            Environment::Dirt => Environment::Grass,
            Environment::Grass => Environment::Planet,
            Environment::Planet => Environment::Sand,
            Environment::Sand => Environment::Snow,
            Environment::Snow => Environment::Stone,
            Environment::Stone => Environment::Dirt,
        }
    }
}

#[derive(Clone, Copy)]
pub enum GroundType {
    Left,
    Mid,
    Right,
}

impl GroundType {
    pub fn get_uv_coords(&self, env: &Environment, bitmap: &Bitmap) -> [Vec2; 4] {
        use Environment::*;
        use GroundType::*;

        let pos = match (env, self) {
            (Dirt, Left) => (1690., 910.),
            (Dirt, Mid) => (1690., 780.),
            (Dirt, Right) => (1690., 650.),

            (Grass, Left) => (1560., 520.),
            (Grass, Mid) => (1560., 390.),
            (Grass, Right) => (1560., 260.),

            (Planet, Left) => (1430., 130.),
            (Planet, Mid) => (1430., 0.),
            (Planet, Right) => (1300., 1820.),

            (Sand, Left) => (1170., 1690.),
            (Sand, Mid) => (1170., 1560.),
            (Sand, Right) => (1170., 1430.),

            (Snow, Left) => (1040., 1300.),
            (Snow, Mid) => (1040., 1170.),
            (Snow, Right) => (1040., 1040.),

            (Stone, Left) => (260., 1806.),
            (Stone, Mid) => (130., 1806.),
            (Stone, Right) => (0., 1806.),
        };

        let result = bitmap.get_uv_coords(pos.into(), 128., 128.);
        result
    }
}

pub enum LadderPart {
    Top,
    Mid,
}

impl LadderPart {
    pub fn get_uv_coords(&self, bitmap: &Bitmap) -> [Vec2; 4] {
        let pos = match self {
            LadderPart::Top => (2210., 260.),
            LadderPart::Mid => (2210., 390.),
        };

        let result = bitmap.get_uv_coords(pos.into(), 128., 128.);
        result
    }
}

pub enum TileComponent {
    Alien(AlienColor, AlienAction),
    Ground(GroundType),
    Ladder(LadderPart),
}

pub enum ControlComponent {
    Keyboard,
}
