use super::{
    components::*,
    level::Level,
    physics::{PhysicsComponent, PhysicsState},
    AssetPacks, Memory, Player,
};
use math::{bvh, Vec2};
use ngen::{
    array::{Array, GenerationalIndexAllocator},
    assets::{AssetManager, Font},
    camera::Camera,
    renderer::Gui,
};

pub(crate) fn init(assets: &mut AssetPacks, screen_dim: Vec2) -> Option<Memory> {
    let level = Level::new(40., 10., 25.);
    let mut camera = Camera::new((-150., -150., 150.).into(), screen_dim);
    level.constrain_camera(&mut camera);

    let mut allocator = GenerationalIndexAllocator::new();
    let mut physics = Array::new();
    let mut controls = Array::new();
    let mut tiles = Array::new();

    let player = Player::new(allocator.allocate());

    let pos = Vec2::new(level.tile_size * 2.5, level.tile_size * 4.);
    let physics_state = PhysicsState::Fluid {
        vel: Vec2::ZERO,
        ground: None,
    };
    physics.set(player.id, PhysicsComponent::impermeable(pos, physics_state));
    controls.set(player.id, ControlComponent::Keyboard);
    tiles.set(
        player.id,
        TileComponent::Alien(AlienColor::Pink, AlienAction::Standing(Vec2::ZERO.into())),
    );

    let texture_id = assets.load_bitmap("assets/platformer/spritesheet_complete.png")?;
    let block_sprites = [
        assets.create_sprite(texture_id, (2730., 910.).into(), 128., 128.)?,
        assets.create_sprite(texture_id, (2730., 780.).into(), 128., 128.)?,
        assets.create_sprite(texture_id, (2730., 650.).into(), 128., 128.)?,
        assets.create_sprite(texture_id, (2730., 520.).into(), 128., 128.)?,
        assets.create_sprite(texture_id, (2730., 390.).into(), 128., 128.)?,
    ];

    let font = Font::from_disk(assets, "assets/platformer/font.ngf").ok()?;

    Some(Memory {
        camera,

        running: 0.,
        current: 0,

        player,
        allocator,
        physics,
        controls,
        bounding: Array::new(),
        tiles,
        aabb: bvh::Tree::new(),

        level,
        gui: Gui::new(font),

        texture_id,

        show_bvh: false,

        last_mouse_pos: Vec2::ZERO,
        block: (100., 100.).into(),
        block_sprites,
    })
}
