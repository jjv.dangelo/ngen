use super::{components::*, AssetPacks, EntityId, Memory};
use math::{Vec2, Vec3};
use ngen::{
    assets::{AssetManager, Bitmap},
    renderer::{RenderCommands, RenderGroup},
};

fn render_tile(
    id: EntityId,
    bitmap: &Bitmap,
    state: &Memory,
    render_group: &mut RenderGroup<'_>,
) -> Option<()> {
    let tile_size = state.level.tile_size;
    let physics_component = state.physics.get(id)?;
    let tile = state.tiles.get(id)?;
    let pos = physics_component.pos();

    let (uv, width, height) = match tile {
        TileComponent::Ground(ground_type) => {
            let uv = ground_type.get_uv_coords(&state.level.environment, bitmap);
            (uv, tile_size, tile_size)
        }

        TileComponent::Ladder(part) => (part.get_uv_coords(bitmap), tile_size, tile_size),

        _ => return None,
    };

    render_group
        .begin_quad()
        .set_pos(*pos)
        .set_dim((width, height).into())
        .set_texture(state.texture_id, uv)
        .push();

    Some(())
}

#[inline]
pub(super) fn process(
    state: &Memory,
    assets: &mut AssetPacks,
    render_commands: &mut RenderCommands,
) {
    let basis = state.camera.perspective_matrix();
    let clear_color = (0.2, 0.4, 0.8, 0.).into();

    if let Some(ref bitmap) = assets.get_bitmap(&state.texture_id) {
        let mut render_group = render_commands.begin_render_group(basis, clear_color);
        for id in state.allocator.iter() {
            render_tile(id, bitmap, state, &mut render_group);
        }
        render_group.end();

        render_group = render_commands.begin_render_group(basis, clear_color);
        for id in state.allocator.iter() {
            if let Some(physics_component) = state.physics.get(id) {
                let pos = physics_component.pos();

                if state.show_bvh {
                    // Show the position point
                    render_group.push_quad(*pos, 0.5, 0.5, (1., 0., 0., 1.).into());
                }

                if let Some(TileComponent::Alien(color, action)) = state.tiles.get(id) {
                    // Offset the player to center on the position
                    let pos = *pos - (state.level.tile_size * 0.5, 0.).into();
                    let uv = color.get_uv_coords(action, bitmap);

                    render_group
                        .begin_quad()
                        .set_pos(pos)
                        .set_width(state.level.tile_size)
                        .set_height(state.level.tile_size * 2.)
                        .set_texture(state.texture_id, uv)
                        .push();
                }
            }
        }
        render_group.end();
    }

    if state.show_bvh {
        let mut render_group = render_commands.begin_render_group(basis, clear_color);
        for i in 0..(state.level.tile_size * state.level.tiles_per_x) as usize {
            let i = i as f32;
            let pos = state.level.tile_size * i;
            let x_pos = Vec2::new(pos, 0.);
            let y_pos = Vec2::new(0., pos);

            let color = (1., 0., 0., 1.).into();
            render_group.push_quad(x_pos, 0.5, 2.5, color);
            render_group.push_quad(y_pos, 2.5, 0.5, color);
        }

        for (_, bb) in state.aabb.iter() {
            let Vec3 {
                x: width,
                y: height,
                ..
            } = bb.max - bb.min;
            let pos = (bb.min.x, bb.min.y).into();
            render_group.push_quad(pos, width, height, (0.2, 0.8, 0.2, 0.5).into());
        }

        render_group.end();
    }

    state.gui.render(render_commands);
}
