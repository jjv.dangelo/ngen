use super::EntityId;

pub enum PlayerState {
    Standing(State<Standing>),
    Jumping(State<Jumping>),
    Ducking(State<Ducking>),
    Climbing(State<Climbing>),
    #[allow(dead_code)]
    Hit(State<Hit>),
}

impl PlayerState {
    pub fn inc_elapsed(&mut self, dt: f32) {
        match self {
            Self::Standing(s) => s.inc_elapsed(dt),
            Self::Jumping(j) => j.inc_elapsed(dt),
            Self::Ducking(d) => d.inc_elapsed(dt),
            Self::Climbing(c) => c.inc_elapsed(dt),
            Self::Hit(h) => h.inc_elapsed(dt),
        }
    }

    #[allow(dead_code)]
    pub fn elapsed(&self) -> f32 {
        match self {
            Self::Standing(s) => s.elapsed,
            Self::Jumping(j) => j.elapsed,
            Self::Ducking(d) => d.elapsed,
            Self::Climbing(c) => c.elapsed,
            Self::Hit(h) => h.elapsed,
        }
    }
}

pub struct Player {
    pub id: EntityId,
    pub state: PlayerState,
}

impl Player {
    pub fn new(id: EntityId) -> Self {
        Self {
            id,
            state: PlayerState::Standing(State::new()),
        }
    }
}

#[derive(Clone, Copy)]
pub struct State<S> {
    pub elapsed: f32,
    curr: S,
}

impl<S> State<S> {
    pub fn inc_elapsed(&mut self, dt: f32) {
        self.elapsed += dt;
    }
}

#[derive(Clone, Copy)]
pub struct Jumping {}

#[derive(Clone, Copy)]
pub struct Standing {
    pub can_jump: bool,
}

#[derive(Clone, Copy)]
pub struct Ducking {
    pub can_jump: bool,
}

#[derive(Clone, Copy)]
pub struct Climbing {
    pub on_ladder: bool,
}

#[derive(Clone, Copy)]
pub struct Hit {}

impl State<Standing> {
    pub fn new() -> Self {
        Self {
            elapsed: 0.,
            curr: Standing { can_jump: true },
        }
    }

    pub fn jump(self) -> State<Jumping> {
        self.into()
    }
    pub fn duck(self) -> State<Ducking> {
        self.into()
    }
    pub fn climb(self) -> State<Climbing> {
        self.into()
    }

    #[allow(dead_code)]
    pub fn hit(self) -> State<Hit> {
        self.into()
    }
}

impl State<Ducking> {
    pub fn stand(self) -> State<Standing> {
        self.into()
    }
    pub fn jump(self) -> State<Jumping> {
        self.into()
    }

    #[allow(dead_code)]
    pub fn hit(self) -> State<Hit> {
        self.into()
    }
}

impl State<Jumping> {
    pub fn land(self) -> State<Standing> {
        self.into()
    }
    pub fn climb(self) -> State<Climbing> {
        self.into()
    }

    #[allow(dead_code)]
    pub fn hit(self) -> State<Hit> {
        self.into()
    }
}

impl State<Climbing> {
    pub fn stand(self) -> State<Standing> {
        self.into()
    }
    pub fn fall(self) -> State<Jumping> {
        self.into()
    }
}

impl<S> std::ops::Deref for State<S> {
    type Target = S;

    fn deref(&self) -> &Self::Target {
        &self.curr
    }
}

impl<S> std::ops::DerefMut for State<S> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.curr
    }
}

impl State<Hit> {
    #[allow(dead_code)]
    pub fn stand(self) -> State<Standing> {
        self.into()
    }
}

impl From<State<Jumping>> for State<Standing> {
    fn from(_: State<Jumping>) -> Self {
        Self {
            elapsed: 0.,
            curr: Standing { can_jump: false },
        }
    }
}

impl From<State<Ducking>> for State<Standing> {
    fn from(d: State<Ducking>) -> Self {
        Self {
            elapsed: 0.,
            curr: Standing {
                can_jump: d.can_jump,
            },
        }
    }
}

impl From<State<Hit>> for State<Standing> {
    fn from(_: State<Hit>) -> Self {
        Self {
            elapsed: 0.,
            curr: Standing { can_jump: true },
        }
    }
}

impl From<State<Climbing>> for State<Standing> {
    fn from(_: State<Climbing>) -> Self {
        Self {
            elapsed: 0.,
            curr: Standing { can_jump: true },
        }
    }
}

impl From<State<Standing>> for State<Jumping> {
    fn from(_: State<Standing>) -> Self {
        Self {
            elapsed: 0.,
            curr: Jumping {},
        }
    }
}

impl From<State<Ducking>> for State<Jumping> {
    fn from(_: State<Ducking>) -> Self {
        Self {
            elapsed: 0.,
            curr: Jumping {},
        }
    }
}

impl From<State<Climbing>> for State<Jumping> {
    fn from(_: State<Climbing>) -> Self {
        Self {
            elapsed: 0.,
            curr: Jumping {},
        }
    }
}

impl From<State<Standing>> for State<Ducking> {
    fn from(s: State<Standing>) -> Self {
        Self {
            elapsed: 0.,
            curr: Ducking {
                can_jump: s.can_jump,
            },
        }
    }
}

impl From<State<Standing>> for State<Climbing> {
    fn from(_: State<Standing>) -> Self {
        Self {
            elapsed: 0.,
            curr: Climbing { on_ladder: false },
        }
    }
}

impl From<State<Jumping>> for State<Climbing> {
    fn from(_: State<Jumping>) -> Self {
        Self {
            elapsed: 0.,
            curr: Climbing { on_ladder: false },
        }
    }
}

impl From<State<Standing>> for State<Hit> {
    fn from(_: State<Standing>) -> Self {
        Self {
            elapsed: 0.,
            curr: Hit {},
        }
    }
}

impl From<State<Jumping>> for State<Hit> {
    fn from(_: State<Jumping>) -> Self {
        Self {
            elapsed: 0.,
            curr: Hit {},
        }
    }
}

impl From<State<Ducking>> for State<Hit> {
    fn from(_: State<Ducking>) -> Self {
        Self {
            elapsed: 0.,
            curr: Hit {},
        }
    }
}
